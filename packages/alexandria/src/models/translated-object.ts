import { object, string, SchemaOf } from "yup";

export interface TranslatedObject<T = string> {
  hu?: T;
  en?: T;
}

export type LanguageKey = keyof TranslatedObject;

// TODO: remove anys when svg importing is fixed in app
export const languages: LanguageKey[] = ["hu", "en"];

export const languageNames: { [key in LanguageKey]: string } = {
  en: "English",
  hu: "Magyar",
};

export function createEmptyTranslatedObject() {
  return languages.reduce((acc, code) => {
    acc[code] = "";
    return acc;
  }, {} as { [key: string]: string });
}

export function createEmptyTranslatedObjectWithContent<T>(content: T) {
  return languages.reduce((acc, code) => {
    acc[code] = content;
    return acc;
  }, {} as { [key: string]: T });
}

export const fallbackOrder: LanguageKey[] = ["en", "hu"];

export function getTranslatedObjectValueInLanguage<T = string>(
  object: TranslatedObject<T> | null | undefined,
  language: LanguageKey
) {
  if (object) {
    if (object[language]) return object[language] as unknown as JSX.Element;

    for (const fallbackLanguage of fallbackOrder) {
      if (object[fallbackLanguage]) return object[fallbackLanguage] as unknown as JSX.Element;
    }
  }

  return "";
}

export function getTranslatedObjectStringInLanguage(
  object: TranslatedObject | null | undefined,
  language: LanguageKey
) {
  return String(getTranslatedObjectValueInLanguage(object, language));
}

export function getTranslatedObjectGenericInLanguage<T = string>(
  object: TranslatedObject<T>,
  language: LanguageKey
): T {
  return getTranslatedObjectValueInLanguage(object, language) as unknown as T;
}

export function isTranslatedObjectEmpty(input: TranslatedObject) {
  return Object.values(input).every((value) => !value);
}

export const translatedObjectSchema = object().shape(
  languages.reduce((acc, code) => {
    // @ts-ignore
    acc[code] = string();
    return acc;
  }, {} as { [key: string]: SchemaOf<string | undefined> })
);
