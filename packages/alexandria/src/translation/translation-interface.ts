import { DocumentInterface } from "@litbase/core";
import { Observable } from "rxjs";

export type LanguageCode = string;
export type TranslationObject = { [key in LanguageCode]: string };
export type TranslationInput = string | TranslationObject | undefined;
export type TranslationEntry = string | undefined | TranslationTable;
export type TranslationParams = Record<string, string | number>;

export interface TranslationTable {
  [key: string]: TranslationEntry;
}

export interface TranslationLoader {
  (code: LanguageCode): Observable<TranslationTable> | Promise<TranslationTable> | TranslationTable;
}

export interface LanguageSettings {
  language: LanguageCode;
  fallbackLanguage: LanguageCode;
  forcePlaceholders: boolean;
  disableFallback: boolean;
  translationLoader: TranslationLoader;
}

export interface TranslationInterface extends DocumentInterface {
  code: LanguageCode;
  countryCode: string;
  name: string;
  translations: TranslationTable;
}
