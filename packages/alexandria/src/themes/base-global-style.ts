import { interFont, metropolisFont } from "../fonts/index.js";
import { reboot } from "styled-reboot";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { createGlobalStyle, css } from "styled-components";

export const baseGlobalStyle = css`
  ${interFont};
  ${metropolisFont};

  ${reboot};

  ${StyledIconBase} {
    height: 1em;
  }

  body {
    min-height: 100vh;
  }

  button:focus {
    outline: none;
  }

  input,
  button,
  select,
  optgroup,
  textarea {
    color: ${({ theme }) => theme.bodyColor};
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: ${({ theme }) => theme.headingsFontFamily};
  }
`;

export const BaseGlobalStyle = createGlobalStyle`
  ${baseGlobalStyle}
`;
