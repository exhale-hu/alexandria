import { baseTheme, BaseThemeInterface } from "./base-theme.js";

export type ThemeInterface<TCustom> = BaseThemeInterface & TCustom;

export function createTheme<TCustom extends Partial<BaseThemeInterface> = {}>(
  custom?: TCustom
): BaseThemeInterface & TCustom {
  return extendTheme<BaseThemeInterface, TCustom>(baseTheme, custom || ({} as TCustom));
}

export function extendTheme<TBase extends object, TExtend>(base: TBase, extend: TExtend): TBase & TExtend {
  return Object.create(base, Object.getOwnPropertyDescriptors(extend));
}
