import { css } from "styled-components";

import metropolisRegular from "../assets/fonts/metropolis/Metropolis-Regular.woff2";
import metropolisMedium from "../assets/fonts/metropolis/Metropolis-Medium.woff2";
import metropolisBold from "../assets/fonts/metropolis/Metropolis-Bold.woff2";

export const metropolisFont = css`
  @font-face {
    font-family: "Metropolis";
    font-style: normal;
    font-weight: 400;
    src: url("${metropolisRegular}") format("woff2");
  }

  @font-face {
    font-family: "Metropolis";
    font-style: normal;
    font-weight: 500;
    src: url("${metropolisMedium}") format("woff2");
  }

  @font-face {
    font-family: "Metropolis";
    font-style: normal;
    font-weight: 700;
    src: url("${metropolisBold}") format("woff2");
  }
`;
