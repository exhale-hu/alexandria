/** Used to render arbitrary number of components */
export function forMap<T = number>(
  from: number,
  to: number,
  stepOrCallback: number | ((value: number) => T),
  callback?: (value: number) => T
): T[] {
  const results: T[] = [];
  let step: number;

  if (typeof stepOrCallback === "number") {
    step = stepOrCallback;
    callback = callback as (value: number) => T;
  } else {
    step = Math.sign(to - from);
    callback = stepOrCallback;
  }

  if (step) {
    // Prevent endless loops
    for (let current = from; 0 < step ? current < to : to < current; current += step) {
      results.push(callback(current));
    }
  }

  return results;
}

export function moveItemInPlace<T>(array: T[], from: number, to: number) {
  const newArray = [...array];
  const [item] = newArray.splice(from, 1);
  newArray.splice(to, 0, item);
  return newArray;
}

export function moveItem<T>(array: T[], from: number, to: number) {
  return moveItemInPlace([...array], from, to);
}
