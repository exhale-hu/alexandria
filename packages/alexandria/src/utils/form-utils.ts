export function getSelfAndAncestorFields(name: string) {
  const names = [name];

  let lastMatchIndex = name.length;
  while (-1 < (lastMatchIndex = name.lastIndexOf(".", lastMatchIndex - 1))) {
    names.push(name.slice(0, lastMatchIndex));
  }

  return names;
}
