import { delayReject } from "./delay.js";

export function timeout<T>(waitMs: number, promise: Promise<T>, error: Error) {
  return Promise.race<T | Error>([delayReject<Error>(waitMs, error), promise]);
}
