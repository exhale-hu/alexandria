import { Meta, Story } from "@storybook/react";
import { IconCircle, iconCircleKinds } from "../../components/index.js";

export default {
  title: "Utils/IconCircle",
  component: IconCircle,
  argTypes: {
    $kind: {
      control: "radio",
      options: Object.keys(iconCircleKinds),
    },
  },
} as Meta;

const Template: Story = (args) => <IconCircle $kind={args.$kind}>!</IconCircle>;

export const Normal = Template.bind({});
Normal.args = {
  $kind: "default",
};
