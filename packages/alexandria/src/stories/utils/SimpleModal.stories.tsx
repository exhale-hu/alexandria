import { Meta, Story } from "@storybook/react";

import { iconCircleKinds, ModalProvider, SimpleModal, SimpleModalProps } from "../../components/index.js";
import { Error } from "@styled-icons/boxicons-regular";

export default {
  title: "Utils/Modal",
  component: SimpleModal,
  argTypes: {
    iconKind: {
      control: "radio",
      options: Object.keys(iconCircleKinds),
    },
    tone: {
      control: "radio",
      options: ["light", "dark"],
    },
    children: {
      control: "text",
    },
    title: {
      control: "text",
    },
    footer: {
      control: "text",
    },
    closeButtonLabel: {
      control: "text",
    },
    icon: {
      table: {
        disable: true,
      },
    },
    className: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story<SimpleModalProps> = (args) => (
  <ModalProvider>
    <SimpleModal {...args} icon={Error} />
  </ModalProvider>
);

export const Normal = Template.bind({});
Normal.args = {
  children: "Lorem ipsum!",
  iconKind: "default",
  title: "Title",
  footer: "Footer",
  hideCloseIcon: false,
  closeButtonLabel: "Close",
  tone: "light",
};
