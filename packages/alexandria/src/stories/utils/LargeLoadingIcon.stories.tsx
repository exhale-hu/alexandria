import { Meta, Story } from "@storybook/react";

import { LargeLoadingIcon } from "../../components/index.js";

export default {
  title: "Utils/LargeLoadingIcon",
  component: LargeLoadingIcon,
} as Meta;

const Template: Story = () => <LargeLoadingIcon />;

export const Large_Loading_Icon = Template.bind({});

Large_Loading_Icon.parameters = {
  controls: { hideNoControlsWarning: true },
};
