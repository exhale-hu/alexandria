import { Meta, Story } from "@storybook/react";

import { RichTextDisplay } from "../../components/index.js";

export default {
  title: "Utils/RichTextDisplay",
  component: RichTextDisplay,
  argTypes: {
    content: {
      control: "text",
    },
    matchers: {
      table: {
        disable: true,
      },
    },
    style: {
      table: {
        disable: true,
      },
    },
    className: {
      table: {
        disable: true,
      },
    },
    innerRef: {
      table: {
        disable: true,
      },
    },
    whitelist: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => <RichTextDisplay content={args.content} />;

export const Normal = Template.bind({});
Normal.args = {
  content: "<h1>Hello World</h1><br/><p>Second line</p>",
};
