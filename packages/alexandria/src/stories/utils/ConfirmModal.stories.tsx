import { Meta, Story } from "@storybook/react";

import { ConfirmModal, iconCircleKinds, ModalProvider } from "../../components/index.js";
import { Error } from "@styled-icons/boxicons-regular";

export default {
  title: "Utils/ConfirmModal",
  component: ConfirmModal,
  argTypes: {
    kind: {
      control: "radio",
      options: Object.keys(iconCircleKinds),
    },
    children: {
      control: "text",
    },
    title: {
      control: "text",
    },
    confirmLabel: {
      control: "text",
    },
    icon: {
      table: {
        disable: true,
      },
    },
    className: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <ModalProvider>
    <ConfirmModal {...args} icon={Error} />
  </ModalProvider>
);

export const Normal = Template.bind({});
Normal.args = {
  children: "Lorem ipsum!",
  kind: "danger",
  title: "Title",
  confirmLabel: "OK",
};
