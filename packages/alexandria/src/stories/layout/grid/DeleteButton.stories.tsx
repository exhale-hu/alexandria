import { Meta, Story } from "@storybook/react";
import { DeleteButton } from "../../../components/index.js";

export default {
  title: "Grid/DeleteButton",
  component: DeleteButton,
} as Meta;

const Template: Story = () => <DeleteButton />;

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
