import { Meta, Story } from "@storybook/react";
import {
  AddButton,
  AdminButton,
  Cell,
  EditButton,
  Grid,
  GridRow,
  HeadCell,
  SortableHeadCell,
} from "../../../components/index.js";
import { GridSearchHeader } from "../../../components/layout/grid/grid-search-header.js";
import { BooleanCell } from "../../../components/layout/grid/boolean-cell.js";
import { GridPaginationFooter } from "../../../components/layout/grid/grid-pagination-footer.js";
import styled from "styled-components";
import { noop } from "lodash-es";

export default {
  title: "Grid/Grid",
  component: Grid,
} as Meta;

const Template: Story = () => (
  <StyledGridContainer>
    <GridSearchHeader isSearchLoading={false} search={noop} />
    <Grid columns="repeat(3, min-content)">
      <GridRow>
        <SortableHeadCell name="shc" value={{}} onSort={noop}>
          Grid Content
        </SortableHeadCell>
        <HeadCell>Boolean Cell</HeadCell>
        <HeadCell>Grid Buttons</HeadCell>
      </GridRow>
      <GridRow>
        <Cell>Some simple content</Cell>
        <Cell>
          <BooleanCell value={true} />
          <BooleanCell value={false} />
        </Cell>
        <Cell>
          <EditButton />
          <AddButton />
          <AdminButton />
        </Cell>
      </GridRow>
    </Grid>
    <GridPaginationFooter
      pageSize={1}
      onPageSizeChange={noop}
      pageCount={3}
      currentPage={0}
      goToPage={noop}
      itemCount={0}
    />
  </StyledGridContainer>
);

export const Normal = Template.bind({});

const StyledGridContainer = styled.div`
  display: block;
  width: fit-content;
`;

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
