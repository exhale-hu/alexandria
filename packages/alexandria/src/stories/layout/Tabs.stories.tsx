import { Meta, Story } from "@storybook/react";
import { TabList, Tabs, Tab, TabContents, TabContent } from "../../components/index.js";

export default {
  title: "Layout/Tabs",
  component: Tabs,
  argTypes: {
    activeIndex: {
      control: "number",
    },
    onChange: {
      table: {
        disable: true,
      },
    },
    children: {
      table: {
        disable: true,
      },
    },
    extraTabProps: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <Tabs {...args}>
    <TabList>
      <Tab>First</Tab>
      <Tab>Second</Tab>
      <Tab>Third</Tab>
    </TabList>
    <TabContents>
      <TabContent>First tab content</TabContent>
      <TabContent>Second tab content</TabContent>
      <TabContent>Third tab content</TabContent>
    </TabContents>
  </Tabs>
);

export const Normal = Template.bind({});

Normal.args = {
  activeIndex: 0,
};
