import { Meta, Story } from "@storybook/react";
import { HookInputField } from "../../components/hook-form/hook-input-field.js";
import { HookTranslatableField } from "../../components/hook-form/hook-translatable-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/TranslatableField",
  component: HookTranslatableField,
} as Meta;

const Template: Story = () => (
  <HookForm>
    <HookTranslatableField>
      <HookInputField name="text" label="hello" />
    </HookTranslatableField>
  </HookForm>
);
export const Normal = Template.bind({});

Normal.args = {};
