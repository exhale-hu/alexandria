import { Meta, Story } from "@storybook/react";
import { CheckboxField } from "../../components/index.js";
import { HookCheckboxField } from "../../components/hook-form/hook-checkbox-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/Checkbox",
  component: CheckboxField,
  argTypes: {
    id: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ check: args.value }}>
    <HookCheckboxField name="check" checkboxLabel={args.checkboxLabel} disabled={args.disabled} />
  </HookForm>
);

export const Normal = Template.bind({});

Normal.args = {
  value: true,
  disabled: false,
  checkboxLabel: "Checkbox",
};
