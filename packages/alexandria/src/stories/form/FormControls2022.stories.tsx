import { Meta, Story } from "@storybook/react";
import {
  Form,
  FormArrayField,
  FormButtonGroupField,
  FormCheckboxField,
  FormDateField,
  FormFileUploadField,
  FormInputField,
  FormRichTextField,
  FormSelectField,
  FormSingleFileUploadField,
  FormStringArrayField,
  FormSwitchField,
  FormTimeField,
} from "../../form/index.js";

export default {
  title: "Form 2022/Everythang",
  component: FormInputField,
  argTypes: {
    required: {
      table: {
        disable: true,
      },
    },
    email: {
      table: {
        disable: true,
      },
    },
    minLength: {
      table: {
        disable: true,
      },
    },
    baseSchema: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
    label: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

// This storybook entry was made entirely just to cover the "description" property being added to
// form controls using FormFieldWrapper.
// You are certainly welcome to split this in multiple files and cover any missing functionality for
// the controls listed in this story.
const Template: Story = (args) => (
  <Form watchDefaultValues defaultValues={{ check: args.value }} onSubmit={(values) => {}}>
    <FormInputField
      disabled={args.disabled}
      name="someInputProperty1"
      label={args.inputLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.propertyDescription}
    />
    <FormArrayField
      name="someArrayPropery1"
      labelFallsBackToName={args.labelFallsBackToName}
      label={args.arrayFieldLabel}
      description={args.arrayPropertyDescription}
    >
      <FormInputField
        disabled={args.disabled}
        labelFallsBackToName={args.labelFallsBackToName}
        name="someInputProperty2"
        label={args.inputLabel}
        description={args.propertyDescription}
      />
    </FormArrayField>
    <FormCheckboxField
      name="someCheckboxProperty"
      label={args.checkboxLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      checkboxLabel="Checkbox label"
      description={args.checkboxPropertyDescription}
    />
    {/* TODO fix tooltip and description overlapping */}
    <FormButtonGroupField
      name="someButtonGroupProperty"
      label={args.buttonGroupLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.buttonGroupDescription}
      options={[{ label: "FIX MUH TOOLTIP BRUV", value: "whatever", tooltip: "CantChangeThisEither" }]}
    />
    <FormDateField
      name="someDateProperty"
      label={args.dateLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.dateFieldDescription}
    />
    <FormFileUploadField
      name="someMultiFileUploadProperty"
      label={args.multiFileUploadLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.multiFileUploadFieldDescription}
    />
    <FormSingleFileUploadField
      name="someSingleFileUploadProperty"
      label={args.multiFileUploadLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.singleFileUploadFieldDescription}
    />
    <FormSwitchField
      name="someSwitchProperty"
      label={args.switchFieldLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.switchFieldDescription}
    />
    <FormRichTextField
      name="someRichTextProperty"
      label={args.richTextFieldLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.richTextFieldDescription}
    />
    <FormSelectField
      name="someSelectProperty"
      label={args.selectFieldLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.selectFieldDescription}
      options={["Option 1", "Option 2", "Option 3"]}
    />
    <FormStringArrayField
      name="someStringArrayProperty"
      label={args.stringArrayFieldLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.stringArrayFieldDescription}
    />
    {/* TODO this seems to be completely broken */}
    <FormTimeField
      name="someTimeProperty"
      label={args.timeFieldLabel}
      labelFallsBackToName={args.labelFallsBackToName}
      description={args.timeFieldDescription}
    />
  </Form>
);

export const Normal = Template.bind({});

Normal.args = {
  inputLabel: "Input field",
  checkboxLabel: "Checkbox field",
  arrayFieldLabel: "Array field",
  dateLabel: "Date field",
  buttonGroupLabel: "Button group",
  multiFileUploadLabel: "Multi file upload field",
  singleFileUploadLabel: "Single file upload field",
  switchFieldLabel: "Switch field",
  richTextFieldLabel: "Rich-text field",
  selectFieldLabel: "Select field",
  stringArrayFieldLabel: "String array field",
  timeFieldLabel: "Time field",

  inputFieldDescription: "Some description under the input field label",
  arrayFieldDescription: "Some description under the array field label",
  checkboxFieldDescription: "Some description under the checkbox field label",
  dateFieldDescription: "Date description",
  buttonGroupDescription: "Some description under the button group label",
  multiFileUploadFieldDescription: "Some description under the multi-file upload field label",
  singleFileUploadFieldDescription: "Some description under the single-file upload field label",
  richTextFieldDescription: "Some description under the Rich-Text field label",
  switchFieldDescription: "Some description under the switch field label",
  selectFieldDescription: "Some description under the select field label",
  stringArrayFieldDescription: "Some description under the string array field label",
  timeFieldDescription: "Some description under the time field label",

  labelFallsBackToName: true,
  disabled: false,
};
