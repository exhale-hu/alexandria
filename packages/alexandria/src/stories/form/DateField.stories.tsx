import { Meta, Story } from "@storybook/react";
import { DateField } from "../../components/index.js";
import { HookDateField } from "../../components/hook-form/hook-date-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/DateField",
  component: DateField,
  argTypes: {
    value: {
      control: "date",
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ date: args.value }}>
    <HookDateField name="date" disabled={args.disabled} />
  </HookForm>
);

export const Normal = Template.bind({});

Normal.args = {
  value: new Date(),
  disabled: false,
};
