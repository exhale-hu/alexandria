import { ComponentPropsWithRef, forwardRef, Ref, useState } from "react";
import { Meta, Story } from "@storybook/react";
import { useEscapableInput } from "../../hooks/index.js";

export default {
  title: "Form/UseEscapableInput",
} as Meta;

function Template() {
  const [value, setValue] = useState("");

  return (
    <div>
      <div>
        value: <span>{value}</span>
      </div>
      <DetachableInput
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
      />
    </div>
  );
}

export const BoundTemplate = (Template as Story).bind({});

BoundTemplate.args = {};

export interface DetachableInputProps extends Omit<ComponentPropsWithRef<"input">, "onChange"> {
  value: string;
  onChange(newValue: string): void;
}

function DetachableInputComponent({ value, onChange, ...props }: DetachableInputProps, ref: Ref<HTMLInputElement>) {
  const [escapableProps] = useEscapableInput({
    ref,
    value,
    onChange,
  });

  return <input {...props} {...escapableProps} />;
}

export const DetachableInput = forwardRef(DetachableInputComponent);
