import { Story, Meta } from "@storybook/react";

import { Button, buttonKinds, ButtonProps, buttonSizes } from "../../components/index.js";

export default {
  title: "Inputs/Button",
  component: Button,
  argTypes: {
    children: { control: "text" },
    $kind: {
      control: "radio",
      options: Object.keys(buttonKinds),
    },
    $size: {
      control: "radio",
      options: Object.keys(buttonSizes),
    },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Configurable_Button = Template.bind({});
Configurable_Button.args = {
  $kind: "primary",
  $size: "default",
  children: "Button",
};
