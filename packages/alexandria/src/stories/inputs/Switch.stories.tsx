import { Story, Meta } from "@storybook/react";

import { Switch, SwitchProps } from "../../components/index.js";
import { noop } from "../../components/utils/noop.js";

export default {
  title: "Inputs/Switch",
  component: Switch,
  argTypes: {
    value: { control: "boolean" },
  },
} as Meta;

const Template: Story<SwitchProps> = (args) => <Switch {...args} onChange={noop} />;

export const Configurable_Switch = Template.bind({});
Configurable_Switch.args = {
  value: true,
  onChange: noop,
};
