import { Meta, Story } from "@storybook/react";
import { ProgressBar } from "../../components/index.js";

export default {
  title: "Feedback/ProgressBar",
  component: ProgressBar,
  argTypes: {
    progress: {
      control: "range",
    },
    className: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => <ProgressBar progress={args.progress} />;

export const Normal = Template.bind({});

Normal.args = {
  progress: 10,
};
