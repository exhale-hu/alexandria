import { Meta, Story } from "@storybook/react";
import { SidebarItem } from "../../components/sidebar/sidebar-item.js";
import { BrowserRouter } from "react-router-dom";

export default {
  title: "Sidebar/SidebarItemCollapsible",
  component: SidebarItem,
  argTypes: {
    icon: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
    to: {
      table: {
        disable: true,
      },
    },
    children: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = () => (
  <BrowserRouter>
    <SidebarItem name="Category">
      <SidebarItem to="/1" name="Link 1" />
      <SidebarItem to="/2" name="Link 2" />
    </SidebarItem>
  </BrowserRouter>
);

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
