import { Meta, Story } from "@storybook/react";
import { BrowserRouter } from "react-router-dom";
import { Sidebar } from "../../components/sidebar/sidebar.js";
import { SidebarItem } from "../../components/sidebar/sidebar-item.js";
import { SidebarProfile } from "../../components/sidebar/sidebar-profile.js";
import { noop } from "../../components/utils/noop.js";

export default {
  title: "Sidebar/Sidebar",
  component: Sidebar,
  argTypes: {
    header: {
      table: {
        disable: true,
      },
    },
    profile: {
      table: {
        disable: true,
      },
    },
    children: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = () => (
  <BrowserRouter>
    <Sidebar header={<h1>Header</h1>} profile={<SidebarProfile name="Test User" isLoading={false} onLogout={noop} />}>
      <SidebarItem name="Category">
        <SidebarItem to="/1" name="Link 1" />
        <SidebarItem to="/2" name="Link 2" />
      </SidebarItem>
      <SidebarItem to="/3" name="Link 3" />
    </Sidebar>
  </BrowserRouter>
);

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
