import { Meta, Story } from "@storybook/react";
import { SidebarItem } from "../../components/sidebar/sidebar-item.js";
import { BrowserRouter } from "react-router-dom";

export default {
  title: "Sidebar/SidebarItem",
  component: SidebarItem,
  argTypes: {
    icon: {
      table: {
        disable: true,
      },
    },
    name: {
      table: {
        disable: true,
      },
    },
    to: {
      table: {
        disable: true,
      },
    },
    children: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <BrowserRouter>
    <SidebarItem to="/" name={args.text} />
  </BrowserRouter>
);

export const Normal = Template.bind({});

Normal.args = {
  text: "Text",
};
