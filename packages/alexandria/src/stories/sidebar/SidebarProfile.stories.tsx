import { Meta, Story } from "@storybook/react";
import { SidebarProfile } from "../../components/sidebar/sidebar-profile.js";
import { noop } from "../../components/utils/noop.js";

export default {
  title: "Sidebar/SidebarProfile",
  component: SidebarProfile,
  argTypes: {},
} as Meta;

const Template: Story = (args) => <SidebarProfile name={args.name} isLoading={args.isLoading} onLogout={noop} />;

export const Normal = Template.bind({});

Normal.args = {
  name: "Test User",
  isLoading: false,
};
