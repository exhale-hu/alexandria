import { Meta, Story } from "@storybook/react";
import { GridRow, HeadCell } from "../../components/index.js";
import { ListCard } from "../../components/layout/list/list-card.js";
import { noop } from "rxjs";
import { LooseObject, mockDataGenerator, rowComponent } from "./listUtils.js";

export default {
  title: "List/ListCard",
  component: ListCard,
  argTypes: {
    columns: {
      control: "number",
    },
    onPageChange: {
      table: {
        disable: true,
      },
    },
    onNItemsChange: {
      table: {
        disable: true,
      },
    },
    items: {
      table: {
        disable: true,
      },
    },
    nItems: {
      table: {
        disable: true,
      },
    },
    page: {
      table: {
        disable: true,
      },
    },
    rowComponent: {
      table: {
        disable: true,
      },
    },
    headerRowComponent: {
      table: {
        disable: true,
      },
    },
    className: {
      table: {
        disable: true,
      },
    },
    style: {
      table: {
        disable: true,
      },
    },
    onQueryChange: {
      table: {
        disable: true,
      },
    },
    query: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;
const Template: Story = (args) => {
  const headCells = [];
  for (let i = 0; i < args.columns; i++) {
    headCells.push("Column " + i);
  }
  const headerRowComponent = (
    <GridRow>
      {headCells.map((item, index) => (
        <HeadCell key={index}>{item}</HeadCell>
      ))}
    </GridRow>
  );
  return (
    <ListCard<LooseObject>
      onCurrentPageChange={noop}
      onPageSizeChange={noop}
      items={mockDataGenerator(args.rows, args.columns)}
      pageSize={args.rows}
      currentPage={0}
      columns={`repeat(${args.columns},1fr)`}
      rowComponent={rowComponent}
      headerRowComponent={headerRowComponent}
      searchable={args.searchable}
      cardTitle={args.title}
      showPagination={args.showPagination}
    />
  );
};

export const Normal = Template.bind({});

Normal.args = {
  rows: 10,
  columns: 5,
  searchable: true,
  title: "Title",
  showPagination: true,
};
