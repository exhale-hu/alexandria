import { ReactNode } from "react";
import { PageBar, PageBarButtons, PageTitle } from "./page.js";

export interface BasicPageProps {
  title: ReactNode;
  /**
   * Additional JSX (optionally wrapped as positional object) to put in the page as buttons.
   * Positional "buttons" objects ( contains "top" and "bottom" properties ) are put to their respective places.
   * Non-positional "buttons" objects are always put in the bottom section.
   */
  buttons?: ReactNode | { top?: ReactNode; bottom?: ReactNode };
  children: ReactNode;
}

export function BasicPage({ title, buttons, children }: BasicPageProps) {
  return (
    <>
      <PageBar>
        <PageTitle>{title}</PageTitle>
        <PageBarButtons>{isTopBottomButtons(buttons) ? buttons.top : null}</PageBarButtons>
      </PageBar>
      {children}
      <PageBar>
        <PageBarButtons>{isTopBottomButtons(buttons) ? buttons.bottom : buttons}</PageBarButtons>
      </PageBar>
    </>
  );
}

/**
 * Returns true, if the provided "value" is an object and contains either "top" or "bottom" properties.
 * Used to determine for BasicPages if the provided buttons prop is positional or not.
 * @param value The "buttons" prop passed to BasicPages
 */
function isTopBottomButtons(
  value: ReactNode | { top?: ReactNode; bottom?: ReactNode }
): value is { top?: ReactNode; bottom?: ReactNode } {
  return typeof value === "object" && !!value && ("bottom" in value || "top" in value);
}
