import { ReactNode } from "react";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { useControllerEx } from "./use-controller-ex.js";
import { FileUploadUrl, FileUploadUrlProps, useUniqueId } from "../form/index.js";

export interface HookFileUploadFieldProps extends Omit<FileUploadUrlProps, "value" | "onChange" | "limit"> {
  name: string;
  label?: ReactNode;
}

export function HookSingleFileUploadField({ name, label, ...props }: HookFileUploadFieldProps) {
  const {
    field: { value, onChange },
  } = useControllerEx({
    name,
    transform: (value) => (value ? [value] : []),
    parse: (newValue: string[]) => (newValue.length ? newValue[0] : null),
  });

  const fieldId = useUniqueId();

  return (
    <HookFieldWrapper id={fieldId} name={name} label={label}>
      <FileUploadUrl value={value} onChange={onChange} limit={1} id={fieldId} {...props} />
    </HookFieldWrapper>
  );
}

export function HookFileUploadField({ name, label, ...props }: HookFileUploadFieldProps) {
  const {
    field: { value, onChange },
  } = useControllerEx<string[]>({
    name,
    transform: (value) => (value ? value : []),
    parse: (newValue: string[]) => newValue || [],
  });

  const fileId = useUniqueId();

  return (
    <HookFieldWrapper id={fileId} name={name} label={label}>
      <FileUploadUrl value={value} onChange={onChange} id={fileId} {...props} />
    </HookFieldWrapper>
  );
}
