import { ReactElement, ReactNode } from "react";
import { useWatch } from "react-hook-form";

export function HookFormConditional<T = Record<string, unknown>>({
  predicate,
  children,
}: {
  predicate?: (values: T) => boolean;
  children?: ReactNode;
}) {
  const values = useWatch({}) as T;

  if (predicate?.(values)) {
    return children as ReactElement;
  }

  return null;
}
