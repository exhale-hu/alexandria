import { ReactNode } from "react";
import Tippy from "@tippyjs/react";
import styled from "styled-components";
import { InfoCircle } from "@styled-icons/boxicons-solid";
import { useController } from "react-hook-form";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { Button, ButtonGroup } from "../inputs/button.js";
import { spacing1 } from "../../themes/index.js";

export interface HookButtonGroupFieldProps {
  name: string;
  label?: ReactNode;
  options?: { label: string; value: unknown; tooltip?: ReactNode }[];
}

export function HookButtonGroupField({ name, label, options = [] }: HookButtonGroupFieldProps) {
  const {
    field: { value: fieldValue, onChange },
  } = useController({ name });

  return (
    <HookFieldWrapper name={name} label={label}>
      <ButtonGroup>
        {options.map(({ label, value, tooltip }, index) => {
          const button = (
            <Button key={index} $isActive={fieldValue === value} onClick={() => onChange(value)}>
              {label} {!!tooltip && <TooltipIndicator />}
            </Button>
          );

          if (tooltip) {
            return (
              <Tippy key={index} content={tooltip}>
                {button}
              </Tippy>
            );
          }

          return button;
        })}
      </ButtonGroup>
    </HookFieldWrapper>
  );
}

const TooltipIndicator = styled(InfoCircle)`
  font-size: ${({ theme }) => theme.textXs};
  margin-left: ${spacing1};
  margin-right: 0 !important;
  opacity: 0.65;
`;
