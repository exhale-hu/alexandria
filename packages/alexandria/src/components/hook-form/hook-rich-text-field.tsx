import { RichTextEditor } from "../form/index.js";
import { ReactNode } from "react";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { noop } from "lodash-es";
import { useControllerEx } from "./use-controller-ex.js";

export interface HookRichTextFieldProps<TInterim> {
  name: string;
  label?: ReactNode;
  onReady?: (editor: any) => void;
  config?: Record<string, unknown>;
  parse?: (value: string) => TInterim;
  transform?: (value: TInterim) => string;
  isVisibleTabs?: boolean;
}

export function HookRichTextField<TInterim = unknown>({
  name,
  label,
  config,
  onReady = noop,
  parse,
  isVisibleTabs = true,
  transform,
  ...props
}: HookRichTextFieldProps<TInterim>) {
  const {
    field: { value, onChange },
  } = useControllerEx({ name, parse, transform });

  return (
    <HookFieldWrapper name={name} label={label}>
      <RichTextEditor
        {...props}
        config={config}
        isVisibleTabs={isVisibleTabs}
        value={value}
        onChange={onChange}
        onReady={onReady}
      />
    </HookFieldWrapper>
  );
}
