import { useFormState } from "react-hook-form";
import { ComponentPropsWithoutRef, ReactNode } from "react";
import styled from "styled-components";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { Button } from "../inputs/button.js";
import { LoadingIcon } from "../utils/loading-icon.js";
import { noop } from "../utils/noop.js";

export interface HookSubmitButtonProps {
  children?: ReactNode;
  className?: string;
  onClick?: () => void;
}

export function HookSubmitButton({ children, className, onClick = noop }: HookSubmitButtonProps) {
  const { isSubmitting } = useFormState();

  return (
    <StyledSubmitButton
      $kind="primary"
      submit
      disabled={isSubmitting}
      $hideOtherIcons={isSubmitting}
      className={className}
      onClick={onClick}
    >
      {isSubmitting && <LoadingIcon />}
      {children}
    </StyledSubmitButton>
  );
}

const StyledSubmitButton = styled(Button)<ComponentPropsWithoutRef<typeof Button> & { $hideOtherIcons?: boolean }>`
  > ${StyledIconBase}:not(${LoadingIcon}) {
    ${({ $hideOtherIcons }) => $hideOtherIcons && "display: none"};
  }
`;
