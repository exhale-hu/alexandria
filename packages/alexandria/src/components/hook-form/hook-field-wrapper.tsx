import styled from "styled-components";
import { ComponentPropsWithoutRef, ReactNode } from "react";
import { useFormState } from "react-hook-form";
import { FieldLabel, fieldMaxWidth, FormRowBase, useUniqueId } from "../form/index.js";

export interface HookFieldWrapperProps extends ComponentPropsWithoutRef<"div"> {
  name: string;
  label?: ReactNode;
  $fullWidth?: boolean;
  children?: ReactNode;
}

export function HookFieldWrapper({ name, label, $fullWidth, children, id, ...props }: HookFieldWrapperProps) {
  const { errors, touchedFields, isSubmitted } = useFormState({ name });
  const fieldId = useUniqueId();
  const shouldShowError = !!errors[name] && (!!touchedFields[name] || isSubmitted);

  return (
    <HookStyledFieldWrapper $fullWidth={$fullWidth} {...props}>
      {label && <FieldLabel htmlFor={id || fieldId}>{label}</FieldLabel>}
      {children}
      {shouldShowError && <FieldWrapperErrorMessage>{errors[name]?.message || null}</FieldWrapperErrorMessage>}
    </HookStyledFieldWrapper>
  );
}

export const HookStyledFieldWrapper = styled(FormRowBase)<{ $fullWidth?: boolean }>`
  max-width: ${({ $fullWidth }) => ($fullWidth ? "none" : fieldMaxWidth)};

  ${({ $fullWidth }) => ($fullWidth ? "width: 100%;" : "")}
  & + &,
  ${FormRowBase} + & {
    margin-top: ${({ theme }) => theme.spacing6};
  }
`;

export function FieldWrapperErrorMessage(props: ComponentPropsWithoutRef<typeof FieldWrapperErrorMessageBody>) {
  // If the error is an object instead of a string, it means while this field doesn't have an error, one of it's
  // subfields (probably translation fields) has one. React would throw an error if we tried to render an object, so
  // skip this error and (hope) one of the subfields will render it's own error message.
  if (typeof props.children === "object") {
    return null;
  }
  return <FieldWrapperErrorMessageBody {...props} />;
}

const FieldWrapperErrorMessageBody = styled.div`
  color: ${({ theme }) => theme.red600};
  font-size: ${({ theme }) => theme.textSm};
  margin-top: ${({ theme }) => theme.spacing2};
`;
