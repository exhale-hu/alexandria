import { useController } from "react-hook-form";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { Switch, SwitchProps } from "../inputs/switch.js";
import { ReactNode } from "react";

export interface HookSwitchFieldProps extends Omit<SwitchProps, "value" | "onChange"> {
  name: string;
  label?: ReactNode;
}

export function HookSwitchField({ name, label, ...props }: HookSwitchFieldProps) {
  const controller = useController({ name });

  return (
    <HookFieldWrapper name={name} label={label} $fullWidth>
      <Switch {...props} value={controller.field.value} onChange={controller.field.onChange} />
    </HookFieldWrapper>
  );
}
