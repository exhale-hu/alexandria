import { FormProvider, Path, useForm, UseFormProps, UseFormReturn } from "react-hook-form";
import { ReactNode, useEffect } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { BaseSchema } from "yup";
import styled from "styled-components";
import { getDisplayableErrorMessage } from "../../utils/index.js";
import { merge } from "lodash-es";
import { noop } from "../utils/noop.js";

export interface HookFormProps<TValues> extends UseFormProps {
  onSubmit?: (values: TValues, methods: UseFormReturn) => void | Promise<void>;
  children?: ReactNode;
  yupSchema?: BaseSchema;
  className?: string;
  watchDefaultValues?: boolean;
  shouldUnregister?: boolean;
  shouldMerge?: boolean;
}

export function HookForm<TValues>({
  onSubmit = noop,
  children,
  yupSchema,
  className,
  watchDefaultValues = false,
  shouldUnregister = true,
  shouldMerge = false,
  ...props
}: HookFormProps<TValues>) {
  const methods = useForm({
    ...props,
    shouldUnregister,
    resolver: yupSchema ? yupResolver(yupSchema) : undefined,
  });

  useEffect(() => {
    if (watchDefaultValues) {
      methods.reset(props.defaultValues);
    }
  }, [props.defaultValues]);

  return (
    <FormProvider {...methods}>
      <StyledHookForm
        className={className}
        onSubmit={methods.handleSubmit((values) =>
          onSubmitHandler(values as Partial<TValues>, props.defaultValues as TValues, methods, shouldMerge, onSubmit)
        )}
      >
        {children}
      </StyledHookForm>
    </FormProvider>
  );
}

export const StyledHookForm = styled.form`
  width: 100%;
`;

async function onSubmitHandler<Values>(
  values: Values | Partial<Values>,
  defaultValues: Values,
  methods: UseFormReturn,
  shouldMerge: boolean,
  onSubmit?: (values: Values, methods: UseFormReturn) => void | Promise<void>
) {
  if (!onSubmit) return;

  try {
    const mergedValues = shouldMerge ? merge({}, defaultValues, values) : (values as Values);

    await onSubmit(mergedValues, methods);
  } catch (error) {
    console.error(error);
    methods.setError("submit" as Path<Values>, {
      type: "submit",
      message: getDisplayableErrorMessage(error as Error),
    });
  }
}
