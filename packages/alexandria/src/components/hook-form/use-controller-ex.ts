import { useController, UseControllerProps } from "react-hook-form";
import { identity } from "lodash-es";

export interface UseControllerExProps<TValue, TReturn = TValue> extends UseControllerProps {
  parse?: (value: TReturn) => TValue;
  transform?: (value: TValue) => TReturn;
}

export function useControllerEx<TValue, TReturn = TValue>({
  parse = identity,
  transform = identity,
  ...props
}: UseControllerExProps<TValue, TReturn>) {
  const returnValue = useController(props);

  return {
    ...returnValue,
    field: {
      ...returnValue.field,
      value: transform(returnValue.field.value as TValue),
      onChange: (value: TReturn) => returnValue.field.onChange(parse(value)),
    },
  };
}
