import styled from "styled-components";
// @ts-ignore
import { Accordion, AccordionWrapper } from "./accordion.js";
import { ReactNode } from "react";
import { spacing4, spacing8 } from "../../themes/index.js";

type SidebarProps = {
  children: ReactNode;
  header: ReactNode;
  profile: ReactNode;
};

export function Sidebar({ children, header, profile }: SidebarProps) {
  return (
    <Body>
      <StyledHeader>{header}</StyledHeader>
      <AccordionWrapper>
        <Accordion>{children}</Accordion>
      </AccordionWrapper>
      {profile}
    </Body>
  );
}

const Body = styled.div`
  display: flex;
  flex-direction: column;
  background: ${({ theme }) => theme.primary800};
  padding: ${spacing4};
  padding-right: ${spacing8};
  color: white;
  height: 100vh;
  width: fit-content;
`;

const StyledHeader = styled.div`
  &:hover {
    text-decoration: none;
  }

  & img {
    height: 6rem;
    margin-bottom: ${spacing4};
  }
`;
