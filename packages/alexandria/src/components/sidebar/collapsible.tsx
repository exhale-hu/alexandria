import { ComponentType, createContext, ReactNode, useContext, useMemo, useRef, useState } from "react";
import { animated, useSpring } from "react-spring";
import { AccordionContext } from "./accordion.js";
import { noop } from "lodash-es";
import styled from "styled-components";
import { useMeasure } from "../../hooks/index.js";
import { useUniqueId } from "../form/field-label.js";

export interface CollapsibleContextValue {
  isOpen: boolean;
  animate: boolean;

  setIsOpen(value: boolean, animate?: boolean): void;
}

export const CollapsibleContext = createContext<CollapsibleContextValue>({
  isOpen: false,
  animate: false,
  setIsOpen: noop,
});

export function Collapsible({ children }: { children?: ReactNode }) {
  const accordionContext = useContext(AccordionContext);
  const isInsideAccordion = accordionContext.exists;

  const collapsibleId = useUniqueId();
  const [localIsOpen, setLocalIsOpen] = useState(false);

  const isOpen = isInsideAccordion ? accordionContext.active === collapsibleId : localIsOpen;
  const animateRef = useRef(true);

  if (isInsideAccordion) {
    animateRef.current = accordionContext.animate;
  }

  const setIsOpen = useMemo(() => {
    return isInsideAccordion
      ? (value: boolean, animate = true) => accordionContext.setActive(value ? collapsibleId : "", animate)
      : (value: boolean, animate = true) => {
          animateRef.current = animate;
          setLocalIsOpen(value);
        };
  }, [isInsideAccordion]);

  const collapsibleContext = useMemo(
    () => ({
      isOpen,
      animate: animateRef.current,
      setIsOpen,
    }),
    [isOpen, animateRef.current, setIsOpen]
  );

  return <CollapsibleContext.Provider value={collapsibleContext}>{children}</CollapsibleContext.Provider>;
}

export function useCollapsible() {
  return useContext(CollapsibleContext);
}

export function CollapsibleContent({
  children,
  as,
  wrapperAs,
}: {
  children?: ReactNode;
  as?: ComponentType;
  wrapperAs?: ComponentType;
}) {
  const { isOpen, animate } = useCollapsible();
  const [{ height }, setBounds] = useState({ left: 0, top: 0, width: 0, height: 0 });

  const boundsRef = useRef<HTMLDivElement>(null);

  useMeasure(setBounds, boundsRef);

  const [style] = useSpring(
    () =>
      isOpen
        ? { to: [{ height, immediate: !animate }, { overflow: "visible" }] }
        : {
            to: [
              { overflow: "hidden" },
              {
                height: 0,
                immediate: !animate,
              },
            ],
          },
    [isOpen, height]
  );

  const OuterElement = as || CollapsibleWrapper;
  const WrapperElement = wrapperAs || "div";

  return (
    <OuterElement style={style}>
      <WrapperElement ref={boundsRef}>{children}</WrapperElement>
    </OuterElement>
  );
}

export const CollapsibleWrapper = styled(animated.div)`
  overflow: hidden;
`;
