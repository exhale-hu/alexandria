import {
  ComponentPropsWithoutRef,
  ComponentType,
  FormEvent,
  forwardRef,
  ForwardRefRenderFunction,
  MouseEvent,
  ReactNode,
  Ref,
  RefAttributes,
} from "react";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { Link, LinkProps, NavLink, useNavigate } from "react-router-dom";
import { LoadingIcon } from "../utils/loading-icon.js";
import styled, { css, CSSProperties } from "styled-components";
import { noop } from "lodash-es";
import { FontWeight, roundedNone, transition } from "../../themes/index.js";
import { useModal } from "../utils/modal-provider.js";
import { useFormikWrapperContext } from "../form/formik-wrapper.js";

const defaultButtonKind = css<ButtonProps>`
  border: 1px solid ${({ theme }) => theme.gray300};
  background-color: ${({ theme }) => theme.white};
  color: ${({ theme }) => theme.gray900};
  box-shadow: ${({ theme }) => theme.shadowSm};

  &:hover:not(:disabled) {
    color: ${({ theme }) => theme.gray500};
  }

  &:focus {
    box-shadow: ${({ theme }) => theme.shadowOutline};
  }

  &.active {
    background-color: ${({ theme }) => theme.primary100};
    color: ${({ theme }) => theme.primary900};

    &:hover:not(:disabled) {
      background-color: ${({ theme }) => theme.primary100};
    }
  }
`;

const linkButtonKind = css`
  background-color: transparent;
  color: ${({ theme }) => theme.primary600};
  padding: ${({ theme }) => theme.spacing1};
  border-color: transparent;

  &:hover {
    text-decoration: none;
  }

  &.active,
  &:hover:not(:disabled) {
    color: ${({ theme }) => theme.primary500};
  }
`;

export const buttonKinds = {
  default: defaultButtonKind,
  primary: css`
    border: 1px solid transparent;
    background-color: ${({ theme }) => theme.primary500};
    color: white;

    &:hover:not(:disabled) {
      background-color: ${({ theme }) => theme.primary400};
      color: white; // Override link hover color
    }
  `,
  secondary: css`
    border: 1px solid transparent;
    background-color: ${({ theme }) => theme.primary100};
    color: ${({ theme }) => theme.primary800};

    &:hover:not(:disabled) {
      background-color: ${({ theme }) => theme.primary100};
      color: ${({ theme }) => theme.primary800}; // Override link hover color
    }
  `,
  danger: css`
    ${defaultButtonKind};

    background-color: ${({ theme }) => theme.red600};
    color: white;
    border-color: transparent;

    &:hover:not(:disabled) {
      background-color: ${({ theme }) => theme.red500};
      color: white;
    }
  `,
  warning: css`
    ${defaultButtonKind};

    background-color: ${({ theme }) => theme.yellow100};
    color: ${({ theme }) => theme.yellow800};
    border-color: ${({ theme }) => theme.yellow300};

    &:hover:not(:disabled) {
      background-color: ${({ theme }) => theme.yellow200};
    }
  `,
  link: linkButtonKind,
  linkDanger: css`
    ${linkButtonKind};

    color: ${({ theme }) => theme.red600};

    &:hover:not(:disabled) {
      color: ${({ theme }) => theme.red500};
    }
  `,
  linkWarning: css`
    ${linkButtonKind};

    color: ${({ theme }) => theme.yellow300};

    &:hover:not(:disabled) {
      color: ${({ theme }) => theme.yellow400};
    }
  `,
  none: "",
};

export const buttonSizes = {
  default: css`
    padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing4};
    font-size: ${({ theme }) => theme.textSm};
    border-radius: ${({ theme }) => theme.roundedMd};
    line-height: ${({ theme }) => theme.leading5};
  `,
  xs: css`
    padding: ${({ theme }) => theme.spacing1} ${({ theme }) => theme.spacing1};
    font-size: ${({ theme }) => theme.textXs};
    border-radius: ${({ theme }) => theme.roundedSm};
  `,
  sm: css`
    padding: ${({ theme }) => theme.spacing1} ${({ theme }) => theme.spacing2};
    font-size: ${({ theme }) => theme.textXs};
    border-radius: ${({ theme }) => theme.rounded};
  `,
  lg: css`
    padding: ${({ theme }) => theme.spacing3} ${({ theme }) => theme.spacing8};
    letter-spacing: 1px;
    font-size: ${({ theme }) => theme.textBase};
    border-radius: ${({ theme }) => theme.roundedMd};
    line-height: ${({ theme }) => theme.leading6};
  `,
};

export interface ButtonProps {
  $size?: keyof typeof buttonSizes;
  $kind?: keyof typeof buttonKinds;
  $display?: CSSProperties["display"];
  $onlyIcon?: boolean;
  $isActive?: boolean;
  children?: ReactNode;
  submit?: boolean;
  to?: string;
  href?: string;
  asNavLink?: boolean;

  onClick?(event: MouseEvent<HTMLElement>): void;

  download?: string | boolean;
}

export interface ButtonComponentProps extends ButtonProps, Omit<ComponentPropsWithoutRef<"button">, "onClick"> {}

function ButtonComponent(
  { submit, to, onClick, asNavLink, ...props }: ButtonComponentProps,
  ref: Ref<HTMLAnchorElement>
) {
  const { dismissModal } = useModal();

  if (
    !submit &&
    (typeof to === "string" || (to && typeof (to as unknown as { pathname: string })?.pathname === "string"))
  ) {
    const Component = (asNavLink ? NavLink : Link) as ComponentType<LinkProps & RefAttributes<HTMLAnchorElement>>;

    return (
      <Component
        {...(props as Omit<LinkProps, "to">)}
        onClick={(e) => {
          dismissModal();
          onClick?.(e);
        }}
        to={to}
        ref={ref}
      />
    );
  } else if (props.href) {
    return (
      <a
        {...(props as Omit<LinkProps, "to">)}
        onClick={(e) => {
          dismissModal();
          onClick?.(e);
        }}
        ref={ref}
      />
    );
  } else {
    if (to) {
      return <LinkButton ref={ref} to={to} onClick={onClick} submit={submit} {...props} />;
    } else {
      return <ButtonBase ref={ref} to={to} onClick={onClick} submit={submit} {...props} />;
    }
  }
}

function LinkButtonComponent(
  { to, onClick, ...props }: Pick<ButtonComponentProps, "to" | "submit" | "onClick">,
  ref: Ref<HTMLAnchorElement>
) {
  const navigate = useNavigate();
  return (
    <ButtonBase
      {...props}
      ref={ref}
      onClick={(event) => {
        onClick?.(event);
        if (to) {
          navigate(to);
        }
      }}
    />
  );
}

function ButtonBaseComponent(
  { submit, onClick, ...props }: Pick<ButtonComponentProps, "to" | "submit" | "onClick">,
  ref: Ref<HTMLAnchorElement>
) {
  return (
    <button
      type={submit ? "submit" : "button"}
      onClick={(event) => {
        if (onClick) onClick(event);
      }}
      ref={ref as Ref<HTMLButtonElement>}
      {...props}
    />
  );
}

const ButtonBase = forwardRef(ButtonBaseComponent);

const LinkButton = forwardRef(LinkButtonComponent);

export const buttonStyles = css<ButtonProps>`
  font-weight: ${FontWeight.Medium};

  display: ${({ $display = "inline-flex" }) => $display};
  align-items: center;
  transition: ${({ theme }) => transition(["background-color", "opacity", "color"], theme.defaultTransitionTiming)};
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    opacity: 0.7;
  }

  &:hover {
    text-decoration: none;
  }

  ${StyledIconBase} {
    font-size: ${({ theme, $size = "default" }) => ($size === "sm" || $size === "xs" ? theme.textBase : theme.textXl)};
    margin-right: ${({ theme, $onlyIcon }) => !$onlyIcon && theme.spacing2};
  }

  ${({ $size = "default" }) => buttonSizes[$size]};
  ${({ $kind = "default" }) => buttonKinds[$kind]};
`;

export const Button = styled(
  forwardRef(ButtonComponent as ForwardRefRenderFunction<HTMLAnchorElement, ButtonComponentProps>)
).attrs<{ $isActive?: boolean }>(({ $isActive, className }) => ({
  className: $isActive ? (className || "") + " active" : className,
}))<ButtonProps>`
  ${buttonStyles};
`;

export interface SubmitButtonProps {
  children?: ReactNode;
  className?: string;
  onClick?: () => void;
}

export function SubmitButton({ children, className, onClick = noop }: SubmitButtonProps) {
  // Use submitForm(), so it works in nested forms
  const { isSubmitting, handleSubmit } = useFormikWrapperContext((context) => [
    context.isSubmitting,
    context.handleSubmit,
  ]);

  return (
    <StyledSubmitButton
      $kind="primary"
      submit
      disabled={isSubmitting}
      $hideOtherIcons={isSubmitting}
      className={className}
      onClick={(event) => {
        /* the expect type is a FormEvent, but I looked at the code and it works fine with click events too */
        handleSubmit(event as unknown as FormEvent<HTMLFormElement>);
        onClick();
      }}
    >
      {isSubmitting && <LoadingIcon />}
      {children}
    </StyledSubmitButton>
  );
}

const StyledSubmitButton = styled(Button)<{ $hideOtherIcons?: boolean }>`
  > ${StyledIconBase}:not(${LoadingIcon}) {
    ${({ $hideOtherIcons }) => $hideOtherIcons && "display: none"};
  }
`;

export const ButtonGroup = styled.div`
  display: flex;

  button:hover:not(:disabled):not(.active) {
    background-color: ${({ theme }) => theme.primary100};
  }

  button.active:hover:not(:disabled) {
    color: ${({ theme }) => theme.primary900};
  }

  button.active {
    position: relative;
  }

  button + button {
    border-top-left-radius: ${roundedNone};
    border-bottom-left-radius: ${roundedNone};
    border-left-width: 0;
  }

  button:not(:last-child) {
    border-top-right-radius: ${roundedNone};
    border-bottom-right-radius: ${roundedNone};
  }
`;
