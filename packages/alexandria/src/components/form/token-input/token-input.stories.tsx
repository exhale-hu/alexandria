import { TokenInput } from "./token-input.js";
import { useState } from "react";
import styled from "styled-components";
import { InputToken } from "./token-input-manager.js";

export default {
  title: "Form/TokenInput",
  component: TokenInput,
};

export function Template() {
  const [state, setState] = useState<InputToken[]>([{ type: "text", text: "", key: "a" }]);

  return (
    <div>
      <TokenInput
        tokens={state}
        onTokensChange={setState}
        onRenderToken={(token) => {
          if (token.type !== "token") return null;

          return (
            <TokenInputToken tabIndex={0} contentEditable={false} key={token.key}>
              {token.text}
            </TokenInputToken>
          );
        }}
      />
      <div>State:</div>
      <pre>{JSON.stringify(state, undefined, 2)}</pre>
    </div>
  );
}

const TokenInputToken = styled.div`
  background-color: lightgreen;
  display: inline-block;

  &:focus {
    background-color: chartreuse;
  }
`;
