import { FocusEvent, KeyboardEvent } from "react";
import useConstant from "use-constant";
import { Overwrite } from "utility-types";
import { noop } from "../../utils/noop.js";

export interface InputToken {
  key: string;
  type: string;
  text: string;
}

export class TokenInputManager {
  private static nextTokenId = 0;

  public static generateKey() {
    this.nextTokenId++;
    return this.nextTokenId.toString();
  }

  public _tokens: InputToken[] = [];
  public onTokensChange: (newValue: InputToken[]) => void = noop;

  public get tokens(): InputToken[] {
    return this._tokens;
  }

  public set tokens(value: InputToken[]) {
    this._tokens = value;
    this.onTokensChange(value);
  }

  public set activeIndex(value: number) {
    const index = Math.max(0, Math.min(value, this._tokens.length - 1));
    (this.element?.children[index] as HTMLElement).focus();
  }

  public get activeIndex() {
    let i = 0;
    for (const child of this.element ? Array.from(this.element.children) : []) {
      if (child.contains(document.activeElement)) {
        return i;
      }
      i++;
    }

    return 0;
  }

  private element: HTMLDivElement | null = null;

  public constructor() {
    this.handleFocus = this.handleFocus.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleRef = this.handleRef.bind(this);
  }

  public handleFocus(event: FocusEvent<HTMLDivElement>) {
    if (event.target !== event.currentTarget) {
      return;
    }

    (event.currentTarget.lastElementChild as HTMLElement).focus();
  }

  public handleKeyDown(event: KeyboardEvent<HTMLDivElement>) {
    switch (event.key) {
      case "ArrowLeft": {
        const { atStart } = this.getCaretPositionInfo();

        if (atStart || !this.isActiveTokenEditable()) {
          this.moveActiveTokenIndex(-1);
        }
        break;
      }
      case "ArrowRight": {
        const { atEnd } = this.getCaretPositionInfo();

        if (atEnd || !this.isActiveTokenEditable()) {
          this.moveActiveTokenIndex(1);
        }
        break;
      }
      case "Delete": {
        if (!this.isActiveTokenEditable()) {
          const caretIndex = this.activeIndex;
          this.removeToken(caretIndex);

          setTimeout(() => this.setActiveTokenIndex(caretIndex), 0);
        }
        break;
      }
      case "Backspace": {
        if (!this.isActiveTokenEditable() || this.getCaretPositionInfo().atStart) {
          const caretIndex = this.activeIndex;

          if (0 < caretIndex) {
            event.preventDefault();
            this.removeToken(caretIndex - 1);
          }
        }
        break;
      }
    }
  }

  public handleRef(element: HTMLDivElement | null) {
    this.element = element;
  }

  public getCaretPositionInfo() {
    const range = window.getSelection()?.getRangeAt(0);

    if (!range || !range.collapsed || !this.element?.contains(range.endContainer)) {
      return { atStart: false, atEnd: false };
    }

    return { atStart: range.startOffset === 0, atEnd: range.endOffset === range.endContainer.textContent?.length };
  }

  public addToken(token: Overwrite<InputToken, { key?: string }>, index?: number) {
    if (!token.key) {
      token.key = TokenInputManager.generateKey();
    }

    if (index === undefined) {
      this.tokens = [...this._tokens, token as InputToken];
    } else {
      const tokens = this._tokens.slice();
      tokens.splice(index, 0, token as InputToken);
      this.tokens = tokens;
    }
  }

  public replaceToken(token: Overwrite<InputToken, { key?: string }>, index: number) {
    if (!token.key) {
      token.key = TokenInputManager.generateKey();
    }

    const tokens = this._tokens.slice();
    tokens.splice(index, 1, token as InputToken);
    this.tokens = tokens;
  }

  public removeToken(index: number) {
    const tokens = this._tokens.slice();
    tokens.splice(index, 1);
    this.tokens = tokens;
  }

  public isActiveTokenEditable() {
    return this._tokens[this.activeIndex].type === "plain";
  }

  public moveActiveTokenIndex(direction: -1 | 1) {
    this.setActiveTokenIndex(Math.max(0, Math.min(this.activeIndex + direction, this._tokens.length - 1)));
  }

  public setActiveTokenIndex(index: number) {
    (this.element?.children[index] as HTMLElement).focus();
  }
}

export function useTokenInput(tokens: InputToken[], onTokensChange: (newTokens: InputToken[]) => void) {
  const manager = useConstant(() => new TokenInputManager());
  manager.onTokensChange = onTokensChange;
  manager._tokens = tokens;
  return manager;
}
