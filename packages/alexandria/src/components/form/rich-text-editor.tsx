import { useCallback, useState } from "react";
import { CKEditor, Editor, EventInfo, FileLoaderInterface } from "@ckeditor/ckeditor5-react";
import DecoupledEditor from "@ckeditor/ckeditor5-build-decoupled-document";
import { noop } from "lodash-es";
import { FieldProps } from "formik";
import { size48, spacing1, spacing2 } from "../../themes/index.js";
import styled from "styled-components";
import { firstValueFrom, Observable, Subject } from "rxjs";
import { client } from "@litbase/client";
import { createFileEntryFromFile, FileEntry } from "./file-upload/index.js";
import { map, takeUntil } from "rxjs/operators";
import { TransitionStatus } from "../../hooks/use-status-transition.js";

export const CKModules = {
  CKEditor: CKEditor,
  DecoupledEditor: DecoupledEditor,
};

export type { Editor, EventInfo, FileLoaderInterface };

export interface RichTextEditorProps extends CkeditorComponentProps {
  isVisibleTabs?: boolean;
}

export function RichTextEditor({ onChange = noop, value = "", config, ...props }: RichTextEditorProps) {
  const [mode, setMode] = useState<"editor" | "code">("editor");
  return (
    <Col>
      {!!props.isVisibleTabs && (
        <Row>
          <Tab $isActive={mode === "editor"} onClick={() => setMode("editor")}>
            Szerkesztő
          </Tab>
          <Tab $isActive={mode === "code"} onClick={() => setMode("code")}>
            Forráskód
          </Tab>
        </Row>
      )}

      {mode === "editor" ? (
        <EditorWrapper>
          <CkeditorComponent {...props} value={value} onChange={onChange} config={config} />
        </EditorWrapper>
      ) : (
        <Textarea value={value} onChange={(e) => onChange?.(e.target.value)} />
      )}
    </Col>
  );
}

interface CkeditorComponentProps {
  value?: string;
  onChange?: (value: string) => void;
  config?: Record<string, unknown>;
  onReady?: (editor: Editor) => void;
  getDownloadUrl?: (fileName: string) => string;

  onUploadFile?(file: File): Observable<FileEntry>;
}

function CkeditorComponent({
  value = "",
  onChange = noop,
  config,
  onReady = noop,
  getDownloadUrl = defaultGetDownloadUrl,
  onUploadFile = defaultOnUploadFile,
}: CkeditorComponentProps) {
  const editorOnReady = useCallback(
    (editor: Editor) => {
      editor.ui
        .getEditableElement()
        .parentElement?.insertBefore(editor.ui.view.toolbar.element, editor.ui.getEditableElement());

      editor.plugins.get("FileRepository").createUploadAdapter = (loader) =>
        new GenericCkeditorUploadAdapter({
          loader,
          getContentUrl: (fileEntry) => getDownloadUrl(fileEntry.contentUrl || ""),
          onUploadFile,
        });

      onReady(editor);
    },
    [onReady]
  );

  return (
    <CKEditor
      config={config}
      editor={DecoupledEditor}
      data={value}
      onChange={(event: EventInfo, editor: Editor) => onChange(editor.getData())}
      onReady={editorOnReady}
    />
  );
}

function defaultGetDownloadUrl(fileName: string) {
  if (fileName.slice(0, 4) === "http") {
    return fileName;
  }
  return "/uploads/" + fileName;
}

function defaultOnUploadFile(file: File) {
  const fileEntry = createFileEntryFromFile(file);

  return client.uploadFile(file).pipe(
    map(({ status, ...results }) => ({
      ...fileEntry,
      ...results,
      status: status as unknown as TransitionStatus,
    }))
  );
}

interface RichTextEditorFieldProps extends FieldProps<string>, CkeditorComponentProps {}

export function RichTextEditorField({ field, form, config, ...props }: RichTextEditorFieldProps) {
  return (
    <RichTextEditor
      {...props}
      config={config}
      value={field.value}
      onChange={(newValue) => form.setFieldValue(field.name, newValue)}
    />
  );
}

/* eslint-disable @typescript-eslint/no-explicit-any */
function ConvertDivAttributes(editor: any) {
  // Allow <div> elements in the model.
  editor.model.schema.register("div", {
    allowWhere: "$block",
    allowContentOf: "$root",
  });

  // Allow <div> elements in the model to have all attributes.
  editor.model.schema.addAttributeCheck((context: any) => {
    if (context.endsWith("div")) {
      return true;
    }
  });

  // The view-to-model converter converting a view <div> with all its attributes to the model.
  editor.conversion.for("upcast").elementToElement({
    view: "div",
    model: (viewElement: any, { writer: modelWriter }: any) => {
      return modelWriter.createElement("div", viewElement.getAttributes());
    },
  });

  // The model-to-view converter for the <div> element (attributes are converted separately).
  editor.conversion.for("downcast").elementToElement({
    model: "div",
    view: "div",
  });

  // The model-to-view converter for <div> attributes.
  // Note that a lower-level, event-based API is used here.
  editor.conversion.for("downcast").add((dispatcher: any) => {
    dispatcher.on("attribute", (evt: any, data: any, conversionApi: any) => {
      // Convert <div> attributes only.
      if (data.item.name != "div") {
        return;
      }

      const viewWriter = conversionApi.writer;
      const viewDiv = conversionApi.mapper.toViewElement(data.item);

      // In the model-to-view conversion we convert changes.
      // An attribute can be added or removed or changed.
      // The below code handles all 3 cases.
      if (data.attributeNewValue) {
        viewWriter.setAttribute(data.attributeKey, data.attributeNewValue, viewDiv);
      } else {
        viewWriter.removeAttribute(data.attributeKey, viewDiv);
      }
    });
  });
}

const Textarea = styled.textarea`
  width: 30rem;
  min-height: 12rem;
  border: 1px solid #cacaca;
`;

const Tab = styled.div<{ $isActive: boolean }>`
  border-top-right-radius: ${({ theme }) => theme.rounded};
  border-top-left-radius: ${({ theme }) => theme.rounded};
  border: 1px solid #cacaca;
  border-bottom: none;
  height: fit-content;
  margin-top: auto;
  cursor: pointer;
  transition: background-color 0.3s, padding-top 0.3s, padding-bottom 0.3s;
  padding: ${({ $isActive }) => ($isActive ? spacing2 : `${spacing1} ${spacing2}`)};
  color: ${({ $isActive }) => ($isActive ? "black" : "#9a9a9a")};
  ${({ $isActive }) => !$isActive && "background: #e2e2e2;"}
`;

const Row = styled.div`
  display: flex;
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

const EditorWrapper = styled.div`
  display: flex;
  width: 100%;
  height: auto;
  color: black !important;
  flex-direction: column;
  background: white;

  > div:nth-child(2) {
    border: 1px solid #cacaca;
    border-top: none;
    display: flex;
    flex-direction: column;
  }

  .ck-editor {
    width: 100% !important;
  }

  .ck-content {
    min-height: ${size48};
  }

  .hashtag-row {
    box-shadow: ${({ theme }) => theme.shadow};
  }

  .ck-button.list-blockquote-preview {
    width: auto;
  }
`;

export interface GenericCkeditorUploadAdapterOptions {
  loader: FileLoaderInterface;
  getContentUrl?: (fileEntry: FileEntry) => string;

  onUploadFile(file: File): Observable<FileEntry>;
}

export class GenericCkeditorUploadAdapter {
  public cancel$: Subject<boolean>;
  public getContentUrl: (fileEntry: FileEntry) => string;

  constructor(public options: GenericCkeditorUploadAdapterOptions) {
    this.cancel$ = new Subject();
    this.getContentUrl = options.getContentUrl || defaultGetContentUrl;
  }

  // Starts the upload process.
  async upload() {
    const file = await this.options.loader.file;

    if (file) {
      return await firstValueFrom(
        this.options.onUploadFile(file).pipe(
          takeUntil(this.cancel$),
          map((elem) => {
            this.options.loader.uploadTotal = 100;
            this.options.loader.uploaded = elem.progress;
            return { default: this.getContentUrl(elem) };
          })
        )
      );
    }
  }

  // Aborts the upload process.
  abort() {
    this.cancel$.next(true);
  }
}

function defaultGetContentUrl(file: FileEntry) {
  const fileName = file.contentUrl || "";

  if (fileName.slice(0, 4) === "http") {
    return fileName;
  }
  return "/uploads/" + fileName;
}
