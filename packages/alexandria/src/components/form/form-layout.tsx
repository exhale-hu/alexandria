import { spacing8, spacing10, spacing6, size72 } from "../../themes/index.js";
import styled from "styled-components";
import { Card } from "../layout/card.js";

export const fieldMaxWidth = "34rem";

export const FormRowBase = styled.div``;

interface FieldWrapperProps {
  $fullWidth?: boolean;
}

export const StyledFieldWrapper = styled(FormRowBase)<FieldWrapperProps>`
  max-width: ${({ $fullWidth }) => ($fullWidth ? "none" : fieldMaxWidth)};

  ${({ $fullWidth }) => ($fullWidth ? "width: 100%;" : "")}
  & + &,
  ${FormRowBase} + & {
    margin-top: ${({ theme }) => theme.spacing6};
  }
`;

export const FormColumns = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(calc(${fieldMaxWidth} / 2), 1fr));
  gap: ${spacing10};

  ${StyledFieldWrapper} {
    max-width: none;
  }

  ${StyledFieldWrapper} + & {
    margin-top: ${({ theme }) => theme.spacing6};
  }
`;

export const FieldRow = styled(FormRowBase)`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(0.1rem, 1fr));
  gap: ${spacing6};

  ${StyledFieldWrapper}:first-child {
    margin-top: ${({ theme }) => theme.spacing6};
  }
`;

export const GridColumns = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(${size72}, 1fr));
  gap: ${spacing10};

  ${Card} + &, & + &, & + ${Card} {
    margin-top: ${spacing8};
  }
`;
