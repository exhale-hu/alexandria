import { ComponentProps, CSSProperties, forwardRef, ReactElement, ReactNode, Ref, useRef } from "react";
import { ErrorMessage, FastField, Field, FieldAttributes, FormikContextType, getIn } from "formik";
import { useAction } from "use-action";
import { useFormikWrapperContext } from "./formik-wrapper.js";
import { FieldLabel, useUniqueId } from "./field-label.js";
import { currentLanguage$ } from "../../services/translation-service.js";
import { FieldFlagIcon } from "./field-flag-icon.js";
import { cardNoPaddingBottom, cardNoPaddingHorizontal } from "../layout/card.js";
import { StyledFieldWrapper } from "./form-layout.js";
import { getSelfAndAncestorFields } from "../../utils/index.js";
import { useObservable } from "@litbase/use-observable";
import { spacing4 } from "../../themes/index.js";
import styled from "styled-components";

export type FieldWrapperProps = FieldAttributes<any> & {
  label?: ReactNode;
  $fullWidth?: boolean;
  className?: string;
  style?: CSSProperties;
  slow?: boolean;
  deps?: (context: FormikContextType<any>) => unknown[];
  [key: string]: unknown;
};

export function FieldWrapperComponent(props: FieldWrapperProps, ref: Ref<HTMLDivElement>) {
  const { name, disabled, label, $fullWidth = false, className, style, slow, deps } = props;
  const { isSubmitting } = useFormikWrapperContext((context) => [context.isSubmitting]);
  const context = useFormikWrapperContext((context) => (deps ? deps(context as FormikContextType<unknown>) : []));
  const fieldId = useUniqueId(name);
  const haveDepsChangedRef = useRef<boolean | null>(null);

  useAction(() => {
    // Set to false on initial render. If null, this is the initial render.
    haveDepsChangedRef.current = null !== haveDepsChangedRef.current;
  }, [context]);

  // Based on: https://github.com/formium/formik/issues/1188#issuecomment-603463915
  function shouldComponentUpdate(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    nextProps: Record<string, any>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    currentProps: Record<string, any>
  ) {
    const haveDepsChanged = haveDepsChangedRef.current;
    haveDepsChangedRef.current = false;

    return (
      haveDepsChanged ||
      nextProps.name !== currentProps.name ||
      nextProps.required !== currentProps.required ||
      nextProps.disabled !== currentProps.disabled ||
      nextProps.readOnly !== currentProps.readOnly ||
      nextProps.formik.isSubmitting !== currentProps.formik.isSubmitting ||
      Object.keys(nextProps).length !== Object.keys(currentProps).length ||
      getIn(nextProps.formik.values, currentProps.name) !== getIn(currentProps.formik.values, currentProps.name) ||
      getIn(nextProps.formik.errors, currentProps.name) !== getIn(currentProps.formik.errors, currentProps.name) ||
      getIn(nextProps.formik.touched, currentProps.name) !== getIn(currentProps.formik.touched, currentProps.name)
    );
  }

  const FieldComponent = slow ? Field : FastField;

  return (
    <StyledFieldWrapper $fullWidth={$fullWidth} ref={ref} className={className} style={style}>
      {label && <FieldLabel htmlFor={props.id || fieldId}>{label}</FieldLabel>}
      <FieldComponent
        {...(props as FieldAttributes<unknown>)}
        id={props.id || fieldId}
        name={name}
        disabled={disabled || isSubmitting}
        shouldUpdate={shouldComponentUpdate}
      />
      {getSelfAndAncestorFields(name).map((subname) => (
        <ErrorMessage key={subname} name={subname} component={FieldWrapperErrorMessage} />
      ))}
    </StyledFieldWrapper>
  ) as ReactElement;
}

export const FieldWrapper = forwardRef(FieldWrapperComponent);

function FieldWrapperErrorMessage(props: ComponentProps<typeof FieldWrapperErrorMessageBody>) {
  // If the error is an object instead of a string, it means while this field doesn't have an error, one of it's
  // subfields (probably translation fields) has one. React would throw an error if we tried to render an object, so
  // skip this error and (hope) one of the subfields will render it's own error message.
  if (typeof props.children === "object") {
    return null;
  }

  return <FieldWrapperErrorMessageBody {...props} />;
}

export const FieldWrapperErrorMessageBody = styled.div`
  color: ${({ theme }) => theme.red600};
  font-size: ${({ theme }) => theme.textSm};
  margin-top: ${({ theme }) => theme.spacing2};
`;

export function TranslatableFieldWrapper(props: FieldWrapperProps) {
  const [currentLanguage] = useObservable(() => currentLanguage$, "hu");

  return (
    <FieldWrapper
      {...props}
      key={`${props.name}.${currentLanguage}`}
      name={`${props.name}.${currentLanguage}`}
      label={
        props.label && (
          <>
            {props.label} <FieldFlagIcon />
          </>
        )
      }
    />
  );
}

export const NoPaddingFieldWrapper = styled(FieldWrapper)<{ $bottomPadding?: boolean }>`
  ${cardNoPaddingHorizontal};
  ${({ $bottomPadding }) => !$bottomPadding && cardNoPaddingBottom};

  // Offset title margin
  margin-top: -${spacing4};
  max-width: none;

  .ck.ck-toolbar {
    border-left-width: 0;
    border-right-width: 0;
    border-top-width: 0;
  }

  .ck.ck-content {
    background: transparent;
  }

  .ck.ck-content:not(.ck-focused) {
    border-left-color: transparent !important;
    border-right-color: transparent !important;
    border-bottom-color: transparent !important;
    border-top-color: ${({ theme }) => theme.gray200} !important;
  }
`;
