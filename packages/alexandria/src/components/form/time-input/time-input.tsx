import styled from "styled-components";
import { Fragment } from "react";
import { useCallback, useEffect, useRef, useState } from "react";
import { useGesture } from "@use-gesture/react";
import { getAnimationValues, getHourValue, getMinuteValue, TimeInputMode, TimeValue } from "./time-input-helpers.js";
import { animated, config, to, useSpring, useTransition } from "react-spring";
import { ClockFace } from "./clock-face.js";
import { useEffectWithLast } from "../../../hooks/index.js";

export interface TimeInputProps {
  value?: TimeValue;

  onChange?(value: TimeValue): void;

  mode?: TimeInputMode;

  onModeChange?(newMode: TimeInputMode): void;
}

const StyledTimeInput = styled.div`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
`;

const ClockFaceContainer = styled.div`
  position: relative;
  border-radius: 50%;
  background: #333;

  // Temporary
  width: 200px;
  height: 200px;
`;

const StyledClockFace = styled(animated.div)`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  color: white;
  user-select: none;
`;

const BackgroundClockFace = styled(StyledClockFace)`
  clip-path: url("#clockClip");
  background-color: #15a6d9;
  pointer-events: none;
  color: #333;
`;

const TimeText = styled.div`
  text-align: center;
  font-size: 2.3em;
  letter-spacing: 0.05em;
`;

const TimeTextPart = styled.span<{ isActive: boolean }>`
  font-weight: ${(props) => (props.isActive ? "bold" : "normal")};
  color: ${(props) => (props.isActive ? "#0093ff" : "unset")};
  cursor: pointer;
`;

export function TimeInput(props: TimeInputProps) {
  const [stateValue, setStateValue] = useState<TimeValue>([0, 0]);
  const [stateMode, setStateMode] = useState<TimeInputMode>("hour");

  const value = props.value || stateValue;
  const onChange = props.onChange || setStateValue;

  const mode = props.mode || stateMode;
  const onModeChange = props.onModeChange || setStateMode;

  useEffect(() => {
    if (!props.onChange && props.value) setStateValue(value);
  }, [props.value]);

  useEffect(() => {
    if (!props.onModeChange && props.mode) setStateMode(mode);
  }, [props.mode]);

  const clockFaceRef = useRef<HTMLDivElement>(null);
  const clockBoundsRef = useRef({ left: 0, top: 0, width: 0, height: 0 });
  useGesture(
    {
      onDrag({ xy, first, last }) {
        if (!clockFaceRef.current) return;

        if (first) clockBoundsRef.current = clockFaceRef.current.getBoundingClientRect();

        if ("hour" === mode) {
          const hour = getHourValue(xy, clockBoundsRef.current);

          if (hour !== value[0]) onChange([hour, value[1]]);
        } else {
          const minute = getMinuteValue(xy, clockBoundsRef.current);

          if (minute !== value[1]) onChange([value[0], minute]);
        }

        if (last && mode === "hour") onModeChange("minute");
      },
    },
    { target: clockFaceRef }
  );

  const [{ rotation, handLength, handTopRadius, handTopOffset }, set] = useSpring(() => ({
    ...getAnimationValues(mode, value),
    config: config.gentle,
  }));

  useEffectWithLast(
    ([lastMode]) => {
      set({
        ...getAnimationValues(mode, value, rotation),
        immediate: lastMode === mode,
      });
    },
    [mode, value]
  );

  const transform = rotation.to((v) => `rotate(${v}, 0.5, 0.5)`);
  const handTransform = to([rotation, handLength], (currentRotation, currentHandLength) => {
    return `rotate(${currentRotation}, 0.5, 0.5) scale(1, ${currentHandLength})`;
  });

  const transitions = useTransition(mode, {
    from: { opacity: 0, transform: "scale(0.7)" },
    enter: { opacity: 1, transform: "scale(1)" },
    leave: { opacity: 0, transform: "scale(0.7)" },
    initial: null,
    config: config.slow,
  });

  const onHourClick = useCallback(() => onModeChange("hour"), []);
  const onMinuteClick = useCallback(() => onModeChange("minute"), []);

  return (
    <StyledTimeInput>
      <svg width="0" height="0" viewBox="0 0 100 100">
        <animated.clipPath id="clockClip" clipPathUnits="objectBoundingBox">
          <animated.circle cx="0.5" cy={handTopOffset} r={handTopRadius} transform={transform} />
          <animated.rect x="0.495" y="0.1" width=".01" height="0.4" transform={handTransform} />
          <animated.circle cx="0.5" cy="0.5" r=".03" transform={transform} />
        </animated.clipPath>
      </svg>
      <TimeText>
        <TimeTextPart isActive={mode === "hour"} onClick={onHourClick}>
          {value[0].toString().padStart(2, "0")}
        </TimeTextPart>
        :
        <TimeTextPart isActive={mode === "minute"} onClick={onMinuteClick}>
          {value[1].toString().padStart(2, "0")}
        </TimeTextPart>
      </TimeText>
      <ClockFaceContainer ref={clockFaceRef}>
        {transitions((style, item, { key }) => (
          <Fragment key={key}>
            <ClockFace component={StyledClockFace} mode={item} style={style} minute={value[1]} />
            <ClockFace component={BackgroundClockFace} mode={item} style={style} minute={value[1]} />
          </Fragment>
        ))}
      </ClockFaceContainer>
    </StyledTimeInput>
  );
}
