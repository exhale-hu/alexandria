import {
  FieldHelperProps,
  FieldInputProps,
  FieldMetaProps,
  Formik,
  FormikConfig,
  FormikContextType,
  FormikHelpers,
  FormikValues,
  getIn,
  useFormikContext,
} from "formik";
import { createContext, ReactNode, useContext, useEffect, useMemo, useRef } from "react";
import { unstable_batchedUpdates } from "react-dom";
import useConstant from "use-constant";
import { BehaviorSubject, combineLatest, Observable } from "rxjs";
import { filter } from "rxjs/operators";
import { useAction } from "use-action";
import { useSubscription } from "@litbase/use-observable";
import { getDisplayableErrorMessage } from "../../utils/index.js";
import { useForceUpdate } from "../../hooks/index.js";
import { useRefValue } from "../../hooks/index.js";
import { Overwrite } from "utility-types";

async function onSubmitHandler<Values>(
  values: Values,
  formikHelpers: FormikHelpers<Values>,
  onSubmit: FormikConfig<Values>["onSubmit"]
) {
  try {
    await onSubmit(values, formikHelpers);
  } catch (error) {
    console.error(error);
    formikHelpers.setFieldError("submit", getDisplayableErrorMessage(error as Error));
  }
}

export function FormikWrapper<Values extends FormikValues = FormikValues, ExtraProps = unknown>({
  onSubmit,
  children,
  ...props
}: Overwrite<FormikConfig<Values>, { children: ReactNode }> & ExtraProps) {
  return (
    // @ts-ignore
    <Formik onSubmit={(values, formikHelpers) => onSubmitHandler(values, formikHelpers, onSubmit)} {...props}>
      <FormikContextHelper>{children}</FormikContextHelper>
    </Formik>
  );
}

const FormikWrapperContext = createContext({
  subject$: new BehaviorSubject({} as FormikContextType<Record<string, unknown>>),
  deferredSubject$: new BehaviorSubject<void>(undefined),
});

function FormikContextHelper({ children }: { children: ReactNode }) {
  const context = useFormikContext<Record<string, unknown>>();
  const value = useConstant(() => ({
    subject$: new BehaviorSubject<FormikContextType<Record<string, unknown>>>(context),
    deferredSubject$: new BehaviorSubject<void>(undefined),
  }));

  useEffect(
    () => () => {
      value.subject$.complete();
      value.deferredSubject$.complete();
    },
    []
  );

  useAction(() => {
    value.subject$.next(context);
  }, [context]);

  // Wrapped in useEffect instead of useAction to prevent
  // "Warning: Cannot update a component from inside the function body of a different component."
  useEffect(() => {
    unstable_batchedUpdates(() => {
      value.deferredSubject$.next();
    });
  }, [context]);

  return <FormikWrapperContext.Provider value={value}>{children}</FormikWrapperContext.Provider>;
}

type FormikWrapperContextCollector<T> = (context: FormikContextType<T>) => unknown[];

export function useFormikWrapperContext<T>(collect: FormikWrapperContextCollector<T>) {
  const context = useContext(FormikWrapperContext);
  const lastInputsRef = useRef<unknown[]>([]);
  const valueRef = useRef(context.subject$.value as FormikContextType<T>);
  const lastReturnedValueRef = useRefValue(valueRef.current as FormikContextType<T>);
  const forceUpdate = useForceUpdate();
  const collect$ = useConstant(() => new BehaviorSubject<FormikWrapperContextCollector<T>>(collect));

  useAction(() => {
    collect$.next(collect);
  }, [collect]);

  useSubscription(
    () =>
      combineLatest([context.subject$ as Observable<FormikContextType<T>>, collect$])
        .pipe(
          filter(([currentContext, currentCollect]) => {
            const currentInputs = currentCollect(currentContext as FormikContextType<T>);
            const lastInputs = lastInputsRef.current;
            lastInputsRef.current = currentInputs;

            if (currentInputs.length !== lastInputs.length) {
              return true;
            }

            for (let i = 0, length = currentInputs.length; i < length; i++) {
              if (currentInputs[i] !== lastInputs[i]) {
                return true;
              }
            }

            return false;
          })
        )
        .subscribe(([currentContext]) => {
          valueRef.current = currentContext;
        }),
    []
  );

  useSubscription(
    () =>
      context.deferredSubject$.subscribe(() => {
        if (valueRef.current !== lastReturnedValueRef.current) {
          forceUpdate();
        }
      }),
    []
  );

  return valueRef.current;
}

export function useFastField<T = string>(name: string) {
  const context = useFormikWrapperContext<Record<string, unknown>>((context) => [
    getIn(context.values, name),
    getIn(context.errors, name),
    getIn(context.touched, name),
  ]);

  return useMemo(
    () =>
      [context.getFieldProps<T>({ name }), context.getFieldMeta<T>(name), context.getFieldHelpers<T>(name)] as [
        FieldInputProps<T>,
        FieldMetaProps<T>,
        FieldHelperProps<T>
      ],
    [context, name]
  );
}
