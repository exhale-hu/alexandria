import { SelectInput } from "./select-input.js";
import { useState } from "react";

export default {
  title: "Form/SelectInput",
  component: SelectInput,
};

export function Template() {
  const [value, setValue] = useState<string | null>(null);
  const options = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven"];

  return <SelectInput value={value} onChange={setValue} options={options} throttleMs={0} />;
}
