import {
  ComponentPropsWithRef,
  CSSProperties,
  forwardRef,
  KeyboardEvent,
  MouseEvent,
  ReactNode,
  Ref,
  useCallback,
  useState,
} from "react";
import styled from "styled-components";
import { useSearchResults } from "./use-search-results.js";
import { getValueOf } from "@litbase/core";
import { Observable } from "rxjs";
import { spacing2 } from "../../../themes/index.js";
import { useSelectInput } from "./use-select-input.js";
import { ChevronDown, X } from "@styled-icons/boxicons-regular";

type BaseProps = ComponentPropsWithRef<"div">;

export interface SelectInputInputProps<T> extends BaseProps {
  onSearchTextChange(value: string): void;

  searchText: string;

  onSubmit(): void;

  isLoading?: boolean;
  clearable?: boolean;
  setSelectedValue: (value: T | null) => void;
  selectedValue: T | null;
}

export interface SelectInputResultProps<T> extends BaseProps {
  $active: boolean;
  entry: T;
}

export type SelectInputListProps = BaseProps;

export interface SelectInputProps<T> {
  value: T | null;

  onChange(value: T | null): void;

  getValueText?(value: T): string;

  getValueKey?(value: T): string;

  getInputElement?(props: SelectInputInputProps<T>): ReactNode;

  getListElement?(props: SelectInputListProps): ReactNode;

  getResultElement?(props: SelectInputResultProps<T>): ReactNode;

  options?: T[];
  optionsCallback?: (search: string) => Promise<T[]> | Observable<T[]> | T[];
  throttleMs?: number;
  throttleLeading?: boolean;
  className?: string;
  style?: CSSProperties;
  placeholder?: string;
  clearable?: boolean;
}

export function SelectInput<T>({
  value,
  onChange,
  options,
  optionsCallback,
  throttleMs,
  throttleLeading,
  getValueText = (value) => String(getValueOf(value)),
  getValueKey = (value) => String(getValueOf(value)),
  getInputElement = (props) => <DefaultSelectInputInput {...props} />,
  getListElement = (props) => <SelectInputResultList {...props} />,
  getResultElement = (props) => <DefaultSelectInputResult {...props} />,
  className,
  style,
  placeholder,
  clearable = true,
}: SelectInputProps<T>) {
  const [searchText, setSearchText] = useState("");
  const [searchResults, isLoading] = useSearchResults<T>({
    searchText,
    options,
    optionsCallback,
    throttleMs,
    throttleLeading,
  });

  const {
    x,
    y,
    strategy,
    reference,
    floating,
    getReferenceProps,
    getFloatingProps,
    getItemProps,
    activeIndex,
    setActiveIndex,
    open,
    setOpen,
    listRef,
  } = useSelectInput();

  function onSubmit() {
    if (null === activeIndex) {
      return;
    }

    setSearchText("");
    setOpen(false);
    onChange(searchResults[activeIndex as number]);
  }

  function onSearchTextChange(newValue: string) {
    setSearchText(newValue);
    setActiveIndex(0);
    setOpen(true);
  }

  return (
    <SelectInputContainer className={className} style={style} ref={reference} {...getReferenceProps()}>
      {/* Input field */}
      {getInputElement({
        onSearchTextChange,
        searchText,
        onSubmit,
        placeholder: null != value ? getValueText(value) : placeholder,
        isLoading,
        "aria-autocomplete": "list",
        setSelectedValue: onChange,
        selectedValue: value,
        clearable,
      })}
      {/* Search results window */}
      {getListElement({
        ...getFloatingProps(),
        hidden: !open,
        ref: floating,
        style: {
          position: strategy,
          top: (y || 0) - 8,
          left: x || 0,
        },
        children: (open ? searchResults : []).map((entry, index) => {
          const key = getValueKey(entry);
          const isActive = activeIndex === index;

          return getResultElement({
            ...getItemProps(),
            key,
            id: key,
            role: "option",
            "aria-selected": isActive,
            $active: isActive,
            ref(node: HTMLElement | null) {
              listRef.current[index] = node;
            },
            onClick(event: MouseEvent) {
              event.preventDefault();

              onChange(entry);
              setSearchText("");
              setOpen(false);
            },

            entry,
            children: getValueText(entry),
          });
        }),
      })}
    </SelectInputContainer>
  );
}

const DefaultSelectInputInput = forwardRef(
  (
    {
      searchText,
      onSearchTextChange,
      onSubmit,
      isLoading,
      clearable,
      selectedValue,
      setSelectedValue,
      ...props
    }: SelectInputInputProps<any>,
    ref: Ref<HTMLElement>
  ) => {
    const onKeyDown = useCallback(
      (event: KeyboardEvent<HTMLInputElement>) => {
        props.onKeyDown?.(event);

        if (event.key === "Enter") {
          event.preventDefault();
          onSubmit();
        }
      },
      [onSubmit]
    );

    return (
      <DefaultSelectInputInputContainer>
        <StyledDefaultSelectInputInput
          {...props}
          value={searchText}
          onChange={(event) => onSearchTextChange(event.currentTarget.value)}
          type="text"
          onKeyDown={onKeyDown}
          ref={ref as Ref<HTMLInputElement>}
        />
        {clearable && null != selectedValue && (
          <button onClick={() => setSelectedValue(null)}>
            <X />
          </button>
        )}
        <DownButton onClick={onFocusButtonClick}>
          <ChevronDown />
        </DownButton>
      </DefaultSelectInputInputContainer>
    );
  }
);

function onFocusButtonClick(event: MouseEvent) {
  event.currentTarget.parentNode?.querySelector("input")?.focus();
}

const DownButton = styled.button`
  padding-left: ${spacing2};
  padding-right: 0;
  border-left: 1px solid #666 !important;

  svg {
    margin-top: -0.3em;
  }
`;

const DefaultSelectInputInputContainer = styled.div`
  position: relative;
  border-radius: ${({ theme }) => theme.roundedMd};
  border: 1px solid ${({ theme }) => theme.gray300};
  padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing3};
  font-size: ${({ theme }) => theme.textSm};
  display: flex;
  width: 100%;
  background-color: white;

  button {
    opacity: 0.6;
    border: none;
    background: none;
    font-size: 1.5em;
    line-height: 1;
    transition: opacity 250ms ease-in-out;

    svg {
      margin-top: -0.3em;
    }
  }

  &:focus-within button {
    opacity: 1;
  }
`;

const StyledDefaultSelectInputInput = styled.input`
  width: 100%;
  background-color: transparent;
  border: none;
  padding: 0;
  outline: 0;

  &::placeholder {
    color: ${({ theme }) => theme.gray500};
  }
`;

const DefaultSelectInputResult = styled.div<{ $active?: boolean }>`
  padding: ${spacing2};

  background-color: ${({ $active, theme }) => ($active ? theme.primary100 : "transparent")};

  &:first-child {
    border-top-left-radius: ${({ theme }) => theme.rounded};
    border-top-right-radius: ${({ theme }) => theme.rounded};
  }

  &:last-child {
    border-bottom-left-radius: ${({ theme }) => theme.rounded};
    border-bottom-right-radius: ${({ theme }) => theme.rounded};
  }
`;

export const SelectInputContainer = styled.div``;

const SelectInputResultList = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  border: 1px solid ${({ theme }) => theme.gray300};
  box-shadow: ${({ theme }) => theme.shadow};
  border-radius: ${({ theme }) => theme.rounded};
  width: 100%;
  overflow-y: auto;

  input {
    color: currentColor;
  }
`;
