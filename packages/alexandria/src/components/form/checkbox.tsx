import { FieldProps } from "formik";
import { FontWeight, transition } from "../../themes/index.js";
import styled from "styled-components";
import { FieldLabel } from "./field-label.js";

export interface CheckboxFieldProps extends FieldProps<boolean> {
  checkboxLabel?: string;
  id?: string;
  disabled?: boolean;
}

export function CheckboxField({ field, form, checkboxLabel, id, disabled }: CheckboxFieldProps) {
  return (
    <StyledCheckboxField>
      <Checkbox
        type="checkbox"
        id={id}
        checked={field.value}
        onChange={(event) => form.setFieldValue(field.name, event.target.checked)}
        onBlur={field.onBlur}
        name={field.name}
        disabled={disabled}
      />
      {checkboxLabel && <CheckboxFieldLabel htmlFor={id}>{checkboxLabel}</CheckboxFieldLabel>}
    </StyledCheckboxField>
  );
}

const StyledCheckboxField = styled.div`
  display: flex;
  align-items: center;
`;

export const Checkbox = styled.input.attrs(() => ({ type: "checkbox" }))`
  appearance: none;
  display: inline-block;
  width: 1rem;
  height: 1rem;
  color: ${({ theme }) => theme.primary500};
  color-adjust: exact;
  border: 1px solid ${({ theme }) => theme.gray300};
  border-radius: ${({ theme }) => theme.rounded};
  transition: ${({ theme }) => transition(["background-color", "border-color"], theme.defaultTransitionTiming)};

  &:checked {
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 16 16' fill='%23fff' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.707 7.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4a1 1 0 00-1.414-1.414L7 8.586 5.707 7.293z'/%3E%3C/svg%3E");
    border-color: transparent;
    background-color: currentColor;
    background-size: 100% 100%;
    background-position: 50%;
    background-repeat: no-repeat;
  }
}
`;

const CheckboxFieldLabel = styled(FieldLabel)`
  display: inline-block;
  margin: 0 ${({ theme }) => theme.spacing2};
  font-weight: ${FontWeight.Regular};
`;
