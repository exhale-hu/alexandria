import useConstant from "use-constant";
import { uniqueId } from "lodash-es";
import { FontWeight } from "../../themes/index.js";
import styled from "styled-components";

export const FieldLabel = styled.label`
  font-size: ${({ theme }) => theme.textSm};
  font-weight: ${FontWeight.Medium};
  display: block;
  line-height: ${({ theme }) => theme.leading5};
  margin-bottom: ${({ theme }) => theme.spacing1};
`;

export function useUniqueId(prefix?: string) {
  return useConstant(() => uniqueId(prefix));
}
