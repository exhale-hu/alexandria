import { ComponentProps, forwardRef, Ref } from "react";
import styled from "styled-components";
import { FieldProps } from "formik";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { getSelfAndAncestorFields } from "../../utils/index.js";
import { useFormikWrapperContext } from "./formik-wrapper.js";
import { TextInput } from "./input.js";

export function DateField({ field, form, ...rest }: FieldProps<string>) {
  const { errors, touched } = useFormikWrapperContext((context) => {
    const fieldNames = getSelfAndAncestorFields(field.name);

    return [
      ...fieldNames.map((fieldName) => context.errors[fieldName as keyof typeof context.errors]),
      ...fieldNames.map((fieldName) => context.touched[fieldName as keyof typeof context.touched]),
    ];
  });

  const hasError = getSelfAndAncestorFields(field.name).some(
    (fieldName) => !!errors[fieldName as keyof typeof errors] && !!touched[fieldName as keyof typeof touched]
  );

  return (
    <DateInput
      hasError={hasError}
      {...field}
      value={field.value ? new Date(field.value) : undefined}
      onChange={(event) => form.setFieldValue(field.name, event.target.value.toISOString())}
      {...rest}
    />
  );
}

interface DateInputProps extends Omit<ComponentProps<"input">, "ref" | "value" | "onChange"> {
  hasError?: boolean;
  value?: Date;
  onChange: (e: { target: { value: Date; name?: string } }) => void;
}

export const DateInput = forwardRef(function DateInputComponent(
  { hasError, value, onChange, ...props }: DateInputProps,
  ref: Ref<HTMLInputElement>
) {
  return (
    // @ts-ignore
    <StyledDatePicker
      {...props}
      dateFormat="yyyy.MM.dd"
      customInput={<TextInput {...props} key={props.key || undefined} hasError={hasError} ref={ref} />}
      selected={value}
      onChange={(value: Date) => value && onChange({ target: { value, name: props.name } })}
    />
  );
});

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`;
