import { FieldProps } from "formik";
import { useCallback, useMemo } from "react";
import { FileUploadUrl, FileUploadUrlProps } from "./file-upload-url.js";
import { FileEntry } from "./file-entry.js";
import { useFormikWrapperContext } from "../formik-wrapper.js";
import { getDisplayableErrorMessage } from "../../../utils/index.js";

export interface FileUploadFieldProps<FileType>
  extends FieldProps<FileType[]>,
    Omit<FileUploadUrlProps, "value" | "onChange"> {}

/**
 * Wraps FileEntityUpload to be usable with a FieldWrapper.
 * */
export function FileUploadField({ field, form, ...props }: FileUploadFieldProps<string>) {
  const { errors } = useFormikWrapperContext((context) => [context.errors[field.name as keyof typeof context.errors]]);

  function onFilesChange(files: FileEntry[]) {
    let hasUploading = false;

    for (const file of files) {
      if (file.status === "errored") {
        form.setFieldError(field.name, getDisplayableErrorMessage(file.error));
        return;
      }

      if (file.status === "working") hasUploading = true;
    }

    if (hasUploading) {
      form.setFieldTouched(field.name, false);
      form.setFieldError(field.name, "Az űrlapot nem lehet menteni, amíg feltöltés van folyamatban");
    } else if (errors[field.name as keyof typeof errors]) {
      form.setFieldError(field.name, "");
    }
  }

  return (
    <FileUploadUrl
      value={field.value}
      onChange={(newValue) => form.setFieldValue(field.name, newValue)}
      onFilesChange={onFilesChange}
      {...props}
    />
  );
}

export interface SingleFileUploadFieldProps<FileType>
  extends FieldProps<string | FileType | null>,
    Omit<FileUploadUrlProps, "value" | "limit"> {}

export function SingleFileUploadField<FileType>({
  field,
  form,
  onChange,
  ...props
}: SingleFileUploadFieldProps<FileType>) {
  const singleValue = useMemo(() => (field.value ? [field.value] : []), [field.value]);
  const singleOnChange = useCallback(
    (newValue: (string | FileType)[]) => form.setFieldValue(field.name, newValue.length ? newValue[0] : null),
    [form.setFieldValue]
  );

  return <FileUploadUrl value={singleValue as string[]} onChange={onChange || singleOnChange} limit={1} {...props} />;
}
