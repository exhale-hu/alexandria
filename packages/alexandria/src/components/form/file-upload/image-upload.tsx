import { FileUpload, FileUploadProps } from "./file-upload.js";

import { ImageUploadItemPreview } from "./file-upload-item-preview.js";

export type ImageUploadProps = FileUploadProps;

/**
 * Wraps FileUpload with proper props for image uploads.
 * */
export function ImageUpload(props: ImageUploadProps) {
  return <FileUpload accept={{ "image/*": [] }} preview={ImageUploadItemPreview} {...props} />;
}

export function ImageUploadFieldWithThumbnail(props: ImageUploadProps) {
  return <FileUpload accept={{ "image/*": [] }} preview={ImageUploadItemPreview} {...props} />;
}
