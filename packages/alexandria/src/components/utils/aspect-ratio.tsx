import { ComponentProps, CSSProperties, forwardRef, ReactNode, Ref } from "react";

const CUSTOM_PROPERTY_NAME = "--aspect-ratio";
const DEFAULT_CLASS_NAME = "react-aspect-ratio-placeholder";

export interface AspectRatioProps extends ComponentProps<"div"> {
  ratio?: string | number;
  children?: ReactNode;
}

export const AspectRatio = forwardRef(
  (
    { className = DEFAULT_CLASS_NAME, children, ratio = 1, style, ...restProps }: AspectRatioProps,
    ref: Ref<HTMLDivElement>
  ) => {
    const newStyle = {
      ...style,
      // https://github.com/roderickhsiao/react-aspect-ratio/commit/53ec15858ae186c41e70b8c14cc5a5b6e97cb6e3
      [CUSTOM_PROPERTY_NAME]: `(${ratio})`,
    } as CSSProperties;

    return (
      <div className={className} style={newStyle} ref={ref} {...restProps}>
        {children}
      </div>
    );
  }
);

if (typeof window !== "undefined" && typeof document !== "undefined") {
  const styleElement = document.createElement("style");
  styleElement.id = "aspect-ratio";
  // Copied from:
  // https://github.com/roderickhsiao/react-aspect-ratio/blob/e40d8e4ada2120bd8d3ef97b14299e199dc9c849/aspect-ratio.css
  styleElement.innerHTML = `
  [style*="--aspect-ratio"] > img {
      height: auto;
  }
  
  [style*="--aspect-ratio"] {
      position: relative;
  }
  
  [style*="--aspect-ratio"] > :first-child {
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      width: 100%;
  }
  
  [style*="--aspect-ratio"]::before {
      content: "";
      display: block;
      width: 100%;
  }
  
  @supports not (aspect-ratio: 1/1) {
      [style*="--aspect-ratio"]::before {
          height: 0;
          padding-bottom: calc(100% / (var(--aspect-ratio)));
      }
  }
  
  @supports (aspect-ratio: 1/1) {
      [style*="--aspect-ratio"]::before {
          aspect-ratio: calc(var(--aspect-ratio));
      }
  }
`;
  document.head.appendChild(styleElement);
}
