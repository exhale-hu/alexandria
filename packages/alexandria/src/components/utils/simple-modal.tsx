import { ReactNode } from "react";
import { StyledIcon } from "@styled-icons/styled-icon";
import { X } from "@styled-icons/boxicons-regular";
import { getKindDefaultIcon, IconCircle, IconCircleKind } from "./icon-circle.js";
import { maxWidth2Xl, spacing2, spacing3, spacing4, spacing6, transition } from "../../themes/index.js";
import styled from "styled-components";
import { Button } from "../inputs/button.js";
import { useModal } from "./modal-provider.js";
import { CardTitle } from "../layout/card.js";

export interface ConfirmModalProps {
  kind?: IconCircleKind;
  icon?: StyledIcon;
  title?: ReactNode;
  children?: ReactNode;
  confirmLabel?: ReactNode;
  cancelLabel?: ReactNode | string;
}

export function ConfirmModal({ kind, icon, title, children, confirmLabel = "OK", cancelLabel }: ConfirmModalProps) {
  return (
    <SimpleModal
      hideCloseIcon
      iconKind={kind}
      icon={icon || getKindDefaultIcon(kind)}
      title={title}
      footer={<ConfirmModalFooter kind={kind} confirmLabel={confirmLabel} cancelLabel={cancelLabel} />}
    >
      {children}
    </SimpleModal>
  );
}

function ConfirmModalFooter({
  kind,
  confirmLabel,
  cancelLabel = "Cancel",
}: {
  kind?: IconCircleKind;
  confirmLabel?: string | ReactNode;
  cancelLabel?: string | ReactNode;
}) {
  const { dismissModal } = useModal<boolean>();

  return (
    <>
      <Button onClick={() => dismissModal(false)}>{cancelLabel}</Button>
      <Button $kind={kind} onClick={() => dismissModal(true)}>
        {confirmLabel}
      </Button>
    </>
  );
}

export interface SimpleModalProps {
  iconKind?: IconCircleKind;
  icon?: StyledIcon;
  title?: ReactNode;
  children: ReactNode;
  footer?: ReactNode;
  className?: string;
  hideCloseIcon?: boolean;
  closeButtonLabel?: ReactNode;
  tone?: "light" | "dark";
}

export function SimpleModal({
  iconKind,
  icon: Icon,
  title,
  children,
  footer,
  className,
  hideCloseIcon,
  tone,
  closeButtonLabel,
}: SimpleModalProps) {
  return (
    <ModalDialog className={className}>
      <ModalDialogBody>
        {Icon && (
          <IconCircle $kind={iconKind}>
            <Icon />
          </IconCircle>
        )}
        <ModalDialogContent $hasExtraLeftMargin={!!Icon}>
          {title && <CardTitle>{title}</CardTitle>}
          <ModalDialogText>
            {!hideCloseIcon && <ModalCloseButton label={closeButtonLabel} tone={tone} />}
            {children}
          </ModalDialogText>
        </ModalDialogContent>
      </ModalDialogBody>
      {footer && <ModalDialogFooter>{footer}</ModalDialogFooter>}
    </ModalDialog>
  );
}

export function ModalCloseButton({
  tone,
  label,
  className,
}: {
  tone?: "dark" | "light";
  label?: ReactNode;
  className?: string;
}) {
  const { dismissModal } = useModal();
  return (
    <XRow onClick={() => dismissModal()} $tone={tone} className={className}>
      {label && <span>{label}</span>}
      <ModalDialogX />
    </XRow>
  );
}

export const XRow = styled.div<{ $tone?: "dark" | "light" }>`
  position: absolute;
  right: ${spacing3};
  top: 0;
  display: flex;
  align-items: center;
  text-transform: uppercase;
  color: ${({ $tone }) => ($tone === "light" ? "grey" : $tone === "dark" ? "white" : "currentColor")};
  width: fit-content;
  cursor: pointer;
  transition: ${({ theme }) => transition(["color"], theme.defaultTransitionTiming)};
  filter: drop-shadow(0 0 ${spacing2} white);

  &:hover {
    color: #eaeaea;
  }
`;

export const ModalDialog = styled.div`
  background: #fff;
  box-shadow: ${({ theme }) => theme.shadowXl};
  border-radius: ${({ theme }) => theme.roundedLg};
  width: 100%;
  position: relative;
  overflow: hidden;
  margin-left: auto;
  margin-right: auto;
  // Since the parent element (ModalBody) has pointer-events: none, we need to turn them back on to make it possible
  // to interact with the modal.
  pointer-events: initial;

  ${maxWidth2Xl};
`;

export const ModalDialogBody = styled.div`
  display: flex;
  align-items: flex-start;
  padding: ${spacing6} ${spacing6} ${spacing4} ${spacing6};
`;

export const modalDialogContentClass = "modal-dialog-content";

export const ModalDialogContent = styled.div.attrs(({ className = "" }) => ({
  className: `${modalDialogContentClass} ${className}`,
}))<{ $hasExtraLeftMargin: boolean }>`
  margin-left: ${({ $hasExtraLeftMargin }) => ($hasExtraLeftMargin ? spacing4 : "0")};
  overflow: auto;
  max-height: 92vh;
  height: fit-content;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const modalDialogTextClass = "modal-dialog-text";

export const ModalDialogText = styled.div.attrs(({ className = "" }) => ({
  className: `${modalDialogTextClass} ${className}`,
}))`
  font-size: ${({ theme }) => theme.textSm};
  color: ${({ theme }) => theme.gray500};
  line-height: ${({ theme }) => theme.leading5};
`;

export const ModalDialogX = styled(X)`
  cursor: pointer;
  font-size: ${({ theme }) => theme.text4xl};
`;

export const ModalDialogFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: ${spacing3} ${spacing6};
  background-color: ${({ theme }) => theme.gray50};

  ${Button} + ${Button} {
    margin-left: ${spacing3};
  }
`;
