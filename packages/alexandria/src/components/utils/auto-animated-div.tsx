import { ComponentProps, useEffect, useRef } from "react";
import autoAnimate from "@formkit/auto-animate";

export function AutoAnimatedDiv(props: ComponentProps<"div">) {
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    // @ts-ignore
    if (!import.meta.vitest) {
      ref.current && autoAnimate(ref.current);
    }
  }, [ref.current]);
  return <div {...props} ref={ref} />;
}
