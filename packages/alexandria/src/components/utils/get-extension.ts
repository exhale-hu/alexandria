export function getExtension(path: string) {
  const [match] = path.match(/\.[a-zA-Z0-9]+$/) || [];

  return match ? match.toLowerCase() : "";
}
