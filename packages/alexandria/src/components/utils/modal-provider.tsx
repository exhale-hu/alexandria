import { animated, SpringValue, SpringValues, useTransition, UseTransitionProps } from "react-spring";
import { createContext, MouseEvent, ReactNode, useCallback, useContext, useState } from "react";
import { defaults, noop } from "lodash-es";
import { Deferred } from "../../utils/index.js";
import { useRefValue } from "../../hooks/index.js";
import styled from "styled-components";
import { zIndex50 } from "../../themes/index.js";

export interface ModalOptions {
  dismissOnBackdropClick?: boolean | (() => boolean);
}

export interface ModalState<TValue = unknown> {
  content: ReactNode;
  options: ModalOptions;
  deferred: Deferred<TValue>;
  backdropProps: {
    onClick(event: MouseEvent): void;
    style: SpringValues;
    children: ReactNode;
  };

  showModal<TValue>(content: ReactNode, options?: ModalOptions): Promise<TValue>;

  dismissModal<TValue>(value?: TValue): void;
}

const defaultModalOptions: ModalOptions = {
  dismissOnBackdropClick: true,
};

function createEmptyModalState<TValue = unknown>(
  valueCreator: () => TValue = () => null as unknown as TValue
): ModalState<TValue> {
  return {
    content: null,
    options: defaultModalOptions,
    deferred: new Deferred<TValue>(),
    backdropProps: { onClick: noop, style: {}, children: null },

    showModal: () => Promise.resolve(valueCreator()),
    dismissModal: noop,
  } as ModalState<TValue>;
}

export const ModalContext = createContext(createEmptyModalState());

export function useModal<TValue = unknown>() {
  return useContext(ModalContext) as ModalState<TValue>;
}

export interface ModalProviderProps {
  children?: ReactNode;
  backdrop?: ReactNode;
  transitionProps?: UseTransitionProps;
}

export const defaultTransitionProps = {
  from: {
    opacity: 0,
    transform: "translateY(3rem)",
  },
  enter: { opacity: 1, transform: "translateY(0rem)" },
  leave: { opacity: 0, transform: "translateY(3rem)" },
};

export function ModalProvider({
  backdrop = <DefaultModalBackdrop />,
  transitionProps = defaultTransitionProps,
  children,
}: ModalProviderProps) {
  const [state, setState] = useState<ModalState>(createEmptyModalState);
  const stateRef = useRefValue(state);

  state.showModal = useCallback(function <TValue = unknown>(
    content: ReactNode,
    options?: ModalOptions
  ): Promise<TValue> {
    const newState: ModalState<TValue> = {
      ...createEmptyModalState<TValue>(),

      content,
      options: defaults(options, defaultModalOptions),
    };

    // Defer opening the modal
    Promise.resolve().then(() => setState(newState as ModalState));

    return newState.deferred.promise;
  },
  []);

  state.dismissModal = useCallback(function <T>(value?: T) {
    stateRef.current.deferred.resolve(value);

    // Defer closing the modal
    Promise.resolve().then(() => setState(createEmptyModalState()));
  }, []);

  const transition = useTransition(state, transitionProps);

  return (
    <ModalContext.Provider value={state}>
      {children}
      {transition((transitionStyle, currentState) => {
        const { dismissModal, content, options } = currentState;

        if (!content) return null;

        function onBackgroundClick(event: MouseEvent<HTMLDivElement>) {
          // Don't handle clicks from the dialog, only the background
          if (event.target !== event.currentTarget) return;

          event.preventDefault();

          const dismissOnBackdropClick = options?.dismissOnBackdropClick ?? true;

          if (typeof dismissOnBackdropClick === "function") {
            if (dismissOnBackdropClick()) {
              dismissModal();
            }
          } else if (dismissOnBackdropClick) {
            dismissModal();
          }
        }

        currentState.backdropProps = {
          onClick: onBackgroundClick,
          style: transitionStyle,
          children: content,
        };

        return <ModalContext.Provider value={currentState}>{backdrop}</ModalContext.Provider>;
      })}
    </ModalContext.Provider>
  );
}

const ModalBody = styled(animated.div)`
  display: flex;
  flex-direction: column;
  max-width: 90%;
`;

function DefaultModalBackdrop() {
  const {
    backdropProps: { style, children, ...backdropProps },
  } = useModal();

  return (
    // Typescript error in react-spring
    // @ts-ignore
    <ModalBackdrop {...backdropProps} style={{ opacity: style.opacity as SpringValue<number> }}>
      <ModalBody style={style}>{children}</ModalBody>
    </ModalBackdrop>
  );
}

export const ModalBackdrop = styled(animated.div)`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: ${zIndex50};

  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.25);
`;
