import { LoaderAlt } from "@styled-icons/boxicons-regular";
import styled, { keyframes } from "styled-components";

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const LoadingIcon = styled(LoaderAlt)`
  animation: 500ms linear ${spin} infinite;
`;

export const LargeLoadingIcon = styled(LoadingIcon)`
  font-size: ${({ theme }) => theme.text6xl};
  color: ${({ theme }) => theme.primary500};
`;
