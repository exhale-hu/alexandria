import { Children, Fragment, isValidElement, ReactElement, ReactNode, useContext } from "react";
import {
  IndexRouteProps,
  LayoutRouteProps,
  Navigate,
  Outlet,
  PathRouteProps,
  Route,
  RouteObject,
  RoutesProps,
  useRoutes,
} from "react-router-dom";
import { LoadingIcon } from "../loading-icon.js";
import { useObservable } from "@litbase/use-observable";
import { AccessLevel, RestrictedContext, RestrictedProps } from "./restricted-provider.js";

type RouteProps = PathRouteProps | LayoutRouteProps | IndexRouteProps;

type RestrictedRouteProps<T = AccessLevel> = RouteProps & {
  only?: T;
  guest?: boolean;
  // Route where guests get redirected when trying to access a restricted (or non-existent) page
  guestFallbackRoute?: string;
  // Route where logged in users get redirected when trying to access a restricted (or non-existent) page
  loggedInFallbackRoute?: string;
};

export function RestrictedRoute({
  element = <Outlet />,
  only = AccessLevel.Normal,
  guest,
  guestFallbackRoute = "/login",
  loggedInFallbackRoute = "/dashboard",
  ...props
}: RestrictedRouteProps) {
  return (
    <Route
      {...props}
      element={
        <Restricted
          level={only}
          guest={guest}
          fallback={<Navigate to={guest ? loggedInFallbackRoute : guestFallbackRoute} />}
        >
          {element}
        </Restricted>
      }
    />
  );
}

function useUser() {
  const contextValue = useContext(RestrictedContext);
  if (!contextValue) throw new Error("Tried to use restricted component without using a RestrictedProvider");
  const [user] = useObservable(() => contextValue.user$, contextValue.defaultUser);
  return user;
}

// Based on the actual Routes component
export function RestrictedRoutes({
  children,
  location,
  loading = <LoadingIcon />,
}: RoutesProps & {
  loading?: ReactNode;
}) {
  const routes = useRoutes(createRoutesFromChildren(children, AccessLevel.Normal), location);

  return (
    <Restricted any loading={loading}>
      {routes}
    </Restricted>
  );
}

export function Restricted({
  level = AccessLevel.Normal,
  fallback = null,
  loading = null,
  children,
  guest,
  exact,
  any = false,
}: RestrictedProps) {
  const user = useUser();
  const context = useContext(RestrictedContext);
  if (!context) throw new Error();

  if (context.isEmptyUser(user)) {
    return loading as ReactElement;
  }

  return any || context.isGranted({ ...context, level, guest, exact, fallback, loading, children, any }, user)
    ? (children as ReactElement)
    : (fallback as ReactElement);
}

// Based on:
// https://github.com/remix-run/react-router/blob/d32662ddd67d489a46171b450ec933a0ce88d346/packages/react-router/index.tsx#L755
function createRoutesFromChildren(children: ReactNode, userLevel: AccessLevel): RouteObject[] {
  const routes: RouteObject[] = [];

  Children.forEach(children, (element) => {
    if (!isValidElement(element)) {
      // Ignore non-elements. This allows people to more easily inline
      // conditionals in their route config.
      return;
    }

    if (element.type === Fragment) {
      // Transparently support React.Fragment and its children.
      routes.push(...createRoutesFromChildren(element.props.children, userLevel));
      return;
    }

    if (
      element.type === RestrictedRoute ||
      (typeof element.type !== "string" && element.type.name === "RestrictedRoute")
    ) {
      element = RestrictedRoute(element.props);
    }

    // Rerun this to make typescript happy
    if (!isValidElement(element)) return;

    // Rewrote the invariant part (see original source)
    if (element.type !== Route) {
      throw new Error(
        `[${
          typeof element.type === "string" ? element.type : element.type.name
        }] is not a <Route> component. All component children of <Routes> must be a <Route> or <React.Fragment>`
      );
    }

    const props = element.props as PathRouteProps;

    const route: RouteObject = {
      caseSensitive: props.caseSensitive,
      element: props.element,
      index: props.index,
      path: props.path,
    };

    if (props.children) {
      route.children = createRoutesFromChildren(props.children, userLevel);
    }

    routes.push(route);
  });

  return routes;
}
