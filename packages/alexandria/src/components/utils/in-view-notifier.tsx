import { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import styled from "styled-components";
import { spacing4 } from "../../themes/index.js";

export function InViewNotifier({
  onInView,
  deps = [],
  onOutOfView,
}: {
  onInView: () => void;
  deps?: unknown[];
  onOutOfView?: () => void;
}) {
  const { ref, inView } = useInView();

  useEffect(() => {
    if (inView) {
      onInView();
    } else {
      onOutOfView?.();
    }
  }, [inView, ...deps]);

  return (
    <RelativeContainer>
      <ZeroHeightDiv ref={ref} />
    </RelativeContainer>
  );
}

const ZeroHeightDiv = styled.div`
  height: 0px;
  position: absolute;
  top: ${spacing4};
`;

const RelativeContainer = styled.div`
  position: relative;
`;
