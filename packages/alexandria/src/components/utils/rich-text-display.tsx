import { Ref, CSSProperties } from "react";
import { Interweave, InterweaveProps, MatcherInterface } from "interweave";
import { size220 } from "../../themes/index.js";
import styled from "styled-components";

interface RichTextDisplayProps extends InterweaveProps {
  content?: string;
  matchers?: MatcherInterface<unknown>[];
  style?: CSSProperties;
  className?: string;
  innerRef?: Ref<HTMLDivElement>;
  whitelist?: string[];
}

export function RichTextDisplay(props: RichTextDisplayProps) {
  return (
    <HTMLDisplay ref={props.innerRef} style={props.style} className={props.className}>
      <Interweave matchers={props.matchers} allowList={props.whitelist} disableLineBreaks {...props} />
    </HTMLDisplay>
  );
}

export const HTMLDisplay = styled.div`
  > span {
    display: flex;
    flex-direction: column;
  }

  * {
    line-break: auto;
  }

  > span .image img {
    display: block;
    margin: 0 auto;
    max-width: min(${size220}, 100%);
    min-width: 50px;
  }

  > span .image-style-side,
  > span .image-style-align-left,
  > span .image-style-align-center,
  > span .image-style-align-right {
    max-width: 50%;
  }

  > span .image-style-side {
    float: right;
    margin-left: var(--ck-image-style-spacing);
  }

  > span .image-style-align-left {
    float: left;
    margin-right: var(--ck-image-style-spacing);
  }

  > span .image-style-align-center {
    margin-left: auto;
    margin-right: auto;
  }

  > span .image-style-align-right {
    float: right;
    margin-left: var(--ck-image-style-spacing);
  }

  > span .image.image_resized > figcaption {
    display: block;
  }

  > span .image > figcaption {
    display: table-caption;
    caption-side: bottom;
    word-break: break-word;
    color: hsl(0, 0%, 20%);
    background-color: hsl(0, 0%, 97%);
    padding: 0.6em;
    font-size: 0.75em;
    outline-offset: -1px;
  }

  /* ckeditor5-table/theme/table.css */

  > span .table {
    margin: 1em auto;
    display: table;
  }

  /* ckeditor5-table/theme/table.css */

  > span .table table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    height: 100%;
    border: 1px double hsl(0, 0%, 70%);
  }

  /* ckeditor5-table/theme/table.css */

  > span .table table td,
  > span .table table th {
    min-width: 2em;
    padding: 0.4em;
    border: 1px solid hsl(0, 0%, 75%);
  }

  /* ckeditor5-table/theme/table.css */

  > span .table table th {
    font-weight: bold;
    background: hsla(0, 0%, 0%, 5%);
  }

  /* ckeditor5-table/theme/table.css */

  > span[dir="rtl"] .table th {
    text-align: right;
  }

  /* ckeditor5-table/theme/table.css */

  > span[dir="ltr"] .table th {
    text-align: left;
  }
`;
