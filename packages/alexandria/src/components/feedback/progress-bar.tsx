import { animated, useSpring } from "react-spring";
import styled from "styled-components";
import { size3 } from "../../themes/style-utils.js";
import { CSSProperties } from "react";

/**
 * Renders a simple progress bar
 * @param progress : number - progress percentage, between 0 - 100
 * */
export function ProgressBar({
  progress,
  className,
  style,
}: {
  progress: number | undefined;
  className?: string;
  style?: CSSProperties;
}) {
  const animStyles = useSpring({
    width: `${progress || 0}%`,
  });
  return (
    <Body className={className} style={style}>
      <Filling style={animStyles} />
    </Body>
  );
}

const Filling = styled(animated.div)`
  background: ${({ theme }) => theme.primary500};
  border-radius: ${({ theme }) => theme.roundedFull};
  position: absolute;
  height: 100%;
  left: 0;
  top: 0;
`;

const Body = styled.div`
  width: 100%;
  border-radius: ${({ theme }) => theme.roundedFull};
  background: ${({ theme }) => theme.gray400};
  height: ${size3};
  position: relative;
`;
