import { ComponentProps, Context, ReactNode, useMemo, useState } from "react";
import { DocumentInterface, QueryTypeBody } from "@litbase/core";
import { useQueryType } from "@litbase/react";
import { AddButton } from "../grid/grid-buttons.js";
import { BasicPage } from "../../pages/basic-page.js";
import { ListCard, ListCardProps } from "./list-card.js";
import { createListPageContext } from "./list-context.js";

export type ListPageProps<T> = Partial<ListCardProps<T>> & {
  title?: string;
  buttons?: ReactNode | { top?: ReactNode; bottom?: ReactNode };
  rowComponent: (item: T, index: number) => ReactNode;
  headerRowComponent: ReactNode;
  columns: string;
  searchParams?: (keyof T)[];
  orderParams?: Partial<{ [x in keyof T]: "asc" | "desc" }>;
  filters?: Record<string, string | undefined | boolean | number>;
  hideAddButtons?: boolean;
  addButtonLink?: string;
  pagination?: boolean;
  children?: ReactNode;
  addButtonProps?: ComponentProps<typeof AddButton>;
};

export type QueryListPageProps<T> = ListPageProps<T> & {
  fields: QueryTypeBody;
  type: string;
};

export type CustomListPageProps<T> = ListPageProps<T> & {
  items: T[];
};

export let ListPageContext: Context<ListCardProps<any>>;

export function CustomListPage<T>({
  title,
  hideAddButtons,
  addButtonLink = "/",
  items = [],
  searchParams,
  pagination,
  children = <ListCard<T> />,
  ...props
}: CustomListPageProps<T>) {
  ListPageContext = createListPageContext<T>();
  const [currentPage, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(25);
  const [search, setSearch] = useState("");
  const searchItems = getSearchItems();

  /**
   * Filter by page number and page size if pagination is enabled.
   */
  const filteredItems = pagination
    ? searchItems.filter((item, index) => index >= (currentPage - 1) * pageSize && index < currentPage * pageSize)
    : searchItems;
  return (
    <BasicPage title={title} buttons={props.buttons}>
      {!hideAddButtons && <AddButton to={addButtonLink} {...props.addButtonProps} />}
      <ListPageContext.Provider
        value={
          {
            searchable: !!searchParams,
            pageSize: pageSize,
            pageCount: Math.floor(items.length / pageSize) + 1,
            onCurrentPageChange: setPage,
            onPageSizeChange: setPageSize,
            onSearchChange: setSearch,
            items: filteredItems,
            showPagination: pagination,
            ...props,
            currentPage: currentPage,
          } as ListCardProps<T>
        }
      >
        {children}
      </ListPageContext.Provider>
      {!hideAddButtons && <AddButton to={addButtonLink} {...props.addButtonProps} />}
    </BasicPage>
  );

  function getSearchItems() {
    /**
     * If there are searchParams, search is enabled and the items should be filtered by them.
     */
    if (searchParams) {
      return items.filter((item) => {
        for (const key of searchParams) {
          const value = item[key];
          if (typeof value === "string") {
            if (value.toString().includes(search) || !search) return true;
          }
        }
        return false;
      });
    } else {
      return items;
    }
  }
}

export function QueryListPage<T extends DocumentInterface>({
  title,
  hideAddButtons,
  addButtonLink = "/",
  pagination,
  searchParams,
  children = <ListCard<T> />,
  ...props
}: QueryListPageProps<T>) {
  ListPageContext = createListPageContext<T>();
  const [currentPage, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(25);
  const [search, setSearch] = useState("");
  // const paginationQueryParams = pagination ? { $limit: pageSize, $skip: (currentPage - 1) * pageSize } : {};
  const queryObject = useMemo(() => {
    return {
      ...props.fields,
      $matchAll: {
        ...(searchParams && {
          $or: (searchParams || []).map((elem) => ({
            [elem]: { $regex: search + ".*", $options: "-i" },
          })),
        }),
        ...(props.fields["$matchAll"] || {}),
      },
    };
  }, [props.fields, search, pageSize, currentPage, pagination]);
  const [items] = useQueryType<T>(props.type, queryObject);
  const filteredItems = pagination
    ? items.filter((item, index) => index >= (currentPage - 1) * pageSize && index < currentPage * pageSize)
    : items;
  return (
    <BasicPage title={title} buttons={props.buttons}>
      {!hideAddButtons && <AddButton to={addButtonLink} {...props.addButtonProps} />}
      <ListPageContext.Provider
        value={
          {
            searchable: !!searchParams,
            pageSize: pageSize,
            pageCount: Math.ceil(items.length / pageSize),
            onCurrentPageChange: setPage,
            onPageSizeChange: setPageSize,
            onSearchChange: setSearch,
            showPagination: pagination,
            ...props,
            currentPage: currentPage,
            items: filteredItems,
          } as ListCardProps<T>
        }
      >
        {children}
      </ListPageContext.Provider>
      {!hideAddButtons && <AddButton to={addButtonLink} {...props.addButtonProps} />}
    </BasicPage>
  );
}
