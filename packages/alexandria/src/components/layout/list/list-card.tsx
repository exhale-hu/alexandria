import { GridPaginationFooter } from "../grid/grid-pagination-footer.js";
import { GridSearchHeader } from "../grid/grid-search-header.js";
import { Grid } from "../grid/grid.js";
import { GridBody } from "../grid/grid-body.js";
import { Card, FormCardTitle } from "../card.js";
import { spacing4 } from "../../../themes/index.js";
import { DocumentInterface } from "@litbase/core";
import { CSSProperties, ReactNode, useContext } from "react";
import styled from "styled-components";
import { ListPageContext } from "./list-page.js";
import { isEmpty } from "lodash-es";
import { defaultContextValue } from "./list-context.js";

export type ListCardProps<T> = {
  pageCount?: number;
  currentPage: number;
  onCurrentPageChange?: (currentPage: number) => void;
  pageSize?: number;
  onPageSizeChange?: (pageSize: number) => void;
  showPagination?: boolean;
  searchable?: boolean;
  isSearchLoading?: boolean;
  onSearchChange?: (query: string) => void;
  items: T[];
  columns: string;
  cardTitle?: string;
  rowComponent: (item: T, index: number) => ReactNode;
  headerRowComponent: ReactNode;
  className?: string;
  style?: CSSProperties;
  pageText?: string | ReactNode;
  searchPlaceholder?: string;
};

/**
 * A grid based list to display the given items with the given rowComponent. Uses ListPageContext by default.
 * Pagination, search, page size etc. is not implemented. This will only call the given function when an event is occurred.
 * @param props Optional props that will overwrite the values got from the context.
 */
export function ListCard<T = DocumentInterface>(props: ListCardProps<T> | {}) {
  let contextValues;
  try {
    contextValues = useContext(ListPageContext);
  } catch (e) {
    contextValues = defaultContextValue<T>();
  }
  if (!isEmpty(props)) contextValues = props as ListCardProps<T>;
  const {
    pageSize = 25,
    onPageSizeChange,
    pageCount = 0,
    currentPage,
    onCurrentPageChange,
    onSearchChange,
    isSearchLoading,
    searchable,
    showPagination,
    items = [],
    rowComponent,
    headerRowComponent,
    columns,
    cardTitle,
    style,
    className,
    pageText,
    searchPlaceholder,
  } = contextValues;
  const paginationComponent = (
    <GridPaginationFooter
      pageSize={pageSize}
      onPageSizeChange={(value) => onPageSizeChange?.(value)}
      pageCount={pageCount}
      currentPage={currentPage}
      goToPage={(value) => onCurrentPageChange?.(value)}
      itemCount={0}
      pageText={pageText}
    />
  );
  return (
    <StyledCard $padding={0} $hideOverflow style={style} className={className}>
      {cardTitle && <CardTitle>{cardTitle}</CardTitle>}
      {searchable && (
        <GridSearchHeader
          isSearchLoading={isSearchLoading}
          search={(value) => onSearchChange?.(value)}
          placeholder={searchPlaceholder}
        />
      )}
      {showPagination && paginationComponent}
      <Grid columns={columns}>
        {headerRowComponent}
        <GridBody isEmpty={items.length === 0}>{rowComponent && items.map(rowComponent)}</GridBody>
      </Grid>
      {showPagination && paginationComponent}
    </StyledCard>
  );
}

export const CardTitle = styled(FormCardTitle)`
  margin: 0;
`;

const StyledCard = styled(Card)`
  margin-top: ${spacing4};
  margin-bottom: ${spacing4};
  min-width: fit-content;
  overflow: visible;

  > *:first-child {
    border-top-left-radius: ${({ theme }) => theme.roundedLg};
    border-top-right-radius: ${({ theme }) => theme.roundedLg};
  }

  > *:last-child {
    border-bottom-left-radius: ${({ theme }) => theme.roundedLg};
    border-bottom-right-radius: ${({ theme }) => theme.roundedLg};
  }
`;
