import { ReactElement, ReactNode, useCallback, useState } from "react";
import {
  BeforeCapture,
  DragDropContext,
  Draggable,
  DraggableChildrenFn,
  DraggableProvided,
  Droppable,
  DroppableProvided,
  DropResult,
} from "react-beautiful-dnd";
import { CheckCircle, Menu } from "@styled-icons/boxicons-regular";
import { Error } from "@styled-icons/boxicons-solid";
import { DocumentInterface, litql } from "@litbase/core";
import { useQuery } from "@litbase/react";
import styled from "styled-components";
import { useUniqueId } from "../../form/index.js";
import { useForceUpdate } from "../../../hooks/index.js";
import { moveItemInPlace } from "../../../utils/util-functions.js";
import { showErrorToast } from "../../../services/toaster-service.js";
import { getDisplayableErrorMessage } from "../../../utils/index.js";
import { BasicPage } from "../../pages/basic-page.js";
import { AddButton } from "../grid/grid-buttons.js";
import { LoadingIcon } from "../../utils/loading-icon.js";
import { GridBody } from "../grid/grid-body.js";
import { spacing4 } from "../../../themes/index.js";
import { Card } from "../card.js";
import { Cell, Grid, GridRow } from "../grid/grid.js";

interface ReorderableListProps {
  onReordered: (from: number, to: number) => void;
  columns: string;
  children: (provided: DroppableProvided) => ReactNode;
  className?: string;
}

export function ReorderableList({ children, columns, onReordered, className }: ReorderableListProps) {
  const listId = useUniqueId("reorderable-list-");

  const getListElement = useCallback(() => {
    return document.querySelector(`[data-rbd-droppable-id="${listId}"]`) as HTMLElement;
  }, []);

  const onBeforeCapture = useCallback(({ draggableId }: BeforeCapture) => {
    const draggableElement = getDraggableElement(draggableId);

    if (!draggableElement) return;

    const cellWidths = Array.prototype.map.call(
      draggableElement.children,
      (child: HTMLElement) => child.getBoundingClientRect().width
    ) as number[];

    iterateOverCells(getListElement(), (cell, columnIndex, rowLength) => {
      if (1 < rowLength) {
        cell.style.width = `${cellWidths[columnIndex]}px`;
      } else {
        cell.style.width = `100%`;
      }
    });

    getListElement()?.classList.add("is-dragging");
  }, []);

  const onDragEnd = useCallback(
    ({ destination, draggableId, source }: DropResult) => {
      const draggableElement = getDraggableElement(draggableId);

      if (draggableElement) {
        getListElement()?.classList.remove("is-dragging");

        iterateOverCells(getListElement(), (cell) => (cell.style.width = ""));
      }

      if (destination) {
        onReordered(source.index, destination.index);
      }
    },
    [onReordered]
  );

  return (
    <DragDropContext onBeforeCapture={onBeforeCapture} onDragEnd={onDragEnd}>
      <Droppable droppableId={listId}>
        {(provided) => (
          <ReorderableListGrid
            className={className}
            columns={columns}
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={{ gridTemplateColumns: columns }}
          >
            {children(provided)}
          </ReorderableListGrid>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export interface DraggableListRowProps {
  draggableId: string;
  index: number;
  isDragDisabled?: boolean;
  children: DraggableChildrenFn;
}

export function DraggableListRow({ draggableId, index, isDragDisabled, children }: DraggableListRowProps) {
  return (
    <Draggable draggableId={draggableId} index={index} isDragDisabled={isDragDisabled}>
      {children}
    </Draggable>
  );
}

interface ReorderableListPageProps<T> extends Omit<ReorderableListProps, "onReordered" | "children"> {
  headerRowCells: ReactNode;
  rowCells: (item: T, index: number, provided: DraggableProvided) => ReactElement<HTMLElement>;
  collection: string;
  title: string;
  getPosition?: (item: T) => number;
  getOrdering?: (item: T) => number;
  buttons?: ReactNode | { top?: ReactNode; bottom?: ReactNode };
  onRefresh?: () => void;
  onAddButtonClick?: () => void;
  filters?: Record<string, string>;
  reorderingQuery: Record<string, unknown>; // A query that returns all elements that could be affected by a reorder.
}

export function ReorderableListPage<T extends DocumentInterface & { ordering?: number; position?: number }>({
  buttons,
  collection,
  columns,
  headerRowCells,
  onAddButtonClick,
  onRefresh,
  reorderingQuery,
  rowCells,
  title,
}: ReorderableListPageProps<T>) {
  const addButtonProps = onAddButtonClick ? { onClick: onAddButtonClick } : {};
  const [saving, setSaving] = useState<boolean | "error">(false);
  const [items, loading] = useQuery<T>(collection, { $matchAll: {} });

  const forceUpdate = useForceUpdate();
  const onReordered = useCallback(
    async (from: number, to: number) => {
      try {
        const item = items[from];
        moveItemInPlace(items, from, to);
        setSaving(true);
        forceUpdate();
        await sendItemToOrdering<T>(item, to + 1, collection, reorderingQuery);
        setSaving(false);
        onRefresh?.();
      } catch (error) {
        console.error(error);
        showErrorToast("Error while saving", getDisplayableErrorMessage(error as Error));
        setSaving("error");
      }
    },
    [items, onRefresh]
  );

  return (
    <BasicPage title={title} buttons={buttons}>
      <Row>
        <AddButton {...addButtonProps} />
        <SavingText $opacity={saving ? 1 : 0}>
          {saving === "error" && (
            <>
              <Error /> Hiba
            </>
          )}
          {saving === true && (
            <>
              <LoadingIcon /> Mentés
            </>
          )}
          {saving === false && (
            <>
              <CheckCircle /> Kész
            </>
          )}
        </SavingText>
      </Row>
      <StyledCard $padding={0} $hideOverflow>
        <ReorderableList columns={columns} onReordered={onReordered}>
          {(provided) => (
            <>
              <ReorderableListRow>{headerRowCells}</ReorderableListRow>
              <GridBody isEmpty={items.length === 0} isLoading={loading}>
                {items.map((item, index) => (
                  <DraggableListRow key={item._id.value} draggableId={item._id.value} index={index}>
                    {(provided) => rowCells(item, index, provided)}
                  </DraggableListRow>
                ))}
                {provided.placeholder}
              </GridBody>
            </>
          )}
        </ReorderableList>
      </StyledCard>
      <AddButton {...addButtonProps} />
    </BasicPage>
  );
}

function iterateOverCells(
  listElement: HTMLElement,
  callback: (cell: HTMLElement, columnIndex: number, rowLength: number) => void
) {
  for (const row of Array.from(listElement.children)) {
    for (let i = 0, length = row.children.length; i < length; i++) {
      callback(row.children[i] as HTMLElement, i, length);
    }
  }
}

function getDraggableElement(draggableId: string) {
  return document.querySelector(`[data-rbd-draggable-id="${draggableId}"]`) as HTMLElement | null;
}

/** The query should return all items that could be affected */
export async function sendItemToOrdering<T extends DocumentInterface>(
  item: T,
  position: number,
  collection: string,
  query: Record<string, unknown>
) {
  const items = await litql.query<T>(collection, {
    $matchAll: query,
  });
  const newItems = moveItemInPlace(
    items,
    items.findIndex((elem) => elem._id.equals(item._id)),
    position
  );
  await Promise.all(
    newItems.map((elem, index) =>
      litql.query(collection, {
        $match: { _id: elem._id },
        $update: {
          $set: {
            position: index,
          },
        },
      })
    )
  );
}

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const SavingText = styled.span<{ $opacity: number }>`
  margin-right: ${spacing4};
  color: ${({ theme }) => theme.gray600};
  opacity: ${(props) => props.$opacity};
  transition-property: opacity;
  transition-duration: 500ms;
  transition-delay: 100ms;
`;

const StyledCard = styled(Card)`
  margin-top: ${spacing4};
  margin-bottom: ${spacing4};
`;

export const ReorderableListRow = styled(GridRow)`
  &:not(:first-child) + & {
    ${Cell} {
      border-top: 1px solid ${({ theme }) => theme.gray200};
    }
  }
`;

export const ReorderableListHandle = styled(Menu).attrs(({ className }) => ({
  className: className + " " + "drag-handle",
}))`
  font-size: 2rem;
  cursor: grab;
`;

const ReorderableListGrid = styled(Grid)`
  /* Make the placeholder element behave nicely as a full length row */

  [data-rbd-placeholder-context-id] {
    grid-column: 1 / -1;
  }

  &.is-dragging {
    ${ReorderableListRow} {
      display: flex;
      grid-column: 1 / -1;
    }
  }
`;
