import styled from "styled-components";

export const Box = styled.div`
  position: relative;
  padding: ${(props) => props.theme.boxPadding};
  background-color: ${(props) => props.theme.boxBg};
  border-radius: 0.4rem;
  box-shadow: ${(props) => props.theme.boxShadow};
`;
