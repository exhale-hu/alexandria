import React, { CSSProperties, useEffect, useMemo, useRef, useState } from "react";
import { useGesture } from "@use-gesture/react";
import { animated, useSpring } from "react-spring";
import { debounce } from "lodash-es";
import styled from "styled-components";
import { useMeasure } from "../../hooks/index.js";

export interface CarouselProps {
  children: (React.ReactElement | null | false)[] | React.ReactElement | null | false;
  index: number;

  onIndexChange(newIndex: number): void;

  className?: string;
  childDisplay?: "inline-block" | "inline-flex" | "inline-grid";
  style?: CSSProperties;
}

export function Carousel({ children: childrenProp, index, onIndexChange, className, childDisplay }: CarouselProps) {
  // Normalize children in to an array
  const children = Array.isArray(childrenProp) ? childrenProp : [childrenProp];

  // Scroll offset when the dragging started
  const dragStartXRef = useRef(0);
  // Maximum possible scroll position in the container
  const maxScrollLeftRef = useRef(1);
  // Width of the visible container
  const containerWidthRef = useRef(1);
  // Reference to the container element
  const elementRef = useRef<HTMLDivElement>(null);
  const [props, setProps] = useSpring(() => ({ scrollLeft: 0 }), []);

  useGesture(
    {
      onDragStart() {
        // Store the starting scroll position so we can calculate the absolute scroll in the end
        dragStartXRef.current = props.scrollLeft.get();

        if (elementRef.current) {
          const containerWidth = elementRef.current.offsetWidth;
          // Update these values for later
          containerWidthRef.current = containerWidth;
          // Max scroll is the width of the carousel contents minus it's visible width
          maxScrollLeftRef.current = elementRef.current.scrollWidth - containerWidth;
        } else {
          maxScrollLeftRef.current = 1;
        }
      },
      onDrag({ movement: [dx, dy] }) {
        if (Math.abs(dx) > 30 && Math.abs(dy) < 50) {
          // Don't move on very small movements to make it possible to scroll past carousel
          // Clamp the scroll position, so it won't get invalid values
          const newScrollLeft = Math.min(Math.max(0, dragStartXRef.current - dx), maxScrollLeftRef.current);

          // When dragging, set immediate to true for instant feedback
          setProps({ scrollLeft: newScrollLeft, immediate: true });
        }
      },
      onDragEnd() {
        if (!elementRef.current) return;

        // Calculate the scroll position at the center of the visible part
        const scrollCenter = props.scrollLeft.get() + containerWidthRef.current / 2;

        // Start searching which child's center position is closest to the carousel's center. As we go through the
        // children we'll get closer and closer to our target element, then elements will start to get more and more
        // distant (as we've passed the closest one). So stop searching when children start to get farther away.
        let minFoundDistance = Number.POSITIVE_INFINITY;
        let newIndex = 0;
        let newScrollLeft = 0;

        let childIndex = 0;
        for (const child of Array.from(elementRef.current.childNodes)) {
          const element = child as HTMLElement;
          const elementCenter = element.offsetLeft + element.offsetWidth / 2;
          const distanceFromContainerCenter = Math.abs(scrollCenter - elementCenter);

          if (distanceFromContainerCenter < minFoundDistance) {
            minFoundDistance = distanceFromContainerCenter;
            newIndex = childIndex;
            newScrollLeft = elementCenter - containerWidthRef.current / 2;
          } else {
            // Found the closest child, the rest are only gonna be farther
            break;
          }

          childIndex++;
        }

        // Update the scroll position with animation
        setProps({ scrollLeft: newScrollLeft });

        // Signal the listener that the active index changed
        if (newIndex !== index) onIndexChange(newIndex);
      },
    },
    { eventOptions: { capture: true }, target: elementRef }
  );

  const [containerWidth, setContainerWidth] = useState(1);
  const debouncedSetWidth = useMemo(() => {
    return debounce(() => {
      if (elementRef.current) {
        setContainerWidth(elementRef.current.getBoundingClientRect().width);
      }
    }, 250);
  }, []);
  useMeasure(() => {
    debouncedSetWidth();
  }, elementRef);

  useEffect(() => {
    const container = elementRef.current;

    // If we have no children, don't do anything
    if (!container?.childNodes.length) return;

    // If the active index or the amount of children changed, recalculate our scroll index
    const centerChild = container.childNodes[Math.min(container.childNodes.length - 1, index)] as HTMLElement;
    // The new scroll position is the scroll position of the child's center minus the container's visible center
    const newScrollLeft = centerChild.offsetLeft + centerChild.offsetWidth / 2 - containerWidth / 2;

    setProps({ scrollLeft: newScrollLeft });
  }, [index, children.length, containerWidth]);

  return (
    <StyledCarousel {...props} $childDisplay={childDisplay} className={className} ref={elementRef}>
      {childrenProp}
    </StyledCarousel>
  );
}

export const StyledCarousel = styled(animated.div)<{ $childDisplay?: "inline-block" | "inline-flex" | "inline-grid" }>`
  width: 100%;
  overflow: hidden;
  position: relative;
  z-index: 2;
  white-space: nowrap;
  touch-action: pan-y;

  > * {
    display: ${({ $childDisplay = "inline-block" }) => $childDisplay};
    vertical-align: top;
    /* Prevent white-space inheritance */
    white-space: normal;
  }
`;
