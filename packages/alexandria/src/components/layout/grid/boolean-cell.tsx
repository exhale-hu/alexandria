import { Check, X } from "@styled-icons/boxicons-regular";
import { ComponentProps } from "react";
import styled from "styled-components";
import { Cell } from "./grid.js";

export function BooleanCell({ value, ...props }: ComponentProps<typeof Cell> & { value: boolean }) {
  return <Cell {...props}>{value ? <BooleanTrueIcon /> : <BooleanFalseIcon />}</Cell>;
}

export const BooleanTrueIcon = styled(Check)`
  color: ${({ theme }) => theme.primary700};
  font-size: ${({ theme }) => theme.text2xl};
`;

export const BooleanFalseIcon = styled(X)`
  color: ${({ theme }) => theme.gray300};
  font-size: ${({ theme }) => theme.text2xl};
`;
