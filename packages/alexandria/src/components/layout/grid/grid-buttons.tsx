import Tippy from "@tippyjs/react";
import { Edit, Plus } from "@styled-icons/boxicons-regular";
import { ComponentProps, forwardRef, Ref } from "react";
import { GridButton } from "./grid-body.js";
import { IdCard } from "@styled-icons/boxicons-solid";
import { Button, ButtonProps } from "../../inputs/button.js";

export function EditButton(props: ComponentProps<typeof GridButton>) {
  return (
    <Tippy content="Szerkesztés">
      <GridButton {...props}>
        <Edit />
      </GridButton>
    </Tippy>
  );
}

export const AddButton = forwardRef(
  (
    {
      $kind = "primary",
      children = (
        <>
          <Plus /> Add
        </>
      ),
      ...props
    }: ButtonProps,
    ref: Ref<HTMLAnchorElement>
  ) => {
    return (
      <Button $kind={$kind} ref={ref} {...props}>
        {children}
      </Button>
    );
  }
);

export function AdminButton(props: ComponentProps<typeof GridButton>) {
  return (
    <Tippy content="Administrator">
      <GridButton {...props}>
        <IdCard />
      </GridButton>
    </Tippy>
  );
}
