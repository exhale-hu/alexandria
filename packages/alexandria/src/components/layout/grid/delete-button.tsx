import { Trash } from "@styled-icons/boxicons-regular";
import Tippy from "@tippyjs/react";
import { ComponentProps, ComponentType, ReactNode } from "react";
import { useModal } from "../../utils/modal-provider.js";
import { ConfirmModal } from "../../utils/simple-modal.js";
import { GridButton } from "./grid-body.js";

export interface DeleteButtonProps {
  title?: ReactNode;
  content: ReactNode;
  component?: ComponentType<{ onClick(): void }>;

  onConfirm(): void;

  confirmLabel?: string;
  cancelLabel?: string;
}

export function ConfirmDeleteButton({
  title,
  content,
  component,
  onConfirm,
  confirmLabel = "Delete",
  cancelLabel = "Cancel",
}: DeleteButtonProps) {
  const { showModal } = useModal();

  async function showDeleteModal() {
    const confirmed = await showModal(
      <ConfirmModal kind="danger" icon={Trash} title={title} confirmLabel={confirmLabel} cancelLabel={cancelLabel}>
        {content}
      </ConfirmModal>
    );

    if (confirmed) {
      onConfirm();
    }
  }

  const Component = component || DeleteButton;

  return <Component onClick={showDeleteModal} />;
}

export function DeleteButton(props: ComponentProps<typeof GridButton>) {
  return (
    <Tippy content="Delete">
      <GridButton $kind="linkDanger" {...props}>
        <Trash />
      </GridButton>
    </Tippy>
  );
}
