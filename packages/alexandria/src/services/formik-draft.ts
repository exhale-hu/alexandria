import { createInstance, INDEXEDDB } from "localforage";
import { useLocation } from "react-router-dom";
import { filter, map, switchMap, throttleTime } from "rxjs/operators";
import { cloneDeepWith } from "lodash-es";
import { useAction } from "use-action";
import { merge, Subject } from "rxjs";
import { useObservable } from "@litbase/use-observable";
import { useFormikWrapperContext } from "../components/index.js";

const draftsExpireInMs = 60 * 60 * 1000; // 1 hour
const draftChange = new Subject<[string, unknown]>();
const throttledDraftChange = draftChange.pipe(throttleTime(2000, undefined, { leading: false, trailing: true }));

throttledDraftChange.subscribe(([draftKey, values]) => {
  setDraft(draftKey, values).catch(console.error);
});

const store = createInstance({
  name: "exhale-admin",
  version: 1.0,
  driver: INDEXEDDB, // So Date objects can be stored
});

function useDraftKey(suffix?: string) {
  const location = useLocation();
  return `formik-draft-${location.pathname}${suffix ? "-" + suffix : ""}`;
}

export function useFormikDraft<TDoc>(suffix?: string) {
  const draftKey = useDraftKey(suffix);

  const [draft, isLoading] = useObservable(($inputs) => $inputs.pipe(switchMap(([key]) => getDraft(key))), null, [
    draftKey,
  ]);

  return [draft as TDoc | null, isLoading, draftKey];
}

export function useHasFormikDraft(suffix?: string) {
  const draftKey = useDraftKey(suffix);

  const [hasDraft] = useObservable(
    ($inputs) =>
      $inputs.pipe(
        switchMap(([key]) =>
          merge(
            store.getItem(key),
            throttledDraftChange.pipe(
              filter(([changedKey]) => changedKey === key),
              map(([, newValue]) => newValue)
            )
          )
        ),
        map((value) => !!value)
      ),
    false,
    [draftKey]
  );

  return [hasDraft, draftKey] as [boolean, string];
}

export async function removeDraft(key: string) {
  await store.removeItem(key);

  draftChange.next([key, null]);
}

export function DraftAutosaver({ suffix }: { suffix?: string }) {
  const draftKey = useDraftKey(suffix);
  const { values, touched, isSubmitting } = useFormikWrapperContext((context) => [
    context.values,
    context.touched,
    context.isSubmitting,
  ]);

  useAction(() => {
    if (isSubmitting) return;

    // If nothing is touched, don't do anything
    if (!Object.values(touched).some((value) => !!value)) return;

    draftChange.next([draftKey, values]);
  }, [draftKey, values, touched]);

  return null;
}

export function clearDrafts() {
  return store.clear();
}

async function getDraft(key: string) {
  const value = await store.getItem(key);

  if (!value) return null;

  const [normalizedValues, createdAt] = value as [unknown, Date];

  if (draftsExpireInMs < Date.now() - createdAt.getTime()) {
    await store.removeItem(key);
    return null;
  }

  return cloneDeepWith(normalizedValues, () => {
    // if (value instanceof Date) return Timestamp.fromDate(value);

    return undefined;
  });
}

async function setDraft(key: string, values: unknown) {
  if (!values) return;

  const normalizedValues = cloneDeepWith(values, () => {
    // if (value instanceof Timestamp) return value.toDate();

    return undefined;
  });

  await store.setItem(key, [normalizedValues, new Date()]);
}
