import { ComponentType, ReactNode } from "react";
import { toast, ToastContainer } from "react-toastify";
import { CheckCircle } from "@styled-icons/boxicons-regular";
import { Error } from "@styled-icons/boxicons-solid";
import { FontWeight, spacing3 } from "../themes/index.js";
import styled from "styled-components";

export { ToastContainer };

export function showSuccessToast(title: ReactNode, content: ReactNode) {
  return toast.success(<Toast icon={ToastSuccessIcon} title={title} content={content} />);
}

export function showErrorToast(title: ReactNode, content: ReactNode) {
  return toast.error(<Toast icon={ToastErrorIcon} title={title} content={content} />);
}

interface ToastProps {
  icon: ComponentType;
  title: ReactNode;
  content: ReactNode;
}

function Toast({ title, content }: ToastProps) {
  return (
    <StyledToast>
      <ToastBody>
        <ToastTitle>{title}</ToastTitle>
        <ToastContent>{content}</ToastContent>
      </ToastBody>
    </StyledToast>
  );
}

const StyledToast = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
`;

const ToastIcon = styled.div`
  font-size: ${({ theme }) => theme.text2xl};
  margin-right: ${spacing3};
`;

const ToastSuccessIcon = styled(ToastIcon).attrs(() => ({ as: CheckCircle }))`
  color: ${({ theme }) => theme.green400};
`;

const ToastErrorIcon = styled(ToastIcon).attrs(() => ({ as: Error }))`
  color: ${({ theme }) => theme.red600};
`;

const ToastBody = styled.div`
  flex-grow: 1;
  font-size: ${({ theme }) => theme.textSm};
  width: 100%;
`;

const ToastTitle = styled.div`
  font-weight: ${FontWeight.Medium};
  margin-bottom: ${({ theme }) => theme.spacing1};
`;

const ToastContent = styled.div`
  color: ${({ theme }) => theme.gray600};
`;
