import { BaseThemeInterface } from "../themes.js";

declare module "styled-components" {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends BaseThemeInterface {}
}
