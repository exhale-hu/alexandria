declare module "*.svg" {
  import { ComponentProps, FunctionComponent } from "react";
  // Declared as FunctionalComponent instead of ComponentType, as the latter causes a "TS2589: Type instantiation
  // is excessively deep and possibly infinite." error
  // Found solution at: https://github.com/mui-org/material-ui/issues/17275
  const value: FunctionComponent<ComponentProps<"svg">>;
  export default value;
}

declare module "!file-loader?modules!*.svg" {
  const value: string;
  export default value;
}
