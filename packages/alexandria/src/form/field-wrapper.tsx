import { CSSProperties, ReactNode, useContext, useMemo } from "react";
import styled from "styled-components";
import { useField, UseFieldOptions } from "./hooks/use-field.js";
import { fieldMaxWidth, FormRowBase } from "../components/form/form-layout.js";
import { FieldLabel } from "../components/form/field-label.js";
import { FormSectionContext } from "./form-section.js";
import { FontWeight } from "../themes/index.js";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface FieldProps<TOutside = any, TInside = any> extends UseFieldOptions<TOutside, TInside> {
  name: string;
  label?: ReactNode;

  // Text to be placed between the label and the form control itself as optional info
  description?: ReactNode;

  // If set to <true> and no "label" property is defined, the "name" property's
  // value is used instead -- for <false>, the label is skipped entirely in this case.
  // default : true
  labelFallsBackToName?: boolean;
}

export interface FormFieldWrapperProps extends FieldProps {
  children: ReactNode;
  className?: string;
  style?: CSSProperties;
}

export interface FieldComponentProps<T> {
  value: T | undefined;
  onChange: (value: T) => void;
  error?: { message: string };
  id: string;
}

export function FormFieldWrapper({
  name,
  label,
  children,
  className,
  required,
  email,
  minLength,
  baseSchema,
  style,
  description,
  labelFallsBackToName = true,
}: FormFieldWrapperProps) {
  const validationProps = useMemo(() => {
    return { required, email, minLength, baseSchema };
  }, [required, email, minLength, baseSchema]);

  const section = useContext(FormSectionContext);
  const { error, id } = useField(name, validationProps);

  return (
    <FieldContainer className={className} style={style}>
      <FieldLabel htmlFor={id}>
        {section.labelPrefix}
        {label || (labelFallsBackToName && name)}
        {section.labelAffix}
      </FieldLabel>
      {description && <FieldDescription>{description}</FieldDescription>}
      {children}
      {error && <ErrorMessage>{error.message}</ErrorMessage>}
    </FieldContainer>
  );
}

/**
 * Wrapper div around whatever you pass as "description" to a FormFieldWrapper.
 * This wrapper is placed AFTER the FieldLabel and BEFORE the content(children) itself
 */
export const FieldDescription = styled.div`
  font-size: ${({ theme }) => theme.textXs};
  font-weight: ${FontWeight.Normal};
  display: block;
  line-height: ${({ theme }) => theme.leading5};
  margin-bottom: ${({ theme }) => theme.spacing1};
`;

export const FieldContainer = styled(FormRowBase)<{ $fullWidth?: boolean }>`
  max-width: ${({ $fullWidth }) => ($fullWidth ? "none" : fieldMaxWidth)};
  ${({ $fullWidth }) => ($fullWidth ? "width: 100%;" : "")}

  & + &,
    ${FormRowBase} + & {
    margin-top: ${({ theme }) => theme.spacing6};
  }
`;

const ErrorMessage = styled.div`
  color: ${({ theme }) => theme.red600};
  font-size: ${({ theme }) => theme.textSm};
  margin-top: ${({ theme }) => theme.spacing2};
`;
