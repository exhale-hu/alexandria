import { cloneElement, createContext, ReactElement, ReactNode, useContext, useMemo } from "react";

export interface FormSectionFactory<T> {
  (parent: SimpleSectionOptions): T;
}

export interface FormSectionOptions {
  namePrefix?: string | FormSectionFactory<string>;
  nameAffix?: string | FormSectionFactory<string>;
  labelPrefix?: ReactNode | FormSectionFactory<ReactNode>;
  labelAffix?: ReactNode | FormSectionFactory<ReactNode>;
}

export interface SimpleSectionOptions {
  namePrefix?: string;
  nameAffix?: string;
  labelPrefix?: ReactNode;
  labelAffix?: ReactNode;
}

export const FormSectionContext = createContext<SimpleSectionOptions>({});

export interface FormSectionProps extends FormSectionOptions {
  children?: ReactNode;
}

export function FormSection({ namePrefix, nameAffix, labelPrefix, labelAffix, children }: FormSectionProps) {
  const parentSection = useContext(FormSectionContext);

  const contextValue = useMemo<SimpleSectionOptions>(
    () => ({
      namePrefix: getSectionValue(namePrefix, parentSection, "namePrefix"),
      nameAffix: getSectionValue(nameAffix, parentSection, "nameAffix"),
      labelPrefix: getSectionValue(labelPrefix, parentSection, "labelPrefix"),
      labelAffix: getSectionValue(labelAffix, parentSection, "labelAffix"),
    }),
    [namePrefix, nameAffix, labelPrefix, labelAffix, parentSection]
  );

  return <FormSectionContext.Provider value={contextValue}>{children}</FormSectionContext.Provider>;
}

function getSectionValue<T>(
  value: T | FormSectionFactory<T>,
  parentSection: SimpleSectionOptions,
  name: keyof SimpleSectionOptions
): T {
  if (isFactory<T>(value)) {
    return value(parentSection);
  }

  switch (name) {
    case "namePrefix":
      return `${parentSection.namePrefix || ""}${value || ""}` as T;
    case "nameAffix":
      return `${value || ""}${parentSection.nameAffix || ""}` as T;
    case "labelPrefix":
      return mergeElements(parentSection.labelPrefix, value) as T;
    case "labelAffix":
      return mergeElements(value, parentSection.labelAffix) as T;
    default:
      throw new Error(`Unknown key ${name} for form section`);
  }
}

function isFactory<T>(value: unknown): value is FormSectionFactory<T> {
  return typeof value === "function";
}

function mergeElements(a: unknown, b: unknown) {
  const array = [...arrayize(a), ...arrayize(b)].filter((v) => v != null) as ReactElement[];

  // Prevent unassigned key errors
  for (let i = 0; i < array.length; i++) {
    array[i] = cloneElement(array[i], { key: i });
  }

  return array;
}

function arrayize(value: unknown) {
  if (Array.isArray(value)) {
    return value;
  }

  return [value];
}
