import { FormManager } from "./form-manager.js";
import { useForm } from "./form.js";
import { ReactElement, ReactNode } from "react";

export interface FormPredicateProps<T extends object = object> {
  predicate?: (state: FormManager<T>) => boolean;
  children?: ReactNode;
}

export function FormPredicate<T extends object = object>({ predicate = () => true, children }: FormPredicateProps<T>) {
  const show = useForm<T>(predicate);

  return show ? (children as ReactElement) : null;
}
