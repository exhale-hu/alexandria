import { Button, ButtonGroup } from "../../components/inputs/button.js";
import { ReactNode } from "react";
import { useField } from "../hooks/use-field.js";
import Tippy from "@tippyjs/react";
import styled from "styled-components";
import { InfoCircle } from "@styled-icons/boxicons-solid";
import { spacing1 } from "../../themes/index.js";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";

export interface FormButtonGroupFieldProps extends FieldProps {
  options?: { label: string; value: unknown; tooltip?: ReactNode }[];
}

export function FormButtonGroupField({ name, label, options = [], ...props }: FormButtonGroupFieldProps) {
  const { value: fieldValue, setValue } = useField(name, props);

  return (
    <FormFieldWrapper name={name} label={label} {...props}>
      <ButtonGroup>
        {options.map(({ label, value, tooltip }, index) => {
          const button = (
            <Button key={index} $isActive={fieldValue === value} onClick={() => setValue(value)}>
              {label} {!!tooltip && <TooltipIndicator />}
            </Button>
          );

          if (tooltip) {
            return (
              <Tippy key={index} content={tooltip}>
                {button}
              </Tippy>
            );
          }

          return button;
        })}
      </ButtonGroup>
    </FormFieldWrapper>
  );
}

const TooltipIndicator = styled(InfoCircle)`
  font-size: ${({ theme }) => theme.textXs};
  margin-left: ${spacing1};
  margin-right: 0 !important;
  opacity: 0.65;
`;
