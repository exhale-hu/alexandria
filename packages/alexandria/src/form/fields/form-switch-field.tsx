import { Switch } from "../../components/index.js";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { useField } from "../hooks/use-field.js";

export function FormSwitchField({ name, ...props }: FieldProps) {
  const { value, setValue, id } = useField<boolean>(name, props);

  return (
    <FormFieldWrapper name={name} {...props}>
      <Switch id={id} value={!!value} onChange={setValue} />
    </FormFieldWrapper>
  );
}
