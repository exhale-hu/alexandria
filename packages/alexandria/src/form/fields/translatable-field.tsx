import { FlagIcon } from "../../components/index.js";
import { ReactNode, useContext } from "react";
import { FormSection } from "../form-section.js";
import { LanguageKey } from "../../models/translated-object.js";
import { FormLanguageContext } from "../form-language-provider.js";

export interface FormTranslatableFieldProps {
  children: ReactNode;
  language?: LanguageKey;
}

export function FormTranslatableField({ children, language: propLanguage }: FormTranslatableFieldProps) {
  const { flags, language: currentLanguage } = useContext(FormLanguageContext);
  const language = propLanguage || currentLanguage;

  return (
    <FormSection
      nameAffix={`.${language}`}
      labelAffix={
        <>
          {" "}
          <FlagIcon flags={flags} language={language} />
        </>
      }
    >
      {children}
    </FormSection>
  );
}
