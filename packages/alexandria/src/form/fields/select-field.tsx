import { useEffect, useMemo, useState } from "react";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { Select, SelectProps } from "../../components/form/select.js";
import { useField } from "../hooks/use-field.js";

interface SelectFieldProps<TValue = string, TOption = TValue>
  extends Omit<SelectProps<TOption, TOption>, "name">,
    FieldProps {
  options: TOption[];
  getLabel?: (option: TOption | null) => string;
  // Transform the selected option into the value it should have in the form's state
  getValue?: (option: TOption | null) => TValue;
  // Transform an option to a string representation, by which it can be uniquely identified
  optionToValueString?: (option: TOption) => string;
  // Transform a value from the form's state to a string representation.
  // valueToValueString(getValue(option)) == optionToValueString(option) should be true
  valueToValueString?: (value: TValue | undefined | null) => string;
}

export function FormSelectField<TValue = string, TOption = TValue>({
  options,
  getLabel = (item) => (item as unknown as { label: string })?.label,
  getValue = (item) => (item as unknown as { value: TValue })?.value,
  optionToValueString = getValue as unknown as (option: TOption) => string,
  valueToValueString = (item) => item as unknown as string,
  onChange,
  name,
  ...props
}: SelectFieldProps<TValue, TOption> & FieldProps) {
  const { value, setValue, id } = useField<TValue>(name, props);

  const [valueOption, setValueOption] = useState<TOption | undefined>(undefined);

  const optionsProp = useMemo(() => {
    const formattedOptions = options.map(
      (elem) => (typeof elem === "string" ? { value: elem, label: elem } : elem) as TOption
    );
    const isValueInOptions =
      !valueOption ||
      !!formattedOptions.find((option) => !!option && optionToValueString(option) === optionToValueString(valueOption));
    return !valueOption || isValueInOptions ? formattedOptions : [valueOption, ...formattedOptions];
  }, [JSON.stringify(options), value]);

  useEffect(() => {
    if (value) {
      setValueOption(optionsProp.find((option) => optionToValueString(option) === valueToValueString(value)));
    }
  }, [optionsProp, value]);

  return (
    <FormFieldWrapper name={name} {...props}>
      <Select<TOption, TOption>
        options={optionsProp}
        getOptionLabel={getLabel}
        getOptionValue={optionToValueString}
        onChange={(option) => {
          setValue(getValue(option as TOption));
        }}
        value={optionsProp.find((option) => optionToValueString(option) === valueToValueString(value))}
        inputId={id}
        {...props}
      />
    </FormFieldWrapper>
  );
}
