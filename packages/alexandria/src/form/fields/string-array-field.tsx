import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { Button, TextInput, TextInputProps } from "../../components/index.js";
import { ComponentPropsWithoutRef, useState } from "react";
import styled from "styled-components";
import { array } from "yup";
import { Plus, X } from "@styled-icons/boxicons-regular";
import { useField } from "../hooks/use-field.js";

interface StringArrayFieldProps extends Omit<TextInputProps, "minLength" | "required" | "name">, FieldProps {
  buttonText?: string;
  containerProps?: ComponentPropsWithoutRef<"div">;
  editable?: boolean;
}

export function FormStringArrayField({
  name,
  buttonText = "Add",
  children,
  containerProps,
  minLength,
  required,
  editable,
  ...props
}: StringArrayFieldProps) {
  // The content of the item in the "add new" input field
  const [nextItem, setNextItem] = useState("");

  const { error, id, setValue, value } = useField<string[]>(name, { minLength, required, ...props });

  // Index of item in "value" array being edited (if editing)
  const [editingField, setEditingField] = useState<number | null>(null);
  // Cached version of the item being edited (if any), is written to field in onBlur
  const [editingFieldText, setEditingFieldText] = useState<string | undefined>();

  return (
    <FormFieldWrapper name={name} baseSchema={array()} {...props}>
      <Container {...containerProps}>
        <BubbleRow>
          {value?.map((elem, index) => (
            <Bubble
              key={index}
              onClick={() => {
                setEditingFieldText(elem);
                setEditingField(index);
              }}
            >
              {editable && editingField === index ? (
                <>
                  <InlineEditInput
                    ref={(instance) => {
                      if (instance) instance?.focus();
                    }}
                    value={editingFieldText}
                    onChange={(e) => setEditingFieldText(e.target.value)}
                    onBlur={(event) => {
                      if (!editingFieldText) return;
                      const arr = [...value];
                      arr[index] = editingFieldText;
                      setValue(arr);
                      setEditingField(null);
                    }}
                    type="text"
                  />
                </>
              ) : (
                <>
                  {typeof elem !== "object" ? elem : null}{" "}
                  <X onClick={() => setValue(value.filter((elem, i) => i !== index))} />
                </>
              )}
            </Bubble>
          ))}
        </BubbleRow>
        <Row>
          <TextInput
            {...props}
            hasError={!!error}
            id={id}
            value={nextItem}
            onChange={(e) => setNextItem(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                submit();
              }
            }}
          />
          <StyledButton $size="sm" onClick={submit}>
            <Plus />
            {buttonText && <span>{buttonText}</span>}
          </StyledButton>
        </Row>
      </Container>
    </FormFieldWrapper>
  );

  function submit() {
    if (nextItem) {
      setValue([...(value || []), nextItem]);
    }
    setNextItem("");
  }
}

const Bubble = styled.span`
  font-size: 11px;
  padding: 0.25rem 0.5rem;
  border: 1px solid ${({ theme }) => theme.gray200};
  border-radius: ${({ theme }) => theme.rounded};
  cursor: pointer;
  transition: background-color 0.3s, color 0.3s;
  &:hover {
    background: lightpink;
    color: white;
  }
`;

const InlineEditInput = styled.input`
  &[type="text"] {
    font-size: inherit;
    border: none;
    padding: 0;
  }
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  > * + button {
    margin-top: 0;
    margin-left: 0.5rem;
  }
`;

const BubbleRow = styled.div`
  display: flex;
  margin: -0.25rem;
  margin-bottom: 0.25rem;
  width: 100%;
  flex-wrap: wrap;
  > * {
    margin: 0.25rem;
  }
`;

const Container = styled.div``;

const StyledButton = styled(Button)`
  > svg:first-child {
    margin-right: 0;
  }
  span {
    margin-left: 0.5rem;
  }
`;
