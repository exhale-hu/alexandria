import { ComponentPropsWithoutRef } from "react";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { TextInput } from "../../components/form/input.js";
import { useField } from "../hooks/use-field.js";

type InputFieldProps = Omit<ComponentPropsWithoutRef<"input">, "required" | "minLength"> & FieldProps;

export function FormInputField({ name, className, minLength, required, ...props }: InputFieldProps) {
  const { value, error, setValue, id } = useField<string | number>(name, { minLength, required, ...props });

  return (
    <FormFieldWrapper name={name} className={className} {...props}>
      <TextInput
        hasError={!!error}
        value={value ? String(value) : props.type === "number" ? "0" : ""}
        onChange={(e) => setValue(props.type === "number" ? Number(e.target.value) : e.target.value)}
        id={id}
        name={name}
        {...props}
      />
    </FormFieldWrapper>
  );
}
