import { afterEach, describe, expect, it, vi } from "vitest";
import { cleanup, render, userEvent, waitFor } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormFileUploadField, FormSingleFileUploadField } from "../fields/file-upload-field.js";
import { FileEntry } from "../../components/form/file-upload/file-entry.js";
import { from } from "rxjs";
import { FormManager } from "../form-manager.js";

describe("File upload field", () => {
  afterEach(cleanup);
  it("should upload image and use url as value", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ img: "" }}>
        <FormSingleFileUploadField label="Image" name="img" onUploadFile={dummyOnFileUpload} />
        <button type="submit">Submit</button>
      </Form>
    );
    const dummyFile = new File(["helloThere"], "chucknorris.png", { type: "image/png" });
    await userEvent.upload(screen.getByText("Image"), dummyFile);
    const submitButton = screen.getByText("Submit");
    await userEvent.click(submitButton);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ img: "chucknorris.png" }, expect.any(FormManager));
    });
  });

  it("should upload multiple images an use value as url array", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ img: "" }}>
        <FormFileUploadField label="Image" name="img" onUploadFile={dummyOnFileUpload} />
        <button type="submit">Submit</button>
      </Form>
    );
    const dummyFile1 = new File(["helloThere"], "chucknorris.png", { type: "image/png" });
    const dummyFile2 = new File(["helloThere"], "chucknorris2.png", { type: "image/png" });
    await userEvent.upload(screen.getByText("Image"), [dummyFile1, dummyFile2]);
    const submitButton = screen.getByText("Submit");
    await userEvent.click(submitButton);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ img: ["chucknorris.png", "chucknorris2.png"] }, expect.any(FormManager));
    });
  });
});

function dummyOnFileUpload(file: FileEntry) {
  return from([{ ...file }, { ...file, contentUrl: file.file?.name, progress: 100, status: "done" as const }]);
}
