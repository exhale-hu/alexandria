import { describe, it, vi, expect, afterEach } from "vitest";
import { cleanup, render, userEvent, waitFor } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormSelectField } from "../fields/select-field.js";
import { select } from "react-select-event";
import { BinaryId, createBinaryId } from "@litbase/core";
import { FormManager } from "../form-manager.js";
import { useState } from "react";

describe("Select field", () => {
  afterEach(cleanup);

  it("should store string value in form state if options are provided as strings", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormSelectField name="category" options={["a", "b", "c"]} />
        <button type="submit">Submit</button>
      </Form>
    );

    await select(screen.getByLabelText("category"), "b");

    await userEvent.click(screen.getByRole("button"));

    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(onSubmit).toHaveBeenCalledWith({ category: "b" }, expect.any(FormManager));
  });

  it("should store string value in form if options are provided as {value, label}[]", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormSelectField
          name="category"
          options={[
            { value: "a", label: "a" },
            { value: "b", label: "b" },
            { value: "c", label: "c" },
          ]}
        />
        <button type="submit">Submit</button>
      </Form>
    );

    await select(screen.getByLabelText("category"), "b");

    userEvent.click(screen.getByRole("button"));

    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(onSubmit).toHaveBeenCalledWith({ category: "b" }, expect.any(FormManager));
  });

  it("should work properly with custom objects and custom getLabel, getValue functions", async () => {
    const onSubmit = vi.fn();
    const options = [
      { _id: createBinaryId(), name: "a" },
      { _id: createBinaryId(), name: "b" },
      { _id: createBinaryId(), name: "c" },
    ];
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormSelectField<BinaryId | undefined, { _id: BinaryId; name: string }>
          name="category"
          options={options}
          getLabel={(elem) => elem?.name || ""}
          getValue={(elem) => elem?._id}
          optionToValueString={(elem) => {
            return elem._id.value;
          }}
        />
        <button type="submit">Submit</button>
      </Form>
    );

    await select(screen.getByLabelText("category"), "b");

    await userEvent.click(screen.getByRole("button"));

    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(onSubmit).toHaveBeenCalledWith({ category: options[1]._id }, expect.any(FormManager));
  });

  it("should read custom values properly from defaultState", async () => {
    const onSubmit = vi.fn();
    const options = [
      { _id: createBinaryId(), name: "a" },
      { _id: createBinaryId(), name: "b" },
      { _id: createBinaryId(), name: "c" },
    ];
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ category: options[1]._id }}>
        <FormSelectField<BinaryId | undefined, { _id: BinaryId; name: string }>
          name="category"
          options={options}
          getLabel={(elem) => elem?.name || ""}
          getValue={(elem) => elem?._id}
          optionToValueString={(elem) => elem._id.value}
          valueToValueString={(_id) => _id?.value || ""}
        />
        <button type="submit">Submit</button>
      </Form>
    );

    expect(screen.getByText("b")).toBeInTheDocument();
  });

  it("should show value even if its not currently present among options", async () => {
    const startingOptions = [
      { _id: createBinaryId(), name: "asdf" },
      { _id: createBinaryId(), name: "bsdf" },
      { _id: createBinaryId(), name: "csdf" },
    ];
    function TestCase() {
      const [options, setOptions] = useState(startingOptions);
      return (
        <Form onSubmit={() => {}} defaultValues={{ category: startingOptions[0]._id }}>
          <FormSelectField<BinaryId | undefined, { _id: BinaryId; name: string }>
            name="category"
            options={options}
            getLabel={(elem) => elem?.name || ""}
            getValue={(elem) => elem?._id}
            optionToValueString={(elem) => elem._id.value}
            valueToValueString={(_id) => _id?.value || ""}
          />
          {!!options.length && <button onClick={() => setOptions([])}>Clear options</button>}
        </Form>
      );
    }
    const screen = render(<TestCase />);
    await waitFor(() => {
      expect(screen.getByText("asdf")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Clear options"));
    await waitFor(() => {
      expect(screen.getByText("asdf")).toBeInTheDocument();
    });
  });
});
