import { describe, it, vi, afterEach } from "vitest";
import { cleanup, render, userEvent, waitFor } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormStringArrayField } from "../fields/string-array-field.js";
import { FormManager } from "../form-manager.js";

describe("String array field", () => {
  afterEach(cleanup);
  it("should allow adding elements on enter, and removing them on click", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormStringArrayField name="elements" label="Elements" />
        <button type="submit">Submit</button>
      </Form>
    );

    await userEvent.type(screen.getByLabelText("Elements"), "Bob{enter}");
    await userEvent.type(screen.getByLabelText("Elements"), "Jeff{enter}");
    await waitFor(() => {
      expect(screen.getByText("Bob")).toBeInTheDocument();
    });

    const bobElement = screen.getByText("Bob");
    const bobDeleteElement = bobElement.querySelector("svg") as Element;

    await userEvent.click(bobDeleteElement);
    await waitFor(() => {
      expect(screen.queryByText("Bob")).not.toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Submit"));
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ elements: ["Jeff"] }, expect.any(FormManager));
    });
  });
});
