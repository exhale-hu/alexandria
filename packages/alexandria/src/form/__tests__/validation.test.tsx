import { describe, it, expect, vi, afterEach } from "vitest";
import { cleanup, render, userEvent, waitForElementToBeRemoved } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormInputField } from "../fields/input-field.js";
import { noop } from "../../components/utils/noop.js";

describe("Validation", () => {
  afterEach(cleanup);
  it("should not submit when required fields are not filled in", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormInputField name="name" required="Name is required" />
        <button type="submit">Submit</button>
      </Form>
    );
    const submitButton = screen.getByRole("button");
    userEvent.click(submitButton);
    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(onSubmit).not.toHaveBeenCalled();
    expect(screen.getByText("Name is required")).toBeInTheDocument();
  });

  it("should make error message disppear once its not applicable anymore", async () => {
    const screen = render(
      <Form onSubmit={noop}>
        <FormInputField name="name" required="Name is required" />
        <button type="submit">Submit</button>
      </Form>
    );
    const submitButton = screen.getByRole("button");
    userEvent.click(submitButton);
    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(screen.getByText("Name is required")).toBeInTheDocument();
    const promise = waitForElementToBeRemoved(() => screen.queryByText("Name is required"));
    userEvent.type(screen.getByRole("textbox"), "Bob");
    await promise;
  });
});
