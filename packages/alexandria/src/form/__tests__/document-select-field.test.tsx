import { describe, it, vi, afterEach } from "vitest";
import { cleanup, render, userEvent } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { select } from "react-select-event";
import { FormDocumentSelectField } from "../fields/document-select-field.js";
import { createBinaryId, litql } from "@litbase/core";
import { FormManager } from "../form-manager.js";

vi.mock("@litbase/core");

describe("Select field", () => {
  afterEach(cleanup);
  it("should make documents queried from server selectable", async () => {
    const onSubmit = vi.fn();
    const options = [
      { _id: createBinaryId(), name: "a" },
      { _id: createBinaryId(), name: "b" },
      { _id: createBinaryId(), name: "c" },
    ] as const;
    await litql.query("docs", { $push: options });
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormDocumentSelectField<typeof options[number]>
          name="category"
          collection="docs"
          getLabel={(elem) => elem?.name || ""}
        />
        <button type="submit">Submit</button>
      </Form>
    );

    await select(screen.getByLabelText("category"), "b");

    await userEvent.click(screen.getByRole("button"));

    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(onSubmit).toHaveBeenCalledWith({ category: options[1]._id }, expect.any(FormManager));
  });

  it("should load in selected document by _id", async () => {
    const onSubmit = vi.fn();
    const options = [
      { _id: createBinaryId(), name: "a" },
      { _id: createBinaryId(), name: "b" },
      { _id: createBinaryId(), name: "c" },
    ] as const;
    await litql.query("docs", { $push: options });
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ category: options[2]._id }}>
        <FormDocumentSelectField<typeof options[number]>
          name="category"
          collection="docs"
          getLabel={(elem) => elem?.name || ""}
        />
        <button type="submit">Submit</button>
      </Form>
    );

    await new Promise((resolve) => setTimeout(resolve, 100));
    expect(screen.getByText("c")).toBeInTheDocument();
  });
});
