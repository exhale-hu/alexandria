import {
  ComponentPropsWithoutRef,
  createContext,
  FormEvent,
  KeyboardEvent,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from "react";
import { spacing } from "../themes/index.js";
import styled from "styled-components";
import { FormManager } from "./form-manager.js";
import { useObservable } from "@litbase/use-observable";
import { map, switchMap } from "rxjs/operators";
import { BaseSchema } from "yup";

const FormContext = createContext<FormManager<any> | null>(null);

export function useFormContext<T extends object = object>() {
  return useContext(FormContext) as unknown as FormManager<T>;
}

export function useForm<T extends object = object, TReturn = any>(
  selector: (manager: FormManager<T>) => TReturn,
  deps?: unknown[]
) {
  const manager = useFormContext<T>();

  if (!manager) {
    throw new Error("useForm used outside Form context");
  }

  const [value] = useObservable(
    (inputs$) => inputs$.pipe(switchMap(() => manager.state$.pipe(map(() => selector(manager))))),
    null as unknown as TReturn,
    deps as unknown[]
  );

  return value;
}

export interface FormProps<T extends object = object> extends Omit<ComponentPropsWithoutRef<"form">, "onSubmit"> {
  children: ReactNode;
  onSubmit: (values: T, manager: FormManager<T>) => void;
  defaultValues?: T;
  watchDefaultValues?: boolean;
  preventEnter?: boolean;
  schema?: BaseSchema | null;
}

export function Form<T extends object = object>({
  children,
  defaultValues = {} as T,
  onSubmit,
  watchDefaultValues = false,
  preventEnter = false,
  schema = null,
  ...props
}: FormProps<T>) {
  const manager = useMemo(() => new FormManager(defaultValues, onSubmit), []);
  manager.schema = schema;
  if (watchDefaultValues) {
    manager.setDefaultValues(defaultValues);
    manager.setValues(defaultValues, true);
  }

  const onKeyPressHandler = useCallback(
    (event: KeyboardEvent<HTMLFormElement>) => {
      if (preventEnter && event.key === "Enter") {
        event.preventDefault();
      }
    },
    [preventEnter]
  );

  const onSubmitHandler = useCallback(async (event?: FormEvent<HTMLFormElement>) => {
    event?.preventDefault();
    await manager.submitForm();
  }, []);

  useEffect(() => {
    manager.onSubmit = onSubmit;
  }, [manager, onSubmit]);

  return (
    <FormContext.Provider value={manager}>
      <StyledForm onKeyPress={onKeyPressHandler} onSubmit={onSubmitHandler} {...props}>
        {children}
      </StyledForm>
    </FormContext.Provider>
  );
}

const StyledForm = styled.form`
  button {
    margin-top: ${spacing._3};
  }
`;
