import { useReducer } from "react";

export function useForceUpdate(): () => void {
  // Prior stuff:
  // https://github.com/CharlesStover/use-force-update/issues/9
  // https://github.com/CharlesStover/use-force-update/issues/18#issuecomment-555793554
  // Benchmarked:
  // - () => []
  // - () => ({})
  // - () => Object.create(null),
  // - (value) => (value + 1) % Number.MAX_VALUE,
  // - () => Symbol()
  // and the results were in that order
  return useReducer(() => [], undefined as unknown as [])[1];
}
