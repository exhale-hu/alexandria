import { useEffect, useRef } from "react";
import { config, useTransition } from "react-spring";
import { spacing4 } from "../themes/index.js";
import { delay } from "../utils/index.js";

export type TransitionStatus = "done" | "working" | "errored";

export const defaultFromStyle = {
  opacity: 0,
  transform: `translateY(${spacing4})`,
};

export const defaultLeaveStyle = {
  opacity: 0,
  transform: `translateY(-${spacing4})`,
};

export const defaultEnterStyle = {
  opacity: 1,
  transform: `translateY(0rem)`,
};

export interface UseStatusTransitionOptions {
  status: TransitionStatus;
  fromStyle?: { [key: string]: unknown };
  leaveStyle?: { [key: string]: unknown };
  enterStyle?: { [key: string]: unknown };
}

export function useStatusTransition({
  status,
  fromStyle = defaultFromStyle,
  leaveStyle = defaultLeaveStyle,
  enterStyle = defaultEnterStyle,
}: UseStatusTransitionOptions) {
  const immediateRef = useRef("done" === status);
  const cancel = immediateRef.current;

  const transitions = useTransition(status, {
    from: fromStyle,
    enter: (item) => {
      if (cancel) return leaveStyle;

      return item === "done"
        ? async (next) => {
            await next({ ...fromStyle, immediate: true });
            await next(enterStyle);
            await delay(1500);
            await next(leaveStyle);
          }
        : enterStyle;
    },
    leave: leaveStyle,
    config: config.slow,
    cancel,
  });

  useEffect(() => {
    immediateRef.current = false;
  }, []);

  return transitions;
}
