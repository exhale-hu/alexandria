import { ChangeEvent, MutableRefObject, Ref, useCallback, useRef, useState } from "react";
import { useFocus } from "./use-focus.js";
import { useHotkeys } from "./use-hotkeys.js";

export interface UseEscapableInputOptions<T> {
  // Optional reference to be passed to the input field to get the underlying html element
  ref?: Ref<HTMLElement>;

  // Modified onChange event that only fires when unfocused without pressing Escape
  onChange?(value: ChangeEvent<HTMLInputElement> | T): void;

  // onChange event that fires regularly as it would with a regular input field
  onInternalChange?(value: T): void;

  // Fired when the input is either focused or unfocused
  onFocusChange?(focused: boolean): void;

  // The usual "value" for controlled inputs
  value?: T;
}

/*
  Returns props to be passed to an input field, providing the ability to revert the contents
  of said input field when the escape button is pressed when focused.
  Pressing enter or simply unfocusing "commit" the input value (firing onChange and having the value changed).
 */
export function useEscapableInput<T = string>(options: UseEscapableInputOptions<T> = {}) {
  const isAttachedRef = useRef(true);
  const onChangeRef = useRef(options.onChange);
  const onInternalChangeRef = useRef(options.onInternalChange);
  const inputRef = useRef<HTMLElement | null>(null);
  const [inputValue, setInputValue] = useState<T | undefined>(options.value);

  onChangeRef.current = options.onChange;
  onInternalChangeRef.current = options.onInternalChange;

  const onChange = useCallback((event: ChangeEvent<HTMLInputElement> | T) => {
    const newValue = (isChangeEventObject(event) ? event.currentTarget.value : event) as T;

    if (isAttachedRef.current) {
      onChangeRef.current && onChangeRef.current(newValue);
    } else {
      setInputValue(newValue);
      onInternalChangeRef.current && onInternalChangeRef.current(newValue);
    }
  }, []);

  function attach(keepInternalValue: boolean) {
    if (isAttachedRef.current) return;

    isAttachedRef.current = true;

    if (keepInternalValue && inputValue !== options.value && options.onChange) options.onChange(inputValue!);

    setInputValue(undefined); // Release value
  }

  function detach() {
    if (!isAttachedRef.current) return;

    isAttachedRef.current = false;
    setInputValue(options.value);
  }

  const focusRef = useCallback(
    (element: HTMLElement | null) => {
      inputRef.current = element;

      if (options.ref) {
        if (typeof options.ref === "function") {
          options.ref(inputRef.current);
        } else {
          (options.ref as MutableRefObject<HTMLElement | null>).current = inputRef.current;
        }
      }
    },
    [options.ref]
  );

  const [focusProps, setFocus] = useFocus({
    ref: focusRef,
    onFocusChange(focused: boolean): void {
      if (focused) detach();
      else attach(true);

      options.onFocusChange && options.onFocusChange(focused);
    },
  });

  const hotkeysProps = useHotkeys({
    ref: focusProps.ref,
    keymap: {
      enter() {
        attach(true);

        setFocus(false);
      },
      escape() {
        attach(false);

        setFocus(false);
      },
    },
    preventDefault: true,
  });

  return [
    {
      onChange,
      value: (isAttachedRef.current ? options.value : inputValue) as T,
      ...focusProps,
      ...hotkeysProps,
    },
    setFocus,
    inputRef,
  ] as const;
}

function isChangeEventObject(obj: unknown): obj is ChangeEvent {
  return !!obj && typeof obj === "object" && "currentTarget" in obj;
}
