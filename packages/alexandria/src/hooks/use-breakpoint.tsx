import useBreakpointOriginal from "use-breakpoint";
import { useMemo } from "react";
import { useTheme, DefaultTheme } from "styled-components";

export function useBreakpoint() {
  const theme = useTheme() as DefaultTheme & { breakpoints: Record<string, string> };
  const breakpoints = useMemo(
    // Using Number.parseInt() as it parses the number from the string "420px" correctly, instead of giving NaN, like
    // Number() does.
    () => Object.fromEntries(Object.entries(theme.breakpoints).map(([key, value]) => [key, Number.parseInt(value)])),
    [theme.breakpoints]
  );

  const { breakpoint } = useBreakpointOriginal(breakpoints, "sm");
  return breakpoint as keyof typeof theme.breakpoints;
}
