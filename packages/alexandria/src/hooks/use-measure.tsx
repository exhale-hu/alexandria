import { RefObject, useEffect, useLayoutEffect, useState } from "react";
import ResizeObserver from "resize-observer-polyfill";

export interface MeasureBounds {
  left: number;
  top: number;
  width: number;
  height: number;
}

// Based on: https://codesandbox.io/embed/q3ypxr5yp4
export function useMeasure<TElement extends HTMLElement = HTMLElement>(
  onChange: (bounds: MeasureBounds) => void,
  ref: RefObject<TElement>
) {
  const [resizeObserver] = useState(
    () => new ResizeObserver(([entry]: { contentRect: MeasureBounds }[]) => entry && onChange(entry.contentRect))
  );

  useLayoutEffect(() => {
    const current = ref.current;

    if (!current) return;

    resizeObserver.observe(current);

    return () => resizeObserver.unobserve(current);
  }, [ref.current]);

  // Cleanup observer on unmount
  useEffect(() => () => resizeObserver.disconnect(), []);
}
