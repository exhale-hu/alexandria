import { useEffect, useRef } from "react";

export function useEffectWithLast<TArgs extends Array<any>>(callback: (last: TArgs) => void, args: TArgs): void {
  const lastArgs = useRef(args);
  useEffect(() => {
    callback(lastArgs.current);
    lastArgs.current = args;
  }, args);
}
