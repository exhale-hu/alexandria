import { MutableRefObject, Ref, useEffect, useMemo, useRef, useState } from "react";
import { fromEvent, merge } from "rxjs";
import { filter, map } from "rxjs/operators";
import { isRefObject } from "../utils/is-ref-object.js";

interface HotkeyCallbackArgs<TContext> {
  event: KeyboardEvent;
  combo: string;
  context?: TContext;
}

type HotkeyCallback<TContext> = (args: HotkeyCallbackArgs<TContext>) => void;

export type SimpleHotkeyKeymapEntry<TContext> =
  | HotkeyCallback<TContext>
  | {
      capture?: boolean;
      event?: HotkeyEventType;
      callback: HotkeyCallback<TContext>;
    };

export interface SimpleHotkeyKeymap<TContext> {
  [key: string]: SimpleHotkeyKeymapEntry<TContext>;
}

export type HotkeyEventType = "keyup" | "keydown" | undefined;

interface UseHotkeyOptions<TContext> {
  defaultEventType?: HotkeyEventType;
  keymap?: SimpleHotkeyKeymap<TContext>;
  attach?: EventTarget;
  ref?: Ref<HTMLElement>;
  disabled?: boolean;
  stopPropagation?: DefaultSetting<TContext>;
  preventDefault?: DefaultSetting<TContext>;
}

export type DefaultSetting<TContext> = ((args: HotkeyCallbackArgs<TContext>) => boolean) | boolean;

interface NormalizedHotkeyObject<TContext> {
  entry: SimpleHotkeyKeymapEntry<TContext>;
  combo: string;
  capture: boolean;
  event: HotkeyEventType;
  keys: Set<string>;
}

interface PressedKey {
  key: string;
  normalizedKey: string;
  code: string;
}

const useHotkeysContextSymbol = Symbol("useHotkeysContext");

interface KeyboardEventWithContext<T> extends KeyboardEvent {
  [useHotkeysContextSymbol]: T;
}

const pressedKeys = new Map<string, PressedKey>();

merge(
  fromEvent<KeyboardEvent>(document, "keyup", { capture: true }),
  fromEvent<KeyboardEvent>(document, "keydown", { capture: true })
).subscribe((event) => {
  if (event.repeat) return;

  switch (event.type) {
    case "keydown":
      if (!event.key) {
        break;
      }

      pressedKeys.set(event.code, {
        normalizedKey: event.key.toLowerCase(),
        key: event.key,
        code: event.code,
      });
      break;
    case "keyup":
      // Remove key after current keyboard event finishes bubbling
      setTimeout(() => pressedKeys.delete(event.code));
      break;
  }
});

const keyAliases: { [key: string]: string } = {
  ctrl: "control",
  up: "arrowup",
  down: "arrowdown",
  left: "arrowleft",
  right: "arrowright",
  esc: "escape",
  space: " ",
  plus: "+",
  minus: "-",
};

export function useHotkeys<TContext = unknown>(props: UseHotkeyOptions<TContext>) {
  const propsRef = useRef(props);
  propsRef.current = props;

  const [currentRef, setCurrentRef] = useState<EventTarget | null>(null);

  const { attach, ref } = props;

  useEffect(() => {
    const targetRef = (attach || currentRef) as EventTarget | null;

    if (!targetRef) return;

    let processedKeymap: NormalizedHotkeyObject<TContext>[] | undefined;
    let lastDefaultEventType: HotkeyEventType | undefined;
    let lastKeymap: SimpleHotkeyKeymap<TContext> | undefined;

    const subscription = merge(
      merge(fromEvent<KeyboardEvent>(targetRef, "keydown"), fromEvent<KeyboardEvent>(targetRef, "keyup")).pipe(
        map((event: KeyboardEvent) => ({ event, capture: false }))
      ),
      merge(
        fromEvent<KeyboardEvent>(targetRef, "keydown", { capture: true }),
        fromEvent<KeyboardEvent>(targetRef, "keyup", { capture: true })
      ).pipe(map((event: KeyboardEvent) => ({ event, capture: true })))
    )
      .pipe(filter(() => !propsRef.current.disabled))
      .subscribe(({ event, capture }) => {
        const { defaultEventType = "keydown", keymap, stopPropagation, preventDefault } = propsRef.current;

        if (!processedKeymap || defaultEventType !== lastDefaultEventType || !tryPreserveKeymap(lastKeymap, keymap))
          processedKeymap = processKeymap(keymap, defaultEventType);

        handleEvent(event, capture, processedKeymap, stopPropagation, preventDefault);
      });

    return () => subscription.unsubscribe();
  }, [attach, currentRef]);

  return useMemo(() => {
    if (attach) return {};

    return {
      ref(refValue: HTMLElement | null) {
        if (typeof ref === "function") ref(refValue);
        else if (isRefObject(ref)) (ref as MutableRefObject<HTMLElement | null>).current = refValue;

        setCurrentRef(refValue);
      },
    };
  }, [ref, attach]);
}

export interface UseHotkeysContextOptions {
  attach?: EventTarget;
  ref?: Ref<HTMLElement>;
}

export function useHotkeysContext<T>(context: T | undefined, { attach, ref }: UseHotkeysContextOptions = {}) {
  const [currentRef, setCurrentRef] = useState<EventTarget | null>(null);
  const contextRef = useRef(context);
  contextRef.current = context;

  useEffect(() => {
    const targetRef = (attach || currentRef) as EventTarget | null;

    if (!targetRef) return;

    const subscription = merge(fromEvent(targetRef, "keydown"), fromEvent(targetRef, "keyup")).subscribe((event) => {
      if (undefined !== contextRef.current)
        (event as KeyboardEventWithContext<T>)[useHotkeysContextSymbol] = contextRef.current;
    });

    return () => subscription.unsubscribe();
  }, [attach, currentRef]);

  return useMemo(() => {
    if (attach) return {};

    return {
      ref(refValue: HTMLElement | null) {
        if (typeof ref === "function") ref(refValue);
        else if (isRefObject(ref)) (ref as MutableRefObject<HTMLElement | null>).current = refValue;

        setCurrentRef(refValue);
      },
    };
  }, [ref, attach]);
}

function handleEvent<TContext>(
  event: KeyboardEvent,
  isCapture: boolean,
  keymap: NormalizedHotkeyObject<TContext>[],
  stopPropagation: DefaultSetting<TContext> = false,
  preventDefault: DefaultSetting<TContext> = false
): void {
  if (event.repeat || (event.type !== "keydown" && event.type !== "keyup")) return;

  let bestMatch: NormalizedHotkeyObject<TContext> | null = null;
  let bestMatchCount = 0;
  for (const entry of keymap) {
    if (entry.capture !== isCapture || entry.event !== event.type) continue;

    let matchCount = 0;
    for (const key of pressedKeys.values()) {
      if (entry.keys.has(key.normalizedKey)) {
        matchCount++;
      }
    }

    if (entry.keys.size <= matchCount && bestMatchCount < matchCount) {
      bestMatch = entry;
      bestMatchCount = matchCount;
    }
  }

  if (bestMatch) {
    const args: HotkeyCallbackArgs<TContext> = {
      event,
      combo: bestMatch.combo,
      context: isKeyboardEventWithContext<TContext>(event) ? event[useHotkeysContextSymbol] : undefined,
    };

    if ((typeof stopPropagation === "function" && stopPropagation(args)) || stopPropagation) event.stopPropagation();

    if ((typeof preventDefault === "function" && preventDefault(args)) || preventDefault) event.preventDefault();

    if (typeof bestMatch.entry === "function") bestMatch.entry(args);
    else bestMatch.entry.callback(args);
  }
}

function processKeymap<TContext>(keymap?: SimpleHotkeyKeymap<TContext>, defaultEventType?: "keyup" | "keydown") {
  if (!keymap) return [];

  const normalizedKeymap: NormalizedHotkeyObject<TContext>[] = [];

  // TODO: support consecutive keys (not pressed at the same time, but after each other)
  for (const [key, entry] of Object.entries(keymap || {})) {
    const combos = key.split(/\s*,\s*/);

    for (const combo of combos) {
      const keys = combo.split(/\s*\+\s*/);

      for (let i = 0; i < keys.length; i++) {
        const lowercased = keys[i].toLowerCase();

        keys[i] = keyAliases[lowercased] || lowercased;
      }

      let capture = false;
      let event = defaultEventType;
      if (typeof entry !== "function") {
        if (undefined !== entry.capture) capture = entry.capture;

        if (undefined !== entry.event) event = entry.event;
      }

      const normalizedObject: NormalizedHotkeyObject<TContext> = {
        entry,
        combo,
        capture,
        event,
        keys: new Set(keys),
      };

      normalizedKeymap.push(normalizedObject);
    }
  }

  return normalizedKeymap;
}

function tryPreserveKeymap<TContext>(
  currentKeymap?: SimpleHotkeyKeymap<TContext>,
  newKeymap?: SimpleHotkeyKeymap<TContext>
) {
  if (currentKeymap === newKeymap) return true;

  if (undefined === currentKeymap || undefined === newKeymap) return false;

  const aKeys = Object.keys(currentKeymap);

  if (Object.keys(newKeymap).length !== aKeys.length) return false;

  for (const key of aKeys) {
    const aValue = currentKeymap[key];
    const bValue = newKeymap[key];

    if (aValue === bValue) continue;

    if (typeof aValue === "function" || typeof bValue === "function") {
      currentKeymap[key] = bValue;
      return true;
    }

    if (aValue.capture !== bValue.capture) return false;

    if (aValue.event !== bValue.capture) return false;

    // Keys are equal, update the old callback. If a future key mismatches, the old keymap will be thrown out anyway
    aValue.callback = bValue.callback;
  }

  return true;
}

function isKeyboardEventWithContext<T>(event: KeyboardEvent): event is KeyboardEventWithContext<T> {
  return useHotkeysContextSymbol in event;
}
