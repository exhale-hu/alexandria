/// <reference types="vitest" />
/// <reference types="vite/client" />

import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";
import * as path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@litbase/use-observable": path.resolve(__dirname, "../use-observable/src"),
      "@litbase/use-force-update": path.resolve(__dirname, "../use-force-update/src"),
      "@litbase/file-upload": path.resolve(__dirname, "../file-upload/src"),
    },
  },

  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "./src/test/setup.ts",
    isolate: false,
    open: false,
    deps: {
      inline: ["@litbase/react"],
    },
  },
});
