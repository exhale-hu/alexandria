import { ConfirmDeleteButton, GridButton } from "@litbase/alexandria";
import Tippy from "@tippyjs/react";
import { Edit } from "@styled-icons/boxicons-regular";
import { useResource } from "../../resource";
import { ReactNode, useContext } from "react";
import { ItemContext } from "../columns/item-context";
import { DataAccessContextValue, useDataAccess } from "../../../../contexts/data-provider/data-provider";
import { AdminContext } from "../../admin";

export function EditButton() {
  const adminContext = useContext(AdminContext);
  const { item } = useContext(ItemContext);
  const model = useResource();
  const { getId, idToString } = useDataAccess();
  if (!adminContext) {
    console.error("Tried to use <EditButton /> outside of an <Admin> wrapper");
  }
  if (!item || !adminContext) return null;
  return (
    <Tippy content="Edit">
      <GridButton to={createEditUrl(item, adminContext, model, { getId, idToString })}>
        <Edit />
      </GridButton>
    </Tippy>
  );
}

export function createEditUrl<ItemType>(
  item: ItemType | null,
  adminContext: { prefix?: string } | undefined | null,
  resourceContext: { name: string },
  { idToString, getId }: Pick<DataAccessContextValue<ItemType>, "idToString" | "getId">
) {
  return (adminContext?.prefix || "") + "/" + resourceContext.name + "/" + (item ? idToString(getId(item)) : "new");
}

export function DeleteButton({ content }: { content?: ReactNode }) {
  const dataAccess = useDataAccess();
  const model = useResource();
  const { item, onDeleted } = useContext(ItemContext);
  return (
    <ConfirmDeleteButton
      onConfirm={async () => {
        await dataAccess.delete(model.name, dataAccess.getId(item));
        onDeleted();
      }}
      content={content || <>Are you sure?</>}
    />
  );
}

export function PropertyButton({}: { property?: string; icon: JSX.Element }) {
  return null;
}
