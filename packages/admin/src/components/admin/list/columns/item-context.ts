import { createContext } from "react";

export const ItemContext = createContext<{ item: Record<string, unknown> | null; onDeleted: () => void }>({
  item: null,
  onDeleted: () => undefined,
});
