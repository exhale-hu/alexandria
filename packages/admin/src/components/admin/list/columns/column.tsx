import { ReactNode } from "react";
import { FieldConfig } from "../../../../types/field-types";

export interface ColumnConfig<T = any> {
  property?: string;
  displayName?: string;
  children?: ReactNode | ((item: T) => JSX.Element);
  columnSpace?: string;
  sortable?: boolean;
  customCell?: boolean;
}

export type ColumnInfo = (FieldConfig & ColumnConfig)[];

export function Column<T = any>(props: ColumnConfig<T>) {
  return null;
}
