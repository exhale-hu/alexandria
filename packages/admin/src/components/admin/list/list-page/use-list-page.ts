import { ModelConfig } from "../../types";
import { Column, ColumnConfig } from "../columns";
import { ReactNode, useEffect, useState } from "react";
import { useChildrenProps } from "../utils";
import { FieldConfig } from "../../../../types/field-types";

export function useListPage(model: ModelConfig, children: ReactNode) {
  const [columns, setColumns] = useState<Record<string, ColumnConfig & FieldConfig>>({});
  const columnProps = useChildrenProps<ColumnConfig>(children, [Column]);

  useEffect(() => {
    setColumns((prev) => ({
      ...prev,
      ...Object.fromEntries(
        columnProps.map((config: ColumnConfig) => [
          config.property,
          { ...config, ...(config.property ? model.fields[config.property] : {}) },
        ])
      ),
    }));
  }, [columnProps, model]);

  return { columns };
}
