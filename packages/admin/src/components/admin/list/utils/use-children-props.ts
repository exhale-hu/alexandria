import { Children, isValidElement, JSXElementConstructor, ReactElement, ReactNode, useMemo } from "react";

export function useChildrenProps<T = Record<string, unknown>>(
  children: ReactNode,
  allowedComponents?: JSXElementConstructor<any>[]
): T[] {
  return useMemo(() => {
    return Children.toArray(children)
      .filter((child) => {
        if (!isValidElement(child)) return false;

        if (allowedComponents) {
          return typeof child.type !== "string" && allowedComponents.includes(child.type);
        }

        return true;
      })
      .map((elem) => ({
        ...(elem as ReactElement).props,
        componentInfo: elem,
      })) as T[];
  }, [children, allowedComponents]);
}
