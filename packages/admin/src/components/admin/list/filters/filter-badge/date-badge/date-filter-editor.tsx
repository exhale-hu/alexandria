import { FieldLabel, Switch, TextInput } from "@litbase/alexandria";
import { FilterBadgeProps } from "../filter-badge";
import { Label } from "../style-components";
import { FieldType } from "../../../../../../types/field-types";

export function DateFilterEditor({
  value,
  onChange,
}: {
  value: FilterBadgeProps<FieldType.date>;
  onChange: (value: FilterBadgeProps<FieldType.date>) => void;
}) {
  const isRange = !(value.value instanceof Date);
  if (value.isRelative) {
    return (
      <>
        <Label>Start time</Label>
        <DateInput
          value={(value.value as { from: Date }).from}
          onChange={(date) => onChange({ ...value, value: { from: date, to: new Date() } })}
        />
      </>
    );
  }
  return (
    <>
      <Label>Date range</Label>
      <Switch
        value={isRange}
        onChange={(isRange) => {
          onChange({
            ...value,
            value: (isRange
              ? { from: value.value, to: new Date() }
              : (value.value as { from: Date })?.from || new Date()) as any,
          });
        }}
      />
      <FieldLabel>{isRange ? "Start time" : "Selected time"}</FieldLabel>
      <DateInput
        value={value.value instanceof Date ? value.value : value.value?.from}
        onChange={(date) =>
          onChange({
            ...value,
            value: isRange ? { ...value.value, from: date } : date,
          })
        }
      />
      {isRange && (
        <>
          <FieldLabel>End time</FieldLabel>
          <DateInput
            value={(value.value as { to: Date })?.to}
            onChange={(date) => onChange({ ...value, value: { ...value.value, to: date } })}
          />
        </>
      )}
    </>
  );
}

function DateInput({ value, onChange }: { value: Date | undefined; onChange: (date: Date) => void }) {
  return (
    <TextInput type="datetime-local" value={toInputValue(value)} onChange={(e) => onChange(new Date(e.target.value))} />
  );
}

function toInputValue(date?: Date) {
  if (!date) return "";
  return new Date(date).toISOString().slice(0, 16);
}
