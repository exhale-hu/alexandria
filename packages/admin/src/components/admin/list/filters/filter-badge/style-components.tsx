import { FieldLabel, spacing1, spacing2, spacing3 } from "@litbase/alexandria";
import { StyledIconBase } from "@styled-icons/styled-icon";
import styled, { css } from "styled-components";
import { X } from "@styled-icons/boxicons-regular";

const padding = css`
  padding: ${spacing1} ${spacing2};
`;

export const CloseButton = styled(X)`
  margin-right: ${spacing2};
  transition: color 0.3s;
  &:hover {
    color: red;
  }
`;

export const KeyName = styled.span`
  background: whitesmoke;
  font-size: ${({ theme }) => theme.textXs};
  ${padding};
  color: ${({ theme }) => theme.primary800};
  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.textBase};
    margin-right: ${spacing2};
    margin-top: -1px;
  }
`;

export const StyledValue = styled.span`
  ${padding};
  font-size: ${({ theme }) => theme.textXs};
  min-width: ${spacing2};
  display: flex;
  align-items: center;
`;

export const Body = styled.div`
  background: white;
  border-radius: ${({ theme }) => theme.roundedLg};
  font-size: ${({ theme }) => theme.textSm};
  box-shadow: ${({ theme }) => theme.shadow};
  width: fit-content;
  border: 1px solid ${({ theme }) => theme.gray100};
  overflow: hidden;
  cursor: pointer;
  display: flex;
  align-items: center;
`;

export const Label = styled(FieldLabel)`
  font-size: ${({ theme }) => theme.textXs};
`;
