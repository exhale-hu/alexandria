import { FieldConfig, FieldType } from "../../../../../../types/field-types";
import { CloseButton, KeyName, StyledValue } from "../style-components";
import { FilterBadgeConfig, FilterBadgeProps } from "../filter-badge";
import { Checkbox } from "@litbase/alexandria";
import { DateBadge } from "../date-badge";
import { ComponentProps } from "react";

export function FilterBadgeRenderer<T extends FieldType>({
  type,
  displayName: name,
  value,
  onDelete,
  ...props
}: { type: FieldType; onDelete: () => void } & FilterBadgeConfig<T>) {
  switch (type) {
    case FieldType.string:
      return (
        <>
          <KeyName>{name}</KeyName>
          <StyledValue>{value}</StyledValue>
          <CloseButton onClick={onDelete} />
        </>
      );
    case FieldType.bool:
      return (
        <>
          <KeyName>{name}</KeyName>
          <StyledValue>
            <Checkbox type="checkbox" checked={value} />
          </StyledValue>
          <CloseButton onClick={onDelete} />
        </>
      );
    case FieldType.date:
      return (
        <DateBadge
          type={type}
          value={value}
          onDelete={onDelete}
          onChange={(props as any).onChange as ComponentProps<typeof DateBadge>["onChange"]}
          isRelative={(props as any).isRelative}
          {...props}
        />
      );
    case FieldType.doc:
      return (
        <>
          <KeyName>{name}</KeyName>
          <StyledValue>
            {(props.fieldConfig as FieldConfig<FieldType.doc>).joinConfig.getLabel(
              (props as unknown as FilterBadgeConfig<FieldType.doc>).doc
            )}
          </StyledValue>
          <CloseButton onClick={onDelete} />
        </>
      );
  }
  return null;
}
