import { FilterBadge } from "./filter-badge";
import { ComponentStory } from "@storybook/react";
import { useState } from "react";
import { FieldType } from "../../../../../types/field-types";

export default {
  title: "Admin/ListPage/Filters/FilterBadge",
  component: FilterBadge,
};

const Template: ComponentStory<typeof FilterBadge> = (args) => {
  const [state, setState] = useState(args);
  return <FilterBadge {...state} onChange={setState} />;
};

export const Text = Template.bind({});
Text.args = {
  type: FieldType.string,
  name: "username",
  value: "Arthur944",
};

export const Boolean = Template.bind({});
Boolean.args = {
  type: FieldType.bool,
  name: "isAdmin",
  value: true,
};
