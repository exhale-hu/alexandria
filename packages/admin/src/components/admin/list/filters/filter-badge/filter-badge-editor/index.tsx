import { FieldType } from "../../../../../../types/field-types";
import { FilterBadgeProps } from "../filter-badge";
import { DateFilterEditor } from "../date-badge";
import { Label } from "../style-components";
import { Switch } from "@litbase/alexandria";
import { DocBadgeEditor } from "../doc-badge/doc-badge-editor";
import { TextFilterBadgeEditor } from "./text-filter-badge-editor";

export function FilterBadgeEditor<T extends FieldType>({
  value,
  onChange,
  type,
}: {
  value: FilterBadgeProps<T>;
  onChange: (value: FilterBadgeProps<T>) => void;
  type: T;
}) {
  switch (type) {
    case FieldType.string:
      return (
        <TextFilterBadgeEditor
          value={value as unknown as FilterBadgeProps<FieldType.string>}
          onChange={onChange as unknown as (value: FilterBadgeProps<FieldType.string>) => void}
        />
      );
    case FieldType.date:
      return (
        <DateFilterEditor
          value={value as unknown as FilterBadgeProps<FieldType.date>}
          onChange={onChange as unknown as (value: FilterBadgeProps<FieldType.date>) => void}
        />
      );
    case FieldType.bool:
      const castValue = value as unknown as FilterBadgeProps<FieldType.bool>;
      return (
        <>
          <Label>Value</Label>
          <Switch
            value={castValue.value}
            onChange={(value) =>
              (onChange as unknown as (value: FilterBadgeProps<FieldType.bool>) => void)({ ...castValue, value })
            }
          />
        </>
      );
    case FieldType.doc:
      return (
        <DocBadgeEditor
          value={value as unknown as FilterBadgeProps<FieldType.doc>}
          onChange={onChange as unknown as (value: FilterBadgeProps<FieldType.doc>) => void}
        />
      );
    default:
      return null;
  }
}
