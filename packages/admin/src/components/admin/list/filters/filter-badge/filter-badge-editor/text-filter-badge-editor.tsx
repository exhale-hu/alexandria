import { FieldType } from "../../../../../../types/field-types";
import { Label } from "../style-components";
import { TextInput } from "@litbase/alexandria";
import { FilterBadgeProps } from "../filter-badge";

export function TextFilterBadgeEditor({
  value,
  onChange,
}: {
  value: FilterBadgeProps<FieldType.string>;
  onChange: (value: FilterBadgeProps<FieldType.string>) => void;
}) {
  return (
    <>
      <Label>Query</Label>
      <TextInput
        autoFocus
        value={value.value}
        onChange={(e) => {
          onChange({
            ...value,
            value: e.target.value,
          });
        }}
      />
    </>
  );
}
