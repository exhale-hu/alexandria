import { AddButton, ListPageHeader } from "./header";
import { Filter, Filters } from "./filters";
import { Column, Table } from "./columns";
import { Cell } from "@litbase/alexandria";
import { DeleteButton, EditButton, PropertyButton } from "./buttons";
import { ListPage } from "./list-page";
import { User } from "@styled-icons/boxicons-regular";

export function ExampleListPage() {
  return (
    <>
      <ListPageHeader>
        <h1>Felhasználók</h1>
        <AddButton />
      </ListPageHeader>

      <Filters>
        <Filter property="birthday" />
        <Filter property="username" />
        <Filter property="isAdmin" />
        <Filter property="parent" />
      </Filters>

      <Table>
        <Column property="profilePic" />
        <Column property="username" sortable />
        <Column property="birthday" sortable />
        <Column property="isAdmin" sortable />
        <Column property="parent" />
        <Column property="parentDoc" />
        <Column displayName="Flamboyant (to show off custom columns)">
          {(item) => (
            <Cell>
              <b>
                <i>Flamboyant {item.username}</i>
              </b>
            </Cell>
          )}
        </Column>
        <Column>
          {() => (
            <Cell>
              <EditButton />
              <DeleteButton />
              <PropertyButton property="isAdmin" icon={<User />} />
            </Cell>
          )}
        </Column>
      </Table>

      <AddButton />
    </>
  );
}
