import { ReactNode, useContext } from "react";
import { Button, spacing4 } from "@litbase/alexandria";
import styled from "styled-components";
import { Plus } from "@styled-icons/boxicons-regular";
import { createEditUrl } from "../buttons";
import { AdminContext } from "../../admin";
import { useResource } from "../../resource";
import { useDataAccess } from "../../../../contexts";
import { useQueryBuilder } from "../list-page";

export function ListPageHeader({ children }: { children: ReactNode }) {
  return <HeaderDiv>{children}</HeaderDiv>;
}

export function AddButton() {
  const listPageContext = useQueryBuilder();
  const adminContext = useContext(AdminContext);
  const model = useResource();
  const dataAccess = useDataAccess();
  const url = createEditUrl(null, adminContext, model, dataAccess);
  return (
    <ButtonBody $size="sm" to={url}>
      <Plus />
      {listPageContext?.newLabel || model?.newLabel || "Add new"}
    </ButtonBody>
  );
}

const ButtonBody = styled(Button)`
  width: fit-content;
  margin-left: 0;
`;

const HeaderDiv = styled.div`
  display: flex;
  align-items: center;
  > * + * {
    margin-left: ${spacing4};
  }
  > h1 {
    margin-bottom: 0;
    margin-left: 0;
  }
`;
