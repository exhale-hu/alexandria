import { StyledIcon } from "@styled-icons/styled-icon";
import { FieldConfig, FieldType } from "../../types/field-types";
import { BatchFetcher, SingleFetcher } from "../../contexts/data-provider/types";

export interface ModelConfig<ItemType = Record<string, unknown>, IdType = unknown> {
  name: string;
  displayName?: string;
  singleName?: string;
  icon?: StyledIcon;
  fields: Record<keyof ItemType, FieldConfig>;

  batchFetcher?: BatchFetcher<ItemType>;
  singleFetcher?: SingleFetcher<IdType, ItemType>;

  newLabel?: string;
  listLabel?: string;
}

export type FieldTypeValue = {
  [FieldType.string]: string;
  [FieldType.number]: number;
  [FieldType.date]: Date;
  [FieldType.bool]: boolean;
  [FieldType.any]: any;
  [FieldType.doc]: Record<string, unknown>;
  [FieldType.image]: string;
  [FieldType.file]: string;
  [FieldType.images]: string[];
  [FieldType.files]: string[];
  [FieldType.text]: string;
  [FieldType.object]: Record<string, unknown>;
};

export function getDefaultFieldValue(config: FieldConfig) {
  if (config.defaultValue) return config.defaultValue;
  switch (config.type) {
    case FieldType.date:
      return new Date();
    case FieldType.number:
      return 0;
    case FieldType.bool:
      return false;
    case FieldType.string:
      return "";
    default:
      return null;
  }
}
