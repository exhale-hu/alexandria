import { useChildrenProps } from "./list";
import { ComponentProps, createContext, ReactNode, useMemo, useState } from "react";
import { Resource } from "./resource";
import { spacing } from "@litbase/alexandria";
import styled from "styled-components";
import { Sidebar } from "./sidebar/sidebar";

export const AdminContext = createContext<{
  prefix: string;
  registerModel: (config: ComponentProps<typeof Resource>) => void;
  saveLabel?: string;
} | null>(null);

export function Admin({
  children,
  prefix = "/admin",
  saveLabel = "Save",
}: {
  children: ReactNode;
  prefix?: string;
  saveLabel?: string;
}) {
  const [models, setModels] = useState<ComponentProps<typeof Resource>[]>([]);
  const sidebarProps = useChildrenProps(children, [Sidebar]);

  const adminContextValue = useMemo(() => {
    const registerModel = (config: ComponentProps<typeof Resource>) => {
      setModels((prev) => [...prev.filter((elem) => elem?.config?.name !== config.config.name), config]);
    };
    return { prefix, registerModel, saveLabel };
  }, [prefix, saveLabel]);

  return (
    <AdminContext.Provider value={adminContextValue}>
      <Row>
        <Sidebar {...sidebarProps[0]} models={models.map((elem) => elem.config)} />
        <Col>{children}</Col>
      </Row>
    </AdminContext.Provider>
  );
}

const Col = styled.div`
  padding: ${spacing._12};
  overflow-y: auto;
  flex: 1;
`;

const Row = styled.div`
  display: flex;
  max-height: 100vh;
`;
