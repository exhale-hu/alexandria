import { ReactNode } from "react";
import { ModelConfig } from "../types";
import { Sidebar as AlexandriaSidebar, SidebarItem } from "@litbase/alexandria";

export function Sidebar({ models, ...props }: { models?: ModelConfig[]; header?: ReactNode; profile?: ReactNode }) {
  if (!models) {
    return null;
  }
  return (
    // @ts-ignore
    <AlexandriaSidebar {...props}>
      <SidebarItems models={models} />
    </AlexandriaSidebar>
  );
}

export function SidebarItems({ models }: { models: ModelConfig[] }) {
  return (
    <>
      {models.map((model, index) => (
        <SidebarItem key={index} name={model.displayName || model.name} icon={model.icon}>
          <SidebarItem name={model.listLabel || "List"} to={model.name} />
          <SidebarItem name={model.newLabel || "New"} to={model.name + "/new"} />
        </SidebarItem>
      ))}
    </>
  );
}
