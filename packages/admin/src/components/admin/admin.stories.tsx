import { Admin } from "./admin";
import { ComponentStory } from "@storybook/react";
import { Resource } from "./resource";
import { ListPage, usersConfig } from "./list";
import { ExampleListPage } from "./list/example-list-page";
import { createGlobalStyle } from "styled-components";
import { EditPage } from "./edit/edit-page";

export default {
  title: "Admin",
  component: Admin,
};

const Template: ComponentStory<typeof Admin> = () => {
  return (
    <Admin prefix="">
      <GlobalStyle />
      <Resource config={usersConfig}>
        <ListPage>
          <ExampleListPage />
        </ListPage>
        <EditPage />
      </Resource>
    </Admin>
  );
};

const GlobalStyle = createGlobalStyle`
  body {
    padding: 0 !important;
  }
`;

export const Default = Template.bind({});
