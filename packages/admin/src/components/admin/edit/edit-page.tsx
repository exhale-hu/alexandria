import { ComponentProps, createContext, ReactNode, useContext, useEffect, useMemo, useState } from "react";
import { Button, HookForm, LoadingIcon, showSuccessToast, spacing } from "@litbase/alexandria";
import { ResourceContext, useResource } from "../resource";
import { useObservable } from "@litbase/use-observable";
import { useParams } from "react-router-dom";
import { switchMap } from "rxjs";
import { getDefaultFieldValue, ModelConfig } from "../types";
import { Field } from "./field";
import { Save } from "@styled-icons/boxicons-regular";
import styled from "styled-components";
import { useChildrenProps } from "../list";
import { useDataAccess } from "../../../contexts/data-provider/data-provider";
import { useFormContext } from "react-hook-form";
import { animated, useSpring } from "react-spring";
import { AdminContext } from "../admin";
import { noop } from "lodash-es";

interface EditPageContextValue {
  register: (props: ComponentProps<typeof Field>) => void;
}

export const EditPageContext = createContext<EditPageContextValue | null>(null);

export interface EditPageConfig<ItemType = unknown> {
  children?: ReactNode;
  transformValuesBeforeSubmit?: (values: ItemType) => unknown | Promise<unknown>;
  transformValuesBeforeFirstRender?: (values: ItemType | null) => Record<string, unknown>;
  hideSaveButton?: string;
  successMessage?: { title: string; text: string };
  afterSubmit?: (item: ItemType) => Promise<void> | void;
}

export function EditPage(props: EditPageConfig) {
  const resourceContext = useContext(ResourceContext);
  useEffect(() => {
    if (resourceContext?.registerEditPage) {
      resourceContext.registerEditPage(props);
    }
  }, [resourceContext, props]);
  return null;
}

export function InternalEditPage<ItemType, IdType>({
  children,
  hideSaveButton,
  transformValuesBeforeSubmit = (val) => val,
  transformValuesBeforeFirstRender = (val) => val as Record<string, unknown>,
  successMessage = { title: "Success", text: "Saved successfully" },
  afterSubmit = noop,
}: EditPageConfig) {
  const headerProps = useChildrenProps<ComponentProps<typeof EditPageHeader>>(children, [EditPageHeader]);
  const { id } = useParams();
  if (!id) throw new Error("Tried to use EditPage with improper route structure (didn't receive the :id parameter)");

  const { fetchSingle, stringToId, persist } = useDataAccess<ItemType, IdType>();
  const model = useResource();

  const [item, loading] = useObservable<ItemType | null, [string, string]>(
    (inputs$) => inputs$.pipe(switchMap(([collection, id]) => fetchSingle(collection, stringToId(id)))),
    null,
    [model.name, id]
  );

  const [fieldProps, setFieldProps] = useState<ComponentProps<typeof Field>[]>([]);
  const contextValue = useMemo(() => {
    return {
      register: (fieldProps: ComponentProps<typeof Field>) =>
        setFieldProps((prev) => [...prev.filter((elem) => elem.property !== fieldProps.property), fieldProps]),
    };
  }, []);

  if (loading) {
    return <LoadingIcon />;
  }

  return (
    <EditPageContext.Provider value={contextValue}>
      <HookForm
        shouldUnregister={false}
        defaultValues={transformValuesBeforeFirstRender(item || createDefaultValues(model))}
        onSubmit={async (values) => {
          const transformedValues = await transformValuesBeforeSubmit(values);
          await persist(model.name, transformedValues);
          await afterSubmit(values);
          showSuccessToast(<>{successMessage.title}</>, <>{successMessage.text}</>);
        }}
      >
        <EditPageHeader __forceRender={true} {...(headerProps[0] || {})} item={item} />
        {Object.keys(model.fields)
          .filter((key) => !fieldProps.some((child) => child.property === key))
          .map((key) => (
            <Field doRegister={false} property={key} />
          ))}
        {children}
        {!hideSaveButton && <SaveButton />}
      </HookForm>
    </EditPageContext.Provider>
  );
}

export function EditPageHeader<ItemType>({
  children,
  item,
  __forceRender,
}: {
  children?: ReactNode | ((item: ItemType | undefined) => ReactNode);
  item?: ItemType;
  __forceRender?: boolean;
}) {
  const model = useResource();
  if (!__forceRender) return null;
  if (children) {
    return <>{typeof children === "function" ? children(item) : children}</>;
  }
  return (
    <h1>
      {item ? "Edit " : "Create new "} {model.singleName || model.displayName || model.name}
    </h1>
  );
}

export function SaveButton({ children, className }: { children?: ReactNode; className?: string }) {
  const adminContext = useContext(AdminContext);
  const state = useFormContext();
  const styles = useSpring({
    width: state.formState.isSubmitting ? "1rem" : "0rem",
    marginRight: state.formState.isSubmitting ? "0.5rem" : "0rem",
  });
  return (
    <StyledSubmitButton disabled={state.formState.isSubmitting} type="submit" className={className}>
      <LoaderContainer style={styles}>
        <LoadingIcon />
      </LoaderContainer>
      <Save />
      {children || adminContext?.saveLabel || "Save"}
    </StyledSubmitButton>
  );
}

const LoaderContainer = styled(animated.div)`
  margin-right: ${spacing._2};
  > svg {
    max-width: 100%;
  }
`;

const StyledSubmitButton = styled(Button).attrs(() => ({ $kind: "primary" }))`
  margin-top: ${spacing._4};
`;

function createDefaultValues(model: ModelConfig) {
  return Object.fromEntries(
    Object.entries(model.fields)
      .filter(([, config]) => !config.virtual)
      .map(([key, config]) => [key, getDefaultFieldValue(config)])
  );
}
