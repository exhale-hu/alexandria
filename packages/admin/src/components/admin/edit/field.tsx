import { useResource } from "../resource";
import {
  FileEntry,
  HookCheckboxField,
  HookDocumentSelectFieldBase,
  HookFileUploadField,
  HookInputField,
  HookSingleFileUploadField,
  HookTextareaField,
} from "@litbase/alexandria";
import { createContext, ReactNode, useContext, useEffect } from "react";
import { Observable, of } from "rxjs";
import { FieldConfig, FieldType } from "../../../types/field-types";
import { useDataAccess } from "../../../contexts/data-provider/data-provider";
import { EditPageContext } from "./edit-page";

export const FileContext = createContext<{ uploadFile: (file: FileEntry) => Observable<FileEntry> }>({
  uploadFile: (file: FileEntry) => {
    const url = URL.createObjectURL(file.file as File);
    return of({ ...file, url: url, iri: url, contentUrl: url, status: "done", progress: 100 });
  },
});

export function Field<ItemType extends Record<string, unknown>, IdType>({
  property,
  children,
  hidden = false,
  limit = 25,
  doRegister = true,
}: {
  property: string;
  children?: ReactNode;
  hidden?: boolean;
  limit?: number;
  doRegister?: boolean;
}) {
  const { uploadFile } = useContext(FileContext);
  const editPageContext = useContext(EditPageContext);
  const model = useResource();
  const dataAccess = useDataAccess<ItemType, IdType>(true);
  const fieldConfig = model.fields[property];
  const commonProps = { label: fieldConfig.displayName || property, placeholder: fieldConfig.placeholder };

  useEffect(() => {
    if (doRegister) {
      editPageContext?.register?.({ property, children, hidden, limit });
    }
  }, [property, children, hidden, limit, doRegister]);

  if (hidden) {
    return null;
  }
  if (children) {
    return <>{children}</>;
  }
  switch (fieldConfig.type) {
    case FieldType.string:
      return <HookInputField {...commonProps} name={property} />;
    case FieldType.number:
      return <HookInputField {...commonProps} name={property} type="number" />;
    case FieldType.doc:
      if (!dataAccess) throw new Error("Tried to use 'doc' fieldType without a DataAccessProvider");
      const joinConfig = (fieldConfig as FieldConfig<FieldType.doc>).joinConfig;
      return (
        <HookDocumentSelectFieldBase<string, ItemType>
          {...commonProps}
          // @ts-ignore
          getId={(item) => dataAccess.getId(item)}
          getOptionLabel={joinConfig.getLabel}
          fetcher={async (query) =>
            (await dataAccess.fetchBatch({
              collection: joinConfig.otherCollection || model.name,
              orderings: {},
              filters: [
                {
                  or:
                    joinConfig.searchableFields.map((fieldName) => ({
                      name: fieldName,
                      value: query,
                      type: FieldType.string,
                      fieldConfig: { type: FieldType.string },
                    })) || [],
                },
              ],
              limit: limit,
            })) || []
          }
          name={property}
        />
      );
    case FieldType.date:
      return <HookInputField {...commonProps} name={property} type="datetime-local" />;
    case FieldType.bool:
      return <HookCheckboxField checkboxLabel={fieldConfig.displayName || property} name={property} />;
    case FieldType.file:
    case FieldType.image:
      return <HookSingleFileUploadField {...commonProps} onUploadFile={uploadFile} name={property} />;
    case FieldType.images:
    case FieldType.files:
      return <HookFileUploadField {...commonProps} onUploadFile={uploadFile} name={property} />;
    case FieldType.text:
      return <HookTextareaField {...commonProps} name={property} />;
    case FieldType.object:
    default:
      return null;
  }
}
