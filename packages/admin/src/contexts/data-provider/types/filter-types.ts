import { FieldTypeValue, FilterBadgeConfig } from "../../../components";
import { FieldConfig, FieldType } from "../../../types/field-types";

export type FiltersDescription = (FilterPrimitive | { or: FiltersDescription })[];

export type FilterPrimitive<T extends FieldType = FieldType> = FilterPrimitiveBase & FilterPrimitiveExtension[T];

type FilterPrimitiveBase<T extends FieldType = FieldType> = {
  name: string;
  value: FilterValue<T>;
  fieldConfig: FieldConfig<T>;
};

export type FilterValue<T extends FieldType = FieldType> = T extends FieldType.date
  ? { from?: Date; to?: Date } | Date
  : FieldTypeValue[T];

export type OrderingConfig = Record<string, 1 | -1>;

export type FilterPrimitiveExtension = {
  [FieldType.doc]: { doc: Record<string, unknown> };
  [FieldType.string]: {};
  [FieldType.number]: {};
  [FieldType.date]: { isRelative: boolean };
  [FieldType.bool]: {};
  [FieldType.any]: {};
  [FieldType.image]: {};
  [FieldType.images]: {};
  [FieldType.file]: {};
  [FieldType.files]: {};
  [FieldType.object]: {};
  [FieldType.text]: {};
};
