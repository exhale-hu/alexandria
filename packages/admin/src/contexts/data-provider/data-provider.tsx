import { Context, createContext, ReactNode, useContext } from "react";
import { BatchFetcher, SingleFetcher } from "./types";

export const DataAccessContext = createContext<DataAccessContextValue | null>(null);

export interface DataAccessContextValue<ItemType = Record<string, unknown>, IdType = unknown> {
  fetchBatch: BatchFetcher<ItemType>;
  fetchSingle: SingleFetcher<IdType, ItemType>;
  persist: (collection: string, item: ItemType | unknown) => Promise<void>;
  delete: (collection: string, id: IdType) => Promise<void>;
  getId: (item: ItemType) => IdType;
  idToString: (id: IdType) => string;
  stringToId: (string: string) => IdType;
}

export function useDataAccess<ItemType, IdType>(allowNonExistentProvider = false) {
  const context = useContext<DataAccessContextValue<ItemType, IdType>>(
    DataAccessContext as unknown as Context<DataAccessContextValue<ItemType, IdType>>
  );
  if (!context && !allowNonExistentProvider)
    throw new Error("Tried to use Data Access context without a DataAccessProvider");
  return context;
}

type optionalFields = "idToString" | "stringToId";

export function DataAccessProvider<ItemType, IdType>({
  children,
  idToString = (val) => val as unknown as string,
  stringToId = (val: string) => val as unknown as IdType,
  ...props
}: { children: ReactNode } & Omit<DataAccessContextValue<ItemType, IdType>, optionalFields> & {
    [x in optionalFields]?: DataAccessContextValue<ItemType, IdType>[x];
  }) {
  const Provider = DataAccessContext.Provider as unknown as Context<
    DataAccessContextValue<ItemType, IdType>
  >["Provider"];
  return <Provider value={{ ...props, idToString, stringToId }}>{children}</Provider>;
}
