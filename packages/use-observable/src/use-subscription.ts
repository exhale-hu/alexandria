import { Subscription } from "rxjs";
import { useAction } from "use-action";

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = () => {};

export function useSubscription(factory: () => Subscription | null | false | undefined, deps: readonly unknown[] = []) {
  useAction(() => {
    const subscription = factory();

    return subscription ? () => subscription.unsubscribe() : noop;
  }, deps);
}
