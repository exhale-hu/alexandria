import { Observable, ObservableInput, Subject } from "rxjs";
import { useConstant } from "./use-constant.js";
import { useObservable } from "./use-observable.js";

type UseSubjectState<TEvent, TValue> = [Subject<TEvent>, TValue, boolean, Error | null, boolean];

export function useSubject<TEvent, TValue>(
  inputFactory: (event$: Observable<TEvent>) => ObservableInput<TValue>
): UseSubjectState<TEvent, TValue | null>;
export function useSubject<TEvent, TValue>(
  inputFactory: (event$: Observable<TEvent>) => ObservableInput<TValue>,
  initialValue: TValue
): UseSubjectState<TEvent, TValue>;
export function useSubject<TEvent, TValue, TInputs extends ReadonlyArray<unknown>>(
  inputFactory: (event$: Observable<TEvent>, inputs$: Observable<TInputs>) => ObservableInput<TValue>,
  initialValue: TValue,
  inputs: TInputs
): UseSubjectState<TEvent, TValue>;
export function useSubject<TEvent, TValue, TInputs extends ReadonlyArray<unknown>>(
  inputFactory: (event$: Observable<TEvent>, inputs$: Observable<TInputs>) => ObservableInput<TValue>,
  initialValue: TValue = null as unknown as TValue,
  inputs: TInputs = [] as never
): UseSubjectState<TEvent, TValue> {
  const subject$ = useConstant(() => new Subject<TEvent>());
  const [value, isLoading, error, isComplete] = useObservable<TValue, TInputs>(
    ($inputs) => inputFactory(subject$, $inputs),
    initialValue,
    inputs
  );

  return [subject$, value, isLoading, error, isComplete];
}
