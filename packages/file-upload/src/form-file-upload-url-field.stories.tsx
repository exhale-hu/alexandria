import { FormFileUploadUrlField, FormSingleFileUploadUrlField } from "./form-file-upload-url-field";
import { Form, useField } from "@litbase/alexandria";
import { toTransitionStatus } from "./hooks/use-on-upload-file";
import { FileEntry } from "./file-entry";
import { interval, of, takeWhile } from "rxjs";
import { map } from "rxjs/operators";

export default {
  title: "FormFileUploadUrlField",
  component: FormFileUploadUrlField,
};

export function Template() {
  return (
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    <Form defaultValues={{ file: null }} onSubmit={() => {}}>
      <FormSingleFileUploadUrlField
        name="file"
        label="An input"
        onUploadFile={(entry) => {
          return interval(1500).pipe(
            map(
              (index) =>
                ({
                  uid: entry.uid,
                  file: entry.file,
                  status: index === 10 ? "done" : "working",
                  error: undefined,
                  progress: index * 10,
                  contentUrl: "/uploads/" + entry.file?.name,
                } as FileEntry)
            ),
            takeWhile((entry) => (entry.progress || 0) <= 100)
          );
        }}
      />
      <Dummy />
    </Form>
  );
}

function Dummy() {
  const { value } = useField("file");

  return <pre>{JSON.stringify(value, undefined, 2)}</pre>;
}
