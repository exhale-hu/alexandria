import { ReactNode, useRef } from "react";
import { HookFieldWrapper, useControllerEx } from "@litbase/alexandria";
import { FileUploadProps } from "./types";
import { FileEntry } from "./file-entry";
import { FileUpload } from "./file-upload";
import { identity } from "lodash-es";
import { useOnUploadFile } from "./hooks/use-on-upload-file";
import { getFileEntryFromName } from "./utils/get-file-entry-from-name";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface HookFileUploadUrlFieldProps<TValue = any> extends Omit<FileUploadProps, "value" | "onChange"> {
  name: string;
  label?: ReactNode;
  parse?: (value: string[]) => TValue;
  transform?: (value: TValue) => string[];
}

export function HookSingleFileUploadUrlField(props: HookFileUploadUrlFieldProps) {
  return (
    <HookFileUploadUrlField<string | null>
      transform={(value) => (value ? [value] : [])}
      parse={(value) => value[0] || null}
      limit={1}
      {...props}
    />
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function HookFileUploadUrlField<TValue = any>({
  name,
  label,
  parse: outerParse = identity,
  transform: outerTransform = identity,
  ...props
}: HookFileUploadUrlFieldProps<TValue>) {
  const fileEntryMapRef = useRef<FileEntry[]>([]);

  const {
    field: { value, onChange },
  } = useControllerEx<TValue, FileEntry[]>({
    name,
    transform: (newValue) =>
      outerTransform(newValue)?.map((v) => getFileEntryFromName(v, fileEntryMapRef.current)) || [],
    parse: (value) => outerParse(value.map((v: FileEntry) => v.contentUrl || "")),
  });

  const onUploadFile = useOnUploadFile();

  return (
    <HookFieldWrapper name={name} label={label}>
      <FileUpload value={value} onChange={onChange} onUploadFile={onUploadFile} {...props} />
    </HookFieldWrapper>
  );
}
