import { useEffect, useState } from "react";
import { FileEntry } from "../file-entry";

export function useFilePreviewUrl(file: Omit<FileEntry, "uid" | "status">) {
  const [url, setUrl] = useState("");

  useEffect(() => {
    if (file.contentUrl) {
      setUrl(file.contentUrl);
    } else if (file.file) {
      setUrl(URL.createObjectURL(file.file));

      return () => URL.revokeObjectURL(url);
    }
  }, [file.file, file.contentUrl]);

  return url;
}
