import { FileEntry } from "./file-entry";
import { FileBlank, FilePdf } from "@styled-icons/boxicons-solid";
import { StyledIcon, StyledIconBase } from "@styled-icons/styled-icon";
import styled from "styled-components";
import { useFilePreviewUrl } from "./hooks/use-file-preview-url";
import { getExtension } from "./utils/get-extension";

export interface FileUploadItemPreviewProps {
  file: FileEntry;
}

export function FileUploadItemPreview({ file }: FileUploadItemPreviewProps) {
  if (file.thumbnailUrl) {
    return <StyledImageUploadItemPreview style={{ backgroundImage: `url("${file.thumbnailUrl}")` }} />;
  }

  let extension = "";

  if (file.contentUrl) {
    extension = getExtension(file.contentUrl);
  }

  switch (extension) {
    case ".jpg":
    case ".jpeg":
    case ".gif":
    case ".png":
    case ".webp":
    case ".bmp":
      return file.status === "done" ? <ImageUploadItemPreview file={file} /> : null;
    case ".pdf":
      return <IconUploadItemPreview icon={FilePdf} />;
    default:
      return <IconUploadItemPreview icon={FileBlank} />;
  }
}

export const StyledFileUploadItemPreview = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: ${({ theme }) => theme.roundedMd};
`;

export function ImageUploadItemPreview({ file }: FileUploadItemPreviewProps) {
  const previewUrl = useFilePreviewUrl(file);
  let extension = "";

  if (file.file) {
    extension = getExtension(file.file.name);
  } else {
    extension = getExtension(previewUrl);
  }

  return (
    <StyledImageUploadItemPreview style={{ backgroundImage: `url("${previewUrl}")` }} $isSvg={extension === ".svg"} />
  );
}

const StyledImageUploadItemPreview = styled(StyledFileUploadItemPreview)<{ $isSvg?: boolean }>`
  background-size: ${({ $isSvg }) => ($isSvg ? "contain" : "cover")};
  background-position: center;
  background-repeat: ${({ $isSvg }) => ($isSvg ? "no-repeat" : "initial")};
`;

export function IconUploadItemPreview({ icon: Icon }: { icon: StyledIcon }) {
  return (
    <StyledIconUploadItemPreview>
      <Icon />
    </StyledIconUploadItemPreview>
  );
}

const StyledIconUploadItemPreview = styled(StyledFileUploadItemPreview)`
  display: flex;
  align-items: center;
  justify-content: center;

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text5xl};
  }
`;
