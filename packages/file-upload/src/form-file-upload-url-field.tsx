import { useRef } from "react";
import { FieldProps, FormFieldWrapper, useField } from "@litbase/alexandria";
import { UploadFileOptions } from "@litbase/client";
import { FileUploadProps, OnUploadFileCallback } from "./types";
import { FileEntry } from "./file-entry";
import { FileUpload } from "./file-upload";
import { identity } from "lodash-es";
import { useOnUploadFile } from "./hooks/use-on-upload-file";
import { getFileEntryFromName } from "./utils/get-file-entry-from-name";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface FormFileUploadUrlFieldProps<TValue = any>
  extends FieldProps<TValue, string[]>,
    Omit<FileUploadProps, "value" | "onChange"> {
  name: string;
  uploadOptions?: UploadFileOptions;
  onUploadFile?: OnUploadFileCallback;
}

export function FormSingleFileUploadUrlField(props: FormFileUploadUrlFieldProps) {
  return (
    <FormFileUploadUrlField<string | null>
      onValueRead={(value) => (value ? [value] : [])}
      onValueWrite={(value) => value[0] || null}
      limit={1}
      {...props}
    />
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function FormFileUploadUrlField<TValue = any>({
  name,
  label,
  onValueRead: outerOnValueRead = identity,
  onValueWrite: outerOnValueWrite = identity,
  uploadOptions,
  ...props
}: FormFileUploadUrlFieldProps<TValue>) {
  const fileEntryMapRef = useRef<FileEntry[]>([]);

  const { value, setValue } = useField<FileEntry[], TValue>(name, {
    onValueRead: (newValue) =>
      outerOnValueRead(newValue)?.map((v) => getFileEntryFromName(v, fileEntryMapRef.current)) || [],
    onValueWrite: (value) => {
      fileEntryMapRef.current = value;

      return outerOnValueWrite(value.map((v: FileEntry) => v.contentUrl || v.uid || ""));
    },
    ...props,
  });

  const onUploadFile = useOnUploadFile(uploadOptions);

  return (
    <FormFieldWrapper name={name} label={label}>
      <FileUpload value={value} onChange={setValue} onUploadFile={onUploadFile} {...props} />
    </FormFieldWrapper>
  );
}
