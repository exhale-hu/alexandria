/* eslint-env node */
module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "react"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    // "plugin:react-hooks/recommended",
    "prettier",
  ],
  rules: {
    // ## Intentional choices ##
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    // We can't use this because of our coding style of putting styled-components last in the file
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    // This gives false-positives
    "react/display-name": "off",
    "no-case-declarations": "off",
    "react/prop-types": "off",
    "@typescript-eslint/no-unused-vars": ["warn", { ignoreRestSiblings: true }],
    "react/no-unescaped-entities": [
      "error",
      {
        // Remove ' from forbidden characters
        forbid: ['"', ">", "}"],
      },
    ],
    "react/jsx-curly-brace-presence": "warn",
    "no-console": ["warn", { allow: ["warn", "error", "info", "debug", "time", "timeLog", "timeEnd"] }],
    "react/jsx-no-target-blank": "off",

    // These are no longer needed since React v17 and produce false errors
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
  },
  settings: {
    react: {
      version: "detect",
    },
  },
};
