/* eslint-disable */
module.exports = {
  name: "deduplicate-deps",
  factory: (require) => ({
    hooks: {
      async afterAllInstalled(project, options) {
        const fs = require("fs/promises");
        const { ppath, npath, Filename } = require("@yarnpkg/fslib");

        const pnpFilePath = npath.fromPortablePath(ppath.resolve(project.cwd, Filename.pnpCjs || Filename.pnpJs));

        const pnpFileContent = await fs.readFile(pnpFilePath, "utf8");

        const newPnpFileContent = pnpFileContent.replace(
          /("packageRegistryData": )(.+?)(}, {basePath: basePath \|\| __dirname}\))/s,
          (match, p1, p2, p3) => {
            const packageRegistryData = eval(p2);
            let dedupeCounter = 0;

            // Avoid recreating this map in a tight loop, instead create it at the beginning, then clear it as needed
            const virtualPackageMap = new Map();

            for (const [packageName, items] of packageRegistryData) {
              // if (2 <= items.length && items.some(item => item[0].startsWith("virtual:"))) {
              //   console.log("packageName", packageName, "items", items);
              // }

              if (items.length < 2) continue;

              for (const [itemName, itemDescriptor] of items) {
                if (!itemName.startsWith("virtual:")) continue;

                const [hash, version] = itemName.split("#");

                const packageLocation = virtualPackageMap.get(version);

                if (packageLocation) {
                  itemDescriptor.packageLocation = packageLocation;
                  dedupeCounter++;
                } else {
                  virtualPackageMap.set(version, itemDescriptor.packageLocation);
                }
              }

              virtualPackageMap.clear();
            }

            console.info(`Deduplicated ${dedupeCounter} virtual packages!`);

            return `${p1}${generate(packageRegistryData, PrettyJsonState.PACKAGE_REGISTRY_DATA, `    `).slice(4)}${p3}`;
          }
        );

        let writeOptions;

        if (process.platform === "linux") {
          // Keep existing file mode
          const stats = await fs.stat(pnpFilePath);

          writeOptions = {
            encoding: "utf8",
            mode: stats.mode
          };

        } else {
          writeOptions = {
            encoding: "utf8"
          };
        }

        await fs.writeFile(pnpFilePath, newPnpFileContent, writeOptions);
      },
    },
  }),
};

// Transpiled from: https://github.com/yarnpkg/berry/blob/5f0439a087b8463d97fb70c96dc6f983a91be512/packages/yarnpkg-pnp/sources/generatePrettyJson.ts

var PrettyJsonState;
(function (PrettyJsonState) {
  PrettyJsonState["DEFAULT"] = "DEFAULT";
  PrettyJsonState["TOP_LEVEL"] = "TOP_LEVEL";
  PrettyJsonState["FALLBACK_EXCLUSION_LIST"] = "FALLBACK_EXCLUSION_LIST";
  PrettyJsonState["FALLBACK_EXCLUSION_ENTRIES"] = "FALLBACK_EXCLUSION_ENTRIES";
  PrettyJsonState["FALLBACK_EXCLUSION_DATA"] = "FALLBACK_EXCLUSION_DATA";
  PrettyJsonState["PACKAGE_REGISTRY_DATA"] = "PACKAGE_REGISTRY_DATA";
  PrettyJsonState["PACKAGE_REGISTRY_ENTRIES"] = "PACKAGE_REGISTRY_ENTRIES";
  PrettyJsonState["PACKAGE_STORE_DATA"] = "PACKAGE_STORE_DATA";
  PrettyJsonState["PACKAGE_STORE_ENTRIES"] = "PACKAGE_STORE_ENTRIES";
  PrettyJsonState["PACKAGE_INFORMATION_DATA"] = "PACKAGE_INFORMATION_DATA";
  PrettyJsonState["PACKAGE_DEPENDENCIES"] = "PACKAGE_DEPENDENCIES";
  PrettyJsonState["PACKAGE_DEPENDENCY"] = "PACKAGE_DEPENDENCY";
})(PrettyJsonState || (PrettyJsonState = {}));
const prettyJsonMachine = {
  [PrettyJsonState.DEFAULT]: {
    collapsed: false,
    next: {
      [`*`]: PrettyJsonState.DEFAULT,
    },
  },
  // {
  //   "fallbackExclusionList": ...
  // }
  [PrettyJsonState.TOP_LEVEL]: {
    collapsed: false,
    next: {
      [`fallbackExclusionList`]: PrettyJsonState.FALLBACK_EXCLUSION_LIST,
      [`packageRegistryData`]: PrettyJsonState.PACKAGE_REGISTRY_DATA,
      [`*`]: PrettyJsonState.DEFAULT,
    },
  },
  // "fallbackExclusionList": [
  //   ...
  // ]
  [PrettyJsonState.FALLBACK_EXCLUSION_LIST]: {
    collapsed: false,
    next: {
      [`*`]: PrettyJsonState.FALLBACK_EXCLUSION_ENTRIES,
    },
  },
  // "fallbackExclusionList": [
  //   [...]
  // ]
  [PrettyJsonState.FALLBACK_EXCLUSION_ENTRIES]: {
    collapsed: true,
    next: {
      [`*`]: PrettyJsonState.FALLBACK_EXCLUSION_DATA,
    },
  },
  // "fallbackExclusionList": [
  //   [..., [...]]
  // ]
  [PrettyJsonState.FALLBACK_EXCLUSION_DATA]: {
    collapsed: true,
    next: {
      [`*`]: PrettyJsonState.DEFAULT,
    },
  },
  // "packageRegistryData": [
  //   ...
  // ]
  [PrettyJsonState.PACKAGE_REGISTRY_DATA]: {
    collapsed: false,
    next: {
      [`*`]: PrettyJsonState.PACKAGE_REGISTRY_ENTRIES,
    },
  },
  // "packageRegistryData": [
  //   [...]
  // ]
  [PrettyJsonState.PACKAGE_REGISTRY_ENTRIES]: {
    collapsed: true,
    next: {
      [`*`]: PrettyJsonState.PACKAGE_STORE_DATA,
    },
  },
  // "packageRegistryData": [
  //   [..., [
  //     ...
  //   ]]
  // ]
  [PrettyJsonState.PACKAGE_STORE_DATA]: {
    collapsed: false,
    next: {
      [`*`]: PrettyJsonState.PACKAGE_STORE_ENTRIES,
    },
  },
  // "packageRegistryData": [
  //   [..., [
  //     [...]
  //   ]]
  // ]
  [PrettyJsonState.PACKAGE_STORE_ENTRIES]: {
    collapsed: true,
    next: {
      [`*`]: PrettyJsonState.PACKAGE_INFORMATION_DATA,
    },
  },
  // "packageRegistryData": [
  //   [..., [
  //     [..., {
  //       ...
  //     }]
  //   ]]
  // ]
  [PrettyJsonState.PACKAGE_INFORMATION_DATA]: {
    collapsed: false,
    next: {
      [`packageDependencies`]: PrettyJsonState.PACKAGE_DEPENDENCIES,
      [`*`]: PrettyJsonState.DEFAULT,
    },
  },
  // "packageRegistryData": [
  //   [..., [
  //     [..., {
  //       "packagePeers": [
  //         ...
  //       ]
  //     }]
  //   ]]
  // ]
  [PrettyJsonState.PACKAGE_DEPENDENCIES]: {
    collapsed: false,
    next: {
      [`*`]: PrettyJsonState.PACKAGE_DEPENDENCY,
    },
  },
  // "packageRegistryData": [
  //   [..., [
  //     [..., {
  //       "packageDependencies": [
  //         [...]
  //       ]
  //     }]
  //   ]]
  // ]
  [PrettyJsonState.PACKAGE_DEPENDENCY]: {
    collapsed: true,
    next: {
      [`*`]: PrettyJsonState.DEFAULT,
    },
  },
};
function generateCollapsedArray(data, state, indent) {
  let result = ``;
  result += `[`;
  for (let t = 0, T = data.length; t < T; ++t) {
    result += generateNext(String(t), data[t], state, indent).replace(/^ +/g, ``);
    if (t + 1 < T) {
      result += `, `;
    }
  }
  result += `]`;
  return result;
}
function generateExpandedArray(data, state, indent) {
  const nextIndent = `${indent}  `;
  let result = ``;
  result += indent;
  result += `[\n`;
  for (let t = 0, T = data.length; t < T; ++t) {
    result += nextIndent + generateNext(String(t), data[t], state, nextIndent).replace(/^ +/, ``);
    if (t + 1 < T) result += `,`;
    result += `\n`;
  }
  result += indent;
  result += `]`;
  return result;
}
function generateCollapsedObject(data, state, indent) {
  const keys = Object.keys(data);
  let result = ``;
  result += `{`;
  for (let t = 0, T = keys.length; t < T; ++t) {
    const key = keys[t];
    const value = data[key];
    if (typeof value === `undefined`) continue;
    result += JSON.stringify(key);
    result += `: `;
    result += generateNext(key, value, state, indent).replace(/^ +/g, ``);
    if (t + 1 < T) {
      result += `, `;
    }
  }
  result += `}`;
  return result;
}
function generateExpandedObject(data, state, indent) {
  const keys = Object.keys(data);
  const nextIndent = `${indent}  `;
  let result = ``;
  result += indent;
  result += `{\n`;
  for (let t = 0, T = keys.length; t < T; ++t) {
    const key = keys[t];
    const value = data[key];
    if (typeof value === `undefined`) continue;
    result += nextIndent;
    result += JSON.stringify(key);
    result += `: `;
    result += generateNext(key, value, state, nextIndent).replace(/^ +/g, ``);
    if (t + 1 < T) result += `,`;
    result += `\n`;
  }
  result += indent;
  result += `}`;
  return result;
}
function generateNext(key, data, state, indent) {
  const { next } = prettyJsonMachine[state];
  const nextState = next[key] || next[`*`];
  return generate(data, nextState, indent);
}
function generate(data, state, indent) {
  const { collapsed } = prettyJsonMachine[state];
  if (Array.isArray(data)) {
    if (collapsed) {
      return generateCollapsedArray(data, state, indent);
    } else {
      return generateExpandedArray(data, state, indent);
    }
  }
  if (typeof data === `object` && data !== null) {
    if (collapsed) {
      return generateCollapsedObject(data, state, indent);
    } else {
      return generateExpandedObject(data, state, indent);
    }
  }
  return JSON.stringify(data);
}
function generatePrettyJson(data) {
  return generate(data, PrettyJsonState.TOP_LEVEL, ``);
}
